const { injectBabelPlugin } = require('react-app-rewired');
const rewireLess = require('react-app-rewire-less');
const rewireSvgReactLoader = require('react-app-rewire-svg-react-loader');
// const AntDesignThemePlugin = require('antd-theme-webpack-plugin');
const FilterWarningsPlugin = require('webpack-filter-warnings-plugin');
const cssVariables = require('./src/utils/cssVariables.js');

module.exports = function override(config, env) {
  // config = rewireLess(config, env);

  config = injectBabelPlugin(
    ['import', { libraryName: 'antd', libraryDirectory: 'es', style: true }], // change importing css to less
    config,
  );
  config = rewireLess.withLoaderOptions({
    modifyVars: cssVariables,
    javascriptEnabled: true,
  })(config, env);

  config = rewireSvgReactLoader(config, env);

  // config.plugins.push(new AntDesignThemePlugin(options));
  // Silence mini-css-extract-plugin generating lots of warnings for CSS ordering.
  // We use CSS modules that should not care for the order of CSS imports, so we
  // should be safe to ignore these.
  //
  // See: https://github.com/webpack-contrib/mini-css-extract-plugin/issues/250
  config.plugins.push(new FilterWarningsPlugin({
    exclude: /mini-css-extract-plugin[^]*Conflicting order between:/
  }));

  return config;
};
