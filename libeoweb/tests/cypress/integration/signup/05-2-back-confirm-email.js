const baseUrl = Cypress.config('baseUrl');
const me = require('../../fixtures/me');
const invoicesEmpty = require('../../fixtures/invoicesEmpty');

describe('Signup', () => {
  context('Signup ('+baseUrl+')', async () => {
    it('Confirm success', () => {
      cy.visitStubbed('/login/965dd5b6f70874ab6a511ea6591a30d1debe6ee28b53eaad68fa64e5def8bf24', {
        activationUser: { data: { activationUser: true } },
        signin: { data: { signin: me.data.me } },
        invoices: invoicesEmpty
      });
      cy.get('.form-signin #email').first().type('Hello1@hello.fr');
      cy.get('.form-signin #password').first().type('Hello1@hello.fr');
      cy.get('.form-signin button').click();
      cy.url().should('equal', `${baseUrl}/`);
    });
  });
});
