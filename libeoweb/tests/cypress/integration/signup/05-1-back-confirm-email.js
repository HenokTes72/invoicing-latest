const baseUrl = Cypress.config('baseUrl');

const activationUser = {
  "errors": [
    {
      "message": "api.error.user.invalid_confirmation_token",
      "locations": [{ "line": 2, "column": 3 }],
      "path": ["activationUser"],
      "extensions": {
        "code": "INTERNAL_SERVER_ERROR",
        "exception": {
          "response": "api.error.user.invalid_confirmation_token",
          "status": 400,
          "message": "api.error.user.invalid_confirmation_token",
          "stacktrace": [
            "Error: api.error.user.invalid_confirmation_token",
            "    at UsersService.<anonymous> (/var/www/html/src/common/services/users.service.ts:120:13)",
            "    at Generator.next (<anonymous>)",
            "    at fulfilled (/var/www/html/src/common/services/users.service.ts:16:58)",
            "    at process.internalTickCallback (internal/process/next_tick.js:77:7)"
          ]
        }
      }
    }
  ],
  "data": { "activationUser": null }
};

describe('Signup', () => {
  context('Signup ('+baseUrl+')', async () => {
    it('Confirm error', () => {
      cy.visitStubbed('/login/965dd5b6f70874ab6a511ea6591a30d1debe6ee28b53eaad68fa64e5def8bf24', {
        activationUser: activationUser
      });
      cy.get('.form-signin #email').first().type('Hello1@hello.fr');
      cy.get('.form-signin #password').first().type('Hello1@hello.fr');
      cy.get('.form-signin button').click();
      cy.get('body').find('.ant-alert-message').should('exist');
    });
  });
});
