const baseUrl = Cypress.config('baseUrl');

const signup = {
  "errors": [
    {
      "message": "api.error.signup_email_exist",
      "locations": [{ "line": 63, "column": 3 }],
      "path": ["signup"],
      "extensions": {
        "code": "INTERNAL_SERVER_ERROR",
        "exception": {
          "response": "api.error.signup_email_exist",
          "status": 400,
          "message": "api.error.signup_email_exist",
          "stacktrace": [
            "Error: api.error.signup_email_exist",
            "    at AuthResolvers.<anonymous> (/var/www/html/src/auth/auth.resolvers.ts:25:13)",
            "    at Generator.next (<anonymous>)",
            "    at fulfilled (/var/www/html/src/auth/auth.resolvers.ts:16:58)",
            "    at process.internalTickCallback (internal/process/next_tick.js:77:7)"
          ]
        }
      }
    }
  ],
  "data": { "signup": null }
};

describe('Signup', () => {
  context('Signup ('+baseUrl+')', async () => {
    it('Email exist', () => {
      cy.visitStubbed('/signup', {
        signup: signup
      });
      cy.get('.form-signup #firstname').first().type('Hello');
      cy.get('.form-signup #lastname').first().type('Hello');
      cy.get('.form-signup #email').first().type('Hello1@hello.fr');
      cy.get('.form-signup #password').first().type('Hello1@hello.fr');
      cy.get('.form-signup #validate-password').first().type('Hello1@hello.fr');
      cy.get('.form-signup #cgu').first().check();
      cy.get('.form-signup button').click();
      cy.get('body').find('.ant-alert-message').should('exist');
    });
  });
});
