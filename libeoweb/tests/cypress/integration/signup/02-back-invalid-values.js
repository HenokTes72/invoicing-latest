const baseUrl = Cypress.config('baseUrl');

const signup = {
  "errors": [
    {
      "message": [
        {
          "target": {
            "enabled": true,
            "lastCguAccept": "2019-02-07T06:28:57.568Z",
            "lastLogin": "2019-02-07T06:28:57.568Z",
            "createdAt": "2019-02-07T06:28:57.568Z",
            "updatedAt": "2019-02-07T06:28:57.568Z",
            "firstname": "Hello1@hello.fr",
            "lastname": "Hello1@hello.fr",
            "email": "Hello1@hello.fr",
            "password": "Hello1@hello.fr"
          },
          "value": "Hello1@hello.fr",
          "property": "firstname",
          "children": [],
          "constraints": {
            "matches": "firstname must match /^[a-zA-Z-\\s]+$/ regular expression"
          }
        },
        {
          "target": {
            "enabled": true,
            "lastCguAccept": "2019-02-07T06:28:57.568Z",
            "lastLogin": "2019-02-07T06:28:57.568Z",
            "createdAt": "2019-02-07T06:28:57.568Z",
            "updatedAt": "2019-02-07T06:28:57.568Z",
            "firstname": "Hello1@hello.fr",
            "lastname": "Hello1@hello.fr",
            "email": "Hello1@hello.fr",
            "password": "Hello1@hello.fr"
          },
          "value": "Hello1@hello.fr",
          "property": "lastname",
          "children": [],
          "constraints": {
            "matches": "lastname must match /^[a-zA-Z-\\s]+$/ regular expression"
          }
        }
      ],
      "locations": [{ "line": 63, "column": 3 }],
      "path": ["signup"],
      "extensions": {
        "code": "INTERNAL_SERVER_ERROR",
        "exception": {
          "response": [
            {
              "target": {
                "enabled": true,
                "lastCguAccept": "2019-02-07T06:28:57.568Z",
                "lastLogin": "2019-02-07T06:28:57.568Z",
                "createdAt": "2019-02-07T06:28:57.568Z",
                "updatedAt": "2019-02-07T06:28:57.568Z",
                "firstname": "Hello1@hello.fr",
                "lastname": "Hello1@hello.fr",
                "email": "Hello1@hello.fr",
                "password": "Hello1@hello.fr"
              },
              "value": "Hello1@hello.fr",
              "property": "firstname",
              "children": [],
              "constraints": {
                "matches": "firstname must match /^[a-zA-Z-\\s]+$/ regular expression"
              }
            },
            {
              "target": {
                "enabled": true,
                "lastCguAccept": "2019-02-07T06:28:57.568Z",
                "lastLogin": "2019-02-07T06:28:57.568Z",
                "createdAt": "2019-02-07T06:28:57.568Z",
                "updatedAt": "2019-02-07T06:28:57.568Z",
                "firstname": "Hello1@hello.fr",
                "lastname": "Hello1@hello.fr",
                "email": "Hello1@hello.fr",
                "password": "Hello1@hello.fr"
              },
              "value": "Hello1@hello.fr",
              "property": "lastname",
              "children": [],
              "constraints": {
                "matches": "lastname must match /^[a-zA-Z-\\s]+$/ regular expression"
              }
            }
          ],
          "status": 400,
          "message": [
            {
              "target": {
                "enabled": true,
                "lastCguAccept": "2019-02-07T06:28:57.568Z",
                "lastLogin": "2019-02-07T06:28:57.568Z",
                "createdAt": "2019-02-07T06:28:57.568Z",
                "updatedAt": "2019-02-07T06:28:57.568Z",
                "firstname": "Hello1@hello.fr",
                "lastname": "Hello1@hello.fr",
                "email": "Hello1@hello.fr",
                "password": "Hello1@hello.fr"
              },
              "value": "Hello1@hello.fr",
              "property": "firstname",
              "children": [],
              "constraints": {
                "matches": "firstname must match /^[a-zA-Z-\\s]+$/ regular expression"
              }
            },
            {
              "target": {
                "enabled": true,
                "lastCguAccept": "2019-02-07T06:28:57.568Z",
                "lastLogin": "2019-02-07T06:28:57.568Z",
                "createdAt": "2019-02-07T06:28:57.568Z",
                "updatedAt": "2019-02-07T06:28:57.568Z",
                "firstname": "Hello1@hello.fr",
                "lastname": "Hello1@hello.fr",
                "email": "Hello1@hello.fr",
                "password": "Hello1@hello.fr"
              },
              "value": "Hello1@hello.fr",
              "property": "lastname",
              "children": [],
              "constraints": {
                "matches": "lastname must match /^[a-zA-Z-\\s]+$/ regular expression"
              }
            }
          ],
          "stacktrace": [
            "Error: An instance of User has failed the validation:",
            " - property firstname has failed the following constraints: matches ",
            ",An instance of User has failed the validation:",
            " - property lastname has failed the following constraints: matches ",
            "",
            "    at UsersService.<anonymous> (/var/www/html/src/common/services/users.service.ts:34:13)",
            "    at Generator.next (<anonymous>)",
            "    at fulfilled (/var/www/html/src/common/services/users.service.ts:16:58)",
            "    at process.internalTickCallback (internal/process/next_tick.js:77:7)"
          ]
        }
      }
    }
  ],
  "data": { "signup": null }
};

describe('Signup', () => {
  context('Signup ('+baseUrl+')', async () => {
    it('Invalid form values', () => {
      cy.visitStubbed('/signup', {
        signup: signup
      });
      cy.get('.form-signup #firstname').first().type('Hello@hello.fr');
      cy.get('.form-signup #lastname').first().type('Hello@hello.fr');
      cy.get('.form-signup #email').first().type('Hello1@hello.fr');
      cy.get('.form-signup #password').first().type('Hello1@hello.fr');
      cy.get('.form-signup #validate-password').first().type('Hello1@hello.fr');
      cy.get('.form-signup #cgu').first().check();
      cy.get('.form-signup button').click();
      cy.get('body').find('.ant-alert-message').should('exist');
      cy.get('.ant-alert-message').should('have.length', 2);
    });
  });
});
