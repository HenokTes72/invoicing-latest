const baseUrl = Cypress.config('baseUrl');

describe('Signup', () => {
  context('Signup ('+baseUrl+')', async () => {
    it('Valid resend email', () => {
      cy.visitStubbed('/signup-confirm-email', {
        refreshConfirmationTokenUser: {
          data: {
            refreshConfirmationTokenUser: {
              email: 'Hello1@hello.fr',
              __typename: 'User'
            }
          }
        }

      });
      cy.get('.form-signup-activate #email').first().type('Hello1@hello.fr');
      cy.get('.form-signup-activate button').click();
      cy.url().should('equal', `${baseUrl}/signup-confirm-email`);
    });
  });
});
