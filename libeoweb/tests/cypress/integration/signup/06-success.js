const baseUrl = Cypress.config('baseUrl');
const me = require('../../fixtures/me');

describe('Signup', () => {
  context('Signup ('+baseUrl+')', async () => {
    it('Success', () => {
      cy.visitStubbed('/signup', {
        signup: { data: { signup: me.data.me } },
      });
      cy.get('.form-signup #firstname').first().type('Hello');
      cy.get('.form-signup #lastname').first().type('Hello');
      cy.get('.form-signup #email').first().type('Hello1@hello.fr');
      cy.get('.form-signup #password').first().type('Hello1@hello.fr');
      cy.get('.form-signup #validate-password').first().type('Hello1@hello.fr');
      cy.get('.form-signup #cgu').first().check();
      cy.get('.form-signup button').click();
      cy.url().should('equal', `${baseUrl}/signup-success`);
    });
  });
});
