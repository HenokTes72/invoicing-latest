const baseUrl = Cypress.config('baseUrl');

const refreshConfirmationTokenUserError = {
  "errors": [
    {
      "message": "api.error.user.invalid_confirmation_token",
      "locations": [{ "line": 2, "column": 3 }],
      "path": ["activationUser"],
      "extensions": {
        "code": "INTERNAL_SERVER_ERROR",
        "exception": {
          "response": "api.error.user.invalid_confirmation_token",
          "status": 400,
          "message": "api.error.user.invalid_confirmation_token",
          "stacktrace": [
            "Error: api.error.user.invalid_confirmation_token",
            "    at UsersService.<anonymous> (/var/www/html/src/common/services/users.service.ts:120:13)",
            "    at Generator.next (<anonymous>)",
            "    at fulfilled (/var/www/html/src/common/services/users.service.ts:16:58)",
            "    at process.internalTickCallback (internal/process/next_tick.js:77:7)"
          ]
        }
      }
    }
  ],
  "data": { "activationUser": null }
};

describe('Signup', () => {
  context('Signup ('+baseUrl+')', async () => {
    it('Invalid resend email', () => {
      cy.visitStubbed('/signup-confirm-email', {
        refreshConfirmationTokenUser: refreshConfirmationTokenUserError
      });
      cy.url().should('equal', `${baseUrl}/signup-confirm-email`);
      cy.get('.form-signup-activate #email').first().type('Hello1@hello.fr');
      cy.get('.form-signup-activate button').click();
      cy.get('body').find('.ant-alert-message').should('exist');
    });
  });
});
