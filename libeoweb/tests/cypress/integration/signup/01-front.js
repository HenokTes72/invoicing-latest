const baseUrl = Cypress.config('baseUrl');
const me = require('../../fixtures/me');

describe('Signup', () => {
    context('Signup ('+baseUrl+')', async () => {
      it('Form values', () => {
        cy.visitStubbed('/signup', {
          signup: {
            data: {
              signup: me.data.me
            }
          }
        });
        cy.get('.form-signup button').click();
        cy.get('.ant-form-explain').should('have.length', 6);
        cy.get('.form-signup #firstname').first().type('Hello');
        cy.get('.form-signup').submit();
        cy.get('.ant-form-explain').should('have.length', 5);
        cy.get('.form-signup #lastname').first().type('Hello');
        cy.get('.form-signup').submit();
        cy.get('.ant-form-explain').should('have.length', 4);
        cy.get('.form-signup #email').first().type('Hello1@hello.fr');
        cy.get('.form-signup').submit();
        cy.get('.ant-form-explain').should('have.length', 3);
        cy.get('.form-signup #password').first().type('Hello1@hello.fr');
        cy.get('.form-signup').submit();
        cy.get('.ant-form-explain').should('have.length', 2);
        cy.get('.form-signup #validate-password').first().type('Hello2@hello.fr');
        cy.get('.form-signup').submit();
        cy.get('.ant-form-explain').should('have.length', 2);
        cy.get('.form-signup #validate-password').first().clear();
        cy.get('.form-signup #validate-password').first().type('Hello1@hello.fr');
        cy.get('.form-signup').submit();
        cy.get('.ant-form-explain').should('have.length', 1);
        cy.get('.form-signup #cgu').first().check();
        cy.get('.form-signup').submit();
        cy.get('.ant-form-explain').should('have.length', 0);
      });
    });
});
