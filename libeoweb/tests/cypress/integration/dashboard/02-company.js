const baseUrl = Cypress.config('baseUrl');
const me = require('../../fixtures/me');
me.data.me.currentCompany.kycStatus = null;
me.data.me.currentCompany.kycStep = 'PERSONNAL_INFORMATION';
const invoices = require('../../fixtures/invoicesEmpty');

describe('Dashboard', () => {
  context('Dashboard ('+baseUrl+')', async () => {
    it('Company', () => {
      localStorage.setItem('token', me.data.me.token);
      cy.visitStubbed('/', {
        me: me,
        invoices: invoices
      });
      cy.url().should('equal', `${baseUrl}/`);

      cy.get('body').find('.ant-steps').should('exist');

      cy.get('body').find('.ant-steps-item:nth-child(2)').should('exist');
      cy.get('body').find('.ant-steps-item:nth-child(2)').should('have.class', 'ant-steps-item-process');
    });
  });
});
