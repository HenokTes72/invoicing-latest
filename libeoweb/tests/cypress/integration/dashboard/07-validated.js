const baseUrl = Cypress.config('baseUrl');
const me = require('../../fixtures/me');
const invoices = require('../../fixtures/invoicesEmpty');

describe('Dashboard', () => {
    context('Dashboard ('+baseUrl+')', async () => {
      it('Validated company', () => {
        localStorage.setItem('token', me.data.me.token);
        me.data.me.currentCompany.kycStatus = 'VALIDATED';
        me.data.me.currentCompany.kycStep = 'IBAN';

        cy.visitStubbed('/', {
          me: me,
          invoices: invoices
        });
        cy.url().should('equal', `${baseUrl}/`);

        cy.get('body').find('.ant-steps').should('not.exist');
      });
    });
});
