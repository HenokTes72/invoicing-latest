const baseUrl = Cypress.config('baseUrl');
const me = require('../../fixtures/me');
const invoices = require('../../fixtures/invoicesScanned');

describe('Dashboard', () => {
  context('Dashboard ('+baseUrl+')', async () => {
    it('With invoices', () => {
      localStorage.setItem('token', me.data.me.token);
      cy.visitStubbed('/', {
        me: me,
        invoices: invoices
      });
      cy.url().should('equal', `${baseUrl}/`);

      cy.get('body').find('.card-dashboard').should('not.exist');
      cy.get('body').find('.bloc-invoice-count').should('exist');
    });
  });
});
