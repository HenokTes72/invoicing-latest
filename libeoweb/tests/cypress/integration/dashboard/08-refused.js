const baseUrl = Cypress.config('baseUrl');
const me = require('../../fixtures/me');
const invoices = require('../../fixtures/invoicesEmpty');

describe('Dashboard', () => {
  context('Dashboard ('+baseUrl+')', async () => {
    it('Refused company', () => {
      localStorage.setItem('token', me.data.me.token);
      me.data.me.currentCompany.kycStatus = 'REFUSED';
      me.data.me.currentCompany.kycStep = 'IBAN';

      cy.visitStubbed('/', {
        me: me,
        invoices: invoices
      });
      cy.url().should('equal', `${baseUrl}/`);

      cy.get('body').find('.ant-steps').should('exist');

      cy.get('body').find('.ant-steps-item:nth-child(5)').should('exist');
      cy.get('body').find('.ant-steps-item:nth-child(5)').should('have.class', 'ant-steps-item-process');

      cy.get('body').find('.ant-steps-item:nth-child(5) .dot-process.error').should('exist');
    });
  });
});
