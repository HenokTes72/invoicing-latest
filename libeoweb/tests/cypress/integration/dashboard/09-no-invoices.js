const baseUrl = Cypress.config('baseUrl');
const me = require('../../fixtures/me');
const invoices = require('../../fixtures/invoicesEmpty');

describe('Dashboard', () => {
  context('Dashboard ('+baseUrl+')', async () => {
    it('No invoices', () => {
      localStorage.setItem('token', me.data.me.token);
      cy.visitStubbed('/', {
        me: me,
        invoices: invoices
      });
      cy.url().should('equal', `${baseUrl}/`);

      cy.get('body').find('.card-dashboard').should('exist');
      cy.get('body').find('.bloc-invoice-count').should('not.exist');
    });
  });
});
