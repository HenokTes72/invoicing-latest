const baseUrl = Cypress.config('baseUrl');
const me = require('../../fixtures/me');
const invoices = require('../../fixtures/invoicesControlled');
const representatives = require('../../fixtures/representatives');
const beneficiaries = require('../../fixtures/beneficiaries');

describe('Pay', () => {
  context('Pay ('+baseUrl+')', async () => {
    it('Iban', () => {
        localStorage.setItem('token', me.data.me.token);
        me.data.me.currentCompany.kycStatus = null;
        me.data.me.currentCompany.kycStep = 'IBAN';
        cy.visitStubbed('/purchase/bills', {
          me: me,
          invoices: invoices,
          representatives: representatives,
          beneficiaries: beneficiaries
        });
        cy.url().should('equal', `${baseUrl}/purchase/bills`);
        cy.get('.btn-invoice-to-pay').click();
        cy.get('body').find('.transfert-dialog').should('exist');
        cy.get('.close-dialog').click();
        cy.get('body').find('.open.RightSideBar_sidebar-right').should('exist');
        cy.get('.close-sidebar-right').click();
        cy.get('body').find('.open.RightSideBar_sidebar-right').should('not.exist');
        cy.get('.btn-invoice-to-pay').click();
        cy.get('body').find('.transfert-dialog').should('not.exist'); // because cookie
      });
    });
});
