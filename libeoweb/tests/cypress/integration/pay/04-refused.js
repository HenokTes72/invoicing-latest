const baseUrl = Cypress.config('baseUrl');
const me = require('../../fixtures/me');
const invoices = require('../../fixtures/invoicesControlled');
me.data.me.currentCompany.kycStatus = 'REFUSED';

describe('Pay', () => {
  context('Pay ('+baseUrl+')', async () => {
    it('Refused company', () => {
      localStorage.setItem('token', me.data.me.token);
      cy.visitStubbed('/purchase/bills', {
        me: me,
        invoices: invoices
      });
      cy.url().should('equal', `${baseUrl}/purchase/bills`);
      cy.get('.btn-invoice-to-pay').click();
      cy.get('body').find('.onboarding-refused-dialog').should('exist');
      cy.get('.close-dialog').click();
      cy.get('body').find('.open.RightSideBar_sidebar-right').should('exist');
      cy.get('.close-sidebar-right').click();
      cy.get('body').find('.open.RightSideBar_sidebar-right').should('not.exist');
      cy.get('.btn-invoice-to-pay').click();
      cy.get('body').find('.onboarding-refused-dialog').should('exist');
      cy.get('.close-dialog').click();
    });
  });
});
