const baseUrl = Cypress.config('baseUrl');
const me = require('../../fixtures/me');
const invoices = require('../../fixtures/invoicesControlled');
const representatives = require('../../fixtures/representatives');
const beneficiaries = require('../../fixtures/beneficiaries');

describe('Pay', () => {
  context('Pay ('+baseUrl+')', async () => {
    it('Personal information', () => {
        localStorage.setItem('token', me.data.me.token);
        me.data.me.currentCompany.kycStatus = null;
        me.data.me.currentCompany.kycStep = 'PERSONNAL_INFORMATION';
        cy.visitStubbed('/purchase/bills', {
          me: me,
          invoices: invoices,
          representatives: representatives,
          beneficiaries: beneficiaries
        });
        cy.url().should('equal', `${baseUrl}/purchase/bills`);

        cy.get('.btn-invoice-to-pay').click();
        cy.get('body').find('.onboarding-start-dialog').should('exist');
        cy.get('.body-dialog .ant-btn-primary').click();
        cy.url().should('equal', `${baseUrl}/kyc/personal-information`);
      });
    });
});
