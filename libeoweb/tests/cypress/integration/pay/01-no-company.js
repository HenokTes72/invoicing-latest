const baseUrl = Cypress.config('baseUrl');
const me = require('../../fixtures/me');
me.data.me.currentCompany.kycStatus = null;
me.data.me.currentCompany.kycStep = null;
const invoices = require('../../fixtures/invoicesControlled');
delete me.currentCompany;

describe('Pay', () => {
    context('Pay ('+baseUrl+')', async () => {
      it('No company', () => {
        localStorage.setItem('token', me.data.me.token);
        cy.visitStubbed('/purchase/bills', {
          me: me,
          invoices: invoices
        });
        cy.url().should('equal', `${baseUrl}/purchase/bills`);

        cy.get('.btn-invoice-to-pay').click();
        cy.get('body').find('.onboarding-start-dialog').should('exist');
        cy.get('.close-dialog').click();
        cy.get('body').find('.open.RightSideBar_sidebar-right').should('not.exist');

        cy.get('.btn-invoice-to-pay').click();
        cy.get('body').find('.onboarding-start-dialog').should('exist');
        cy.get('.body-dialog .ant-btn-primary').click();
        cy.url().should('equal', `${baseUrl}/kyc`);
      });
    });
});
