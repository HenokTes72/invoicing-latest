const baseUrl = Cypress.config('baseUrl');
const me = require('../../fixtures/me');
const partner = require('../../fixtures/partner');
const invoices = require('../../fixtures/invoicesControlled');
me.data.me.currentCompany.kycStatus = 'VALIDATED';
invoices.data.invoices.rows[0].total = 2001;

describe('Pay', () => {
  beforeEach(() => {
    cy.initStub(me);
  });

  context('Pay (' + baseUrl + ')', async () => {
    it('Code > 2000', () => {
      cy.visitStubbed('/purchase/bills', {
        me: me,
        invoices: invoices,
        partner: partner,
        checkBalance: {data: {checkBalance:false}},
        generateCode: {data: {generateCode: {id:1051, __typename: 'Invoice'}}}
      })
      cy.url().should('equal', `${baseUrl}/purchase/bills`);
      cy.get('.btn-invoice-to-pay').click();
      cy.get('body')
        .find('.code-dialog')
        .should('exist');

      cy.get('#code-0').first().type('1');
      cy.get('#code-1').first().type('2');
      cy.get('#code-2').first().type('3');
      cy.get('#code-3').first().type('4');
      cy.get('#code-4').first().type('5');
      cy.get('#code-5').first().type('6');

      cy.get('body').find('.transfert-dialog').should('exist');
      cy.get('.close-dialog').click();
      cy.get('body').find('.open.RightSideBar_sidebar-right').should('exist');
    });
  });
});
