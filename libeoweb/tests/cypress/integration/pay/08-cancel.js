const baseUrl = Cypress.config('baseUrl');
const me = require('../../fixtures/me');
const invoices = require('../../fixtures/invoicesControlled');
invoices.data.invoices.rows[0].status = 'PLANNED';
me.data.me.currentCompany.kycStatus = 'VALIDATED';
const invoice = JSON.parse(JSON.stringify(invoices.data.invoices.rows[0]));
invoice.status = 'TO_PAY';

describe('Pay', () => {
  context('Pay (' + baseUrl + ')', async () => {
    it('Code company with no fund', () => {
      localStorage.setItem('token', me.data.me.token);
      cy.visitStubbed('/purchase/bills', {
        me: me,
        invoices: invoices,
        updateInvoiceStatus: {data: {updateInvoiceStatus: invoice}}
      });
      cy.url().should('equal', `${baseUrl}/purchase/bills`);
      cy.get('.btn-invoice-planned').click();
      cy.get('body')
        .find('.btn-invoice-to-pay')
        .should('exist');
    });
  });
});
