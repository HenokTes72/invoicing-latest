const baseUrl = Cypress.config('baseUrl');
const me = require('../../fixtures/me');

describe('Signin', () => {
    context('Signin ('+baseUrl+')', async () => {
      it('Signin invalid front', () => {
        cy.visitStubbed('/login', {
          signin: {
            data: {
              signin: me.data.me
            }
          }
        });
        cy.get('.form-signin button').click();
        cy.get('.form-signin').submit();
        cy.get('.ant-form-explain').should('have.length', 2);
        cy.get('#email').first().type('Hello1@hello.fr');
        cy.get('.form-signin').submit();
        cy.get('.ant-form-explain').should('have.length', 1);
        cy.get('.form-signin button').click();
        cy.get('.form-signin').submit();
        cy.get('.ant-form-explain').should('have.length', 1);
        cy.get('#password').first().type('Hello1@hello.fr');
        cy.get('.form-signin').submit();
        cy.get('.ant-form-explain').should('have.length', 0);
      });
    });
});
