const baseUrl = Cypress.config('baseUrl');
const signin = {
  "errors": [
    {
      "message": "api.error.signin",
      "locations": [{ "line": 63, "column": 3 }],
      "path": ["signin"],
      "extensions": {
        "code": "INTERNAL_SERVER_ERROR",
        "exception": {
          "response": "api.error.signin",
          "status": 400,
          "message": "api.error.signin",
          "stacktrace": [
            "Error: api.error.signin",
            "    at AuthResolvers.<anonymous> (/var/www/html/src/auth/auth.resolvers.ts:42:13)",
            "    at Generator.next (<anonymous>)",
            "    at fulfilled (/var/www/html/src/auth/auth.resolvers.ts:16:58)",
            "    at process.internalTickCallback (internal/process/next_tick.js:77:7)"
          ]
        }
      }
    }
  ],
  "data": { "signin": null }
};

describe('Signin', () => {
  context('Signin ('+baseUrl+')', async () => {
    it('Signin invalid back', () => {
      cy.visitStubbed('/login', {
        signin: signin
      });
      cy.get('.form-signin #email').first().type('Hello2@hello.fr');
      cy.get('.form-signin #password').first().type('Hello2@hello.fr');
      cy.get('.form-signin button').click();
      cy.get('body').find('.ant-alert-message').should('exist');
    });
  });
});
