const baseUrl = Cypress.config('baseUrl');
const me = require('../../fixtures/me');
const invoicesEmpty = require('../../fixtures/invoicesEmpty');

describe('Signin', () => {
  context('Signin ('+baseUrl+')', async () => {
    it('Signin', () => {
      cy.visitStubbed('/login', {
        signin: { data: { signin: me.data.me } },
        me: me,
        invoices: invoicesEmpty
      });
      cy.get('.form-signin #email').first().type('Hello1@hello.fr');
      cy.get('.form-signin #password').first().type('Hello1@hello.fr');
      cy.get('.form-signin button').click();
      cy.url().should('equal', `${baseUrl}/`);
    });
  });
});
