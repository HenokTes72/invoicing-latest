import gql from 'graphql-tag';

import fragments from '../User/fragments';

export const signup = gql`
  ${fragments.fragment}

  mutation signup($input: SignUpInput!) {
    signup(input: $input) {
      ${fragments.query}
    }
  }
`;

export const signin = gql`
  ${fragments.fragment}

  mutation signin($input: SignInInput!) {
    signin(input: $input) {
      ${fragments.query}
    }
  }
`;

export const signout = gql`
  mutation {
    logout
  }
`;

export const activationUser = gql`
  mutation activationUser($confirmationToken: String!) {
    activationUser(confirmationToken: $confirmationToken)
  }
`;

export const refreshConfirmationTokenUser = gql`
  mutation refreshConfirmationTokenUser($email: String!) {
    refreshConfirmationTokenUser(email: $email) {
      email
    }
  }
`;
