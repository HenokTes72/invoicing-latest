import * as React from 'react';
import { IUser } from '../User/types';

export interface IAuthInterface {
  activate: (confirmationToken: string) => Promise<boolean>;
  resendActivate: (email: string) => Promise<boolean>;
  reset: () => void;
  signin: (user: IUser) => Promise<string | undefined>;
  signout: () => Promise<boolean>;
  signup: (user: IUser) => Promise<boolean>;
  token: string | null;
}

export interface IAuthContextInterface {
  auth?: IAuthInterface;
}

const { Provider, Consumer } = React.createContext<IAuthContextInterface>({
  auth: {
    activate: async () => false,
    resendActivate: async (email: string) => false,
    reset: () => {},
    signin: async () => undefined,
    signout: async () => false,
    signup: async () => false,
    token: null,
  },
});

export { Provider, Consumer };
