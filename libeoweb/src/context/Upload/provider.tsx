import { UploadFile } from 'antd/lib/upload/interface';
import { ApolloClient, ApolloError } from 'apollo-client';
import { UploadWrapper } from 'components/Upload';
import * as Alert from 'context/Alert';
import { invoices } from 'context/Invoices/queries';
import events, { EventEmitter } from 'events';
import * as React from 'react';
import { compose, graphql, withApollo } from 'react-apollo';
import Dropzone, { DropFileEventHandler } from 'react-dropzone';
import { InjectedIntlProps, injectIntl } from 'react-intl';
import { RouteComponentProps, withRouter } from 'react-router';
import { delay, errorOrSuccess, parseError } from 'utils';
import uuidv1 from 'uuid/v1';
import { IUploadContextInterface, Provider } from './context';
import { createInvoice } from './queries';

interface IState extends IUploadContextInterface {
  limit: number;
  queu: File[];
}

interface IProps
  extends InjectedIntlProps,
    Alert.InjectedProps,
    RouteComponentProps {
  children: React.ReactNode;
  client: ApolloClient<any>;
  createInvoice: any;
}

class AuthProvider extends React.PureComponent<IProps, IState> {
  state = {
    limit: 10,
    queu: [],
    upload: {
      clearFilesUploading: () => {
        this.setState({
          upload: {
            ...this.state.upload,
            filesUploading: [],
          },
        });
      },
      create: async (file: File, uuid: string) => {
        try {
          const results = await this.props.createInvoice({
            update: (cache: any, { data }: any) => {
              data.createInvoice &&
                this.setState(
                  {
                    upload: {
                      ...this.state.upload,
                      filesUploading: this.updateLocalFileStatus(
                        { ...data.createInvoice, uuid },
                        false,
                      ),
                    },
                  },
                  () => {
                    this.next();
                    this.onUpload && this.onUpload.emit('onUpload', file);
                  },
                );
            },
            variables: { input: { file } },
          });
          if (results.errors) {
            this.setState(
              {
                upload: {
                  ...this.state.upload,
                  filesUploading: this.updateLocalFileStatus(
                    { name: file.name, size: file.size, uuid },
                    false,
                    parseError(results.errors).join(' '), // 'common.upload.server_error'
                  ),
                },
              },
              () => {
                this.next();
              },
            );
          }
        } catch (e) {
          this.next();
          this.setState({
            upload: {
              ...this.state.upload,
              filesUploading: this.updateLocalFileStatus(
                file,
                false,
                parseError(e).join(' '), // 'common.upload.server_error'
              ),
            },
          });
        }
      },
      filesUploading: [],
      off: (event: (file?: File) => void) => {
        this.onUpload && this.onUpload.removeListener('onUpload', event);
      },
      on: (event: (file?: File) => void) => {
        this.onUpload && this.onUpload.addListener('onUpload', event);
      },
      setVisibility: (visible: boolean) => {
        const { upload } = this.state;
        this.setState({
          upload: {
            ...upload,
            visible,
          },
        });
      },
      visible: false,
    },
  };

  handleDisableClick: (e: any) => void;
  handleClickUpload: (file: File) => void;
  handleDrop: (acceptedFiles: File[], rejectedFiles: File[]) => void;
  handleDropAccepted: (acceptedOrRejected: File[]) => void;
  handleDropRejected: (acceptedOrRejected: File[]) => void;
  handleZonRef: (node: React.ReactNode) => void;

  onUpload?: EventEmitter;
  constructor(props: any) {
    super(props);

    this.handleDisableClick = this.disableClick.bind(this);
    this.handleClickUpload = this.clickUpload.bind(this);
    this.handleDrop = this.drop.bind(this);
    this.handleDropAccepted = this.accepted.bind(this);
    this.handleDropRejected = this.rejected.bind(this);
    this.handleZonRef = this.zone.bind(this);
  }
  updateLocalFileStatus = (
    file: any,
    finish: boolean,
    error?: string,
  ): any[] => {
    let found = false;
    const files = this.state.upload && this.state.upload.filesUploading;

    let res = [];
    if (files) {
      res = files.map((updatedFile: any) => {
        if (updatedFile.uuid === file.uuid) {
          found = true;
          updatedFile.loading = finish;
          updatedFile.error = error;
        }

        return updatedFile;
      });
    }

    return res;
  };

  disableClick(e: any) {
    e.preventDefault();
  }

  drop() {
    this.state.upload.setVisibility(false);
  }

  toFile = (f: any): any => {
    return {
      lastModified: f.lastModified,
      lastModifiedDate: f.lastModifiedDate,
      loading: true,
      name: f.name,
      size: f.size,
      type: f.type,
      uid: f.uid,
      uuid: uuidv1(),
    };
  };

  async clickUpload(file: File) {
    const fileRef = this.toFile(file);
    if (file.size / 10000000 >= 10) {
      fileRef.error = 'common.upload.error_file_too_big';
    }
    this.setState(
      {
        upload: {
          ...this.state.upload,
          filesUploading: [...this.state.upload.filesUploading, fileRef],
        },
      },
      () => {
        this.state.upload.create(file, fileRef.uuid);
        this.state.upload.setVisibility(false);
      },
    );
  }

  next() {
    if (this.state.queu) {
      const nexts: File[] = this.state.queu.slice(0);
      const next = nexts.shift();
      if (next) {
        this.clickUpload(next);
      }
      this.setState({
        queu: nexts,
      });
    }
  }

  accepted(acceptedOrRejected: File[]) {
    if (acceptedOrRejected.length > this.state.limit) {
      const queu: File[] = acceptedOrRejected.splice(this.state.limit);
      this.setState({
        queu,
      });
    }

    const filesRef: any[] = acceptedOrRejected.map(file => {
      const fileRef = this.toFile(file);
      if (file.size / 10000000 >= 10) {
        fileRef.error = 'common.upload.error_file_too_big';
      }
      this.state.upload.create(file, fileRef.uuid);
      return fileRef;
    });

    this.setState(
      {
        upload: {
          ...this.state.upload,
          filesUploading: [...this.state.upload.filesUploading, ...filesRef],
        },
      },
      () => {
        this.state.upload.setVisibility(false);
      },
    );
  }

  rejected(acceptedOrRejected: File[]) {
    setTimeout(() => {
      const files: any[] = acceptedOrRejected.map(file => ({
        error: 'common.upload.invalid_file',
        lastModified: file.lastModified,
        name: file.name,
        size: file.size,
        type: file.type,
      }));
      this.setState(
        {
          upload: {
            ...this.state.upload,
            filesUploading: [...this.state.upload.filesUploading, ...files],
          },
        },
        () => {
          this.state.upload.setVisibility(false);
        },
      );
    }, 100);
  }

  zone() {}
  componentDidMount() {
    this.onUpload = new events.EventEmitter();
  }

  render() {
    const { upload } = this.state;
    const visible = upload && upload.visible;

    return (
      <Dropzone
        maxSize={10000000}
        accept="image/jpeg, image/jpg, image/png, application/pdf, image/bmp, image/gif, image/png"
        // onClick={this.handleDisableClick}
        onDrop={this.handleDrop}
        onDropAccepted={this.handleDropAccepted}
        onDropRejected={this.handleDropRejected}
      >
        {({ isDragActive, getRootProps, getInputProps }) => {
          return (
            <Provider value={{ upload }}>
              <>
                <UploadWrapper
                  rootProps={getRootProps()}
                  inputProps={getInputProps()}
                  onClickUpload={this.handleClickUpload}
                  visible={isDragActive || visible}
                  drag={isDragActive}
                  setVisibility={this.state.upload.setVisibility}
                >
                  {this.props.children}
                </UploadWrapper>
              </>
            </Provider>
          );
        }}
      </Dropzone>
    );
  }
}

export default compose(
  withApollo,
  injectIntl,
  withRouter,
  Alert.hoc(),
  graphql(createInvoice, { name: 'createInvoice' }),
)(AuthProvider);
