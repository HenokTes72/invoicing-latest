import * as React from 'react';

interface IUploadInterface {
  clearFilesUploading: () => void;
  create: (file: File, uuid: string) => void;
  filesUploading: File[];
  off: (event: (file?: File) => void) => void;
  on: (event: (file?: File) => void) => void;
  setVisibility: (visible: boolean) => void;
  visible: boolean;
}

export interface IUploadContextInterface {
  upload?: IUploadInterface;
}

const { Provider, Consumer } = React.createContext<IUploadContextInterface>({
  upload: {
    clearFilesUploading: () => {},
    create: () => {},
    filesUploading: [],
    off: () => {},
    on: () => {},
    setVisibility: () => {},
    visible: false,
  },
});

export { Provider, Consumer };
