import { SignUp } from 'components/Sign';
import * as React from 'react';
import { RouteComponentProps } from 'react-router';

const SignUpScreen: React.FunctionComponent<RouteComponentProps> = () => (
  <SignUp />
);

export default SignUpScreen;
