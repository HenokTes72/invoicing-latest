import { SignupConfirmEmail } from 'components/Sign';
import * as React from 'react';
import { RouteComponentProps } from 'react-router';

const SignupConfirmEmailScreen: React.FunctionComponent<
  RouteComponentProps
> = () => <SignupConfirmEmail />;

export default SignupConfirmEmailScreen;
