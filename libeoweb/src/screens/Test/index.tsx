import * as Auth from 'context/Auth';
import * as React from 'react';
import { compose } from 'react-apollo';
import { RouteComponentProps } from 'react-router';

interface IProps extends RouteComponentProps, Auth.InjectedProps {}

class TestSuccessScreen extends React.PureComponent<IProps> {
  state = {};

  // constructor(props: any) {
  //   super(props);

  //   this.handleClick = this.click.bind(this);
  // }

  // handleClick = () => {};

  // click() {
  //   this.props.auth &&
  //     this.props.auth.signup &&
  //     this.props.auth.signup({
  //       email: 'nicolas.labbe@adfab.fr',
  //       firstname: 'nicolas',
  //       lastname: 'labbé',
  //       password: 'Libeo19'
  //     });
  // }

  render() {
    return <button>hello</button>;
    // return <button onClick={this.handleClick}>hello</button>;
  }
}

export default TestSuccessScreen;
