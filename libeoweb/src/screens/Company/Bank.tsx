import { Avatar, Col, Row, Table } from 'antd';
import { Icon } from 'components/Assets';
import { IconValue } from 'components/Assets/Icon';
import { Content } from 'components/Layout';
import { Empty } from 'components/Table';
import 'components/Table/Table.module.less';
import { Heading } from 'components/Typo';
import * as BalanceCtx from 'context/Balance';
import { ICompany, IKycStatus } from 'context/Company/types';
import * as User from 'context/User';
import { IUser } from 'context/User/types';
import * as React from 'react';
import { InjectedIntlProps, injectIntl } from 'react-intl';
import { RouteComponentProps } from 'react-router';
import currencies from 'utils/currencies';
import './Bank.module.less';

interface IProps
  extends RouteComponentProps,
    InjectedIntlProps,
    User.InjectedProps {}

interface IState {
  devises: any;
}

class Bank extends React.PureComponent<IProps, IState> {
  state = {
    devises: {},
  };

  constructor(props: any) {
    super(props);
  }

  async componentDidMount() {
    const devises = await currencies.all();
    this.setState({ devises });
  }

  render() {
    const { user, intl } = this.props;
    const devises: any = this.state.devises || {};
    const me: IUser = user && user.data && user.data.me;
    const currentCompany: ICompany = me && me.currentCompany;

    return (
      <Content>
        <Row type="flex">
          <Heading
            icon={IconValue.UserGroup}
            title={'bank.header.title'}
            description={'bank.header.description'}
          />
        </Row>
        {currentCompany && (
          <BalanceCtx.Provider balance>
            <BalanceCtx.Consumer>
              {({ balance }) =>
                currentCompany &&
                balance &&
                balance.data &&
                balance.data.balance && (
                  <Table
                    pagination={false}
                    rowKey="id"
                    bordered={false}
                    dataSource={[
                      {
                        ...currentCompany,
                        balance: `${
                          balance.data.balance.currentBalance
                        }${devises[balance.data.balance.currency] &&
                          devises[balance.data.balance.currency].symbol}`,
                        bank: 'treezor',
                        status: currentCompany.kycStatus,
                      },
                    ]}
                    columns={[
                      {
                        dataIndex: 'status',
                        key: 'status',
                        render: (value: any, row: any, index: any) => {
                          switch (value) {
                            case IKycStatus.VALIDATED:
                              return <Icon value={IconValue.LogoMini} />;
                            case IKycStatus.REFUSED:
                              return <Icon value={IconValue.Cross} />;
                            case IKycStatus.PENDING:
                              return <Icon value={IconValue.Clock} />;
                            default:
                              return <Icon value={IconValue.Clock} />;
                          }
                        },
                        title: intl.formatMessage({
                          id: 'bank.table.status',
                        }),
                      },
                      {
                        dataIndex: 'bank',
                        key: 'bank',
                        title: intl.formatMessage({
                          id: 'bank.table.bank',
                        }),
                      },
                      {
                        dataIndex: 'balance',
                        key: 'balance',
                        title: intl.formatMessage({
                          id: 'bank.table.balance',
                        }),
                      },
                    ]}
                  />
                )
              }
            </BalanceCtx.Consumer>
          </BalanceCtx.Provider>
        )}
      </Content>
    );
  }
}

export default injectIntl(User.hoc()(Bank));
