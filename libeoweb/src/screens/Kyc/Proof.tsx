import { RcFile, UploadFile } from 'antd/lib/upload/interface';
import { Icon } from 'components/Assets';
import { IconValue } from 'components/Assets/Icon';
import UploadDisplay from 'components/Upload/UploadDisplay';
import { DocumentStatus, IDocument } from 'context/Beneficiaries/types';
import * as React from 'react';
import Dropzone from 'react-dropzone';
import { FormattedMessage } from 'react-intl';
import PDF from 'react-pdf-js';
import './Proof.module.less';

interface IProps {
  title: string;
  children?: React.ReactNode;
  childrenAfter?: boolean;
  hasError?: boolean;
  error_text?: string;
  documents?: IDocument[];
  documentsType?: number[];
  onChange?: (file: UploadFile, base64: string) => void;
  onRemove?: (uid: string) => void;
  onCancel?: (documentId: number) => void;
}
interface IState {
  file?: File;
  preview?: string;
}

class Proof extends React.PureComponent<IProps, IState> {
  static defaultProps = {};

  state = {
    file: undefined,
    preview: undefined,
  };

  handleDisableClick: (e: any) => void;
  handleDrop: (acceptedFiles: File[], rejectedFiles: File[]) => void;
  handleDropAccepted: (acceptedOrRejected: File[]) => void;
  // handleDropRejected: (acceptedOrRejected: File[]) => void;
  handleCustomRequest: () => void;

  handleRemove: () => void;
  handleBeforeUpload: (file: RcFile, FileList: RcFile[]) => Promise<boolean>;
  constructor(props: any) {
    super(props);

    this.handleDisableClick = this.disableClick.bind(this);
    this.handleDrop = this.drop.bind(this);
    this.handleDropAccepted = this.accepted.bind(this);
    this.handleCustomRequest = this.customRequest.bind(this);

    this.handleRemove = this.onRemove.bind(this);
    this.handleBeforeUpload = this.beforeUpload.bind(this);
  }

  disableClick(e: any) {
    e.preventDefault();
  }

  customRequest() {
    // disable default ant design upload
    return { abort: () => {} };
  }

  drop() {}

  async accepted(acceptedOrRejected: File[]) {}

  async beforeUpload(file: RcFile, FileList: RcFile[]): Promise<boolean> {
    this.setPreview(file);
    return false;
  }

  async setPreview(file: File) {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onloadend = () => {
      this.setState({
        file,
        preview: reader.result as string,
      });
      this.props.onChange &&
        this.props.onChange(file as any, reader.result as string);
    };
  }

  onRemove() {
    if (typeof this.state.file !== 'undefined') {
      const file: any = this.state.file;
      this.props.onRemove && this.props.onRemove(file.uid);
    }
    this.setState({
      file: undefined,
      preview: undefined,
    });
  }

  render() {
    const {
      hasError,
      error_text,
      title,
      documents,
      onCancel,
      documentsType,
      children,
      childrenAfter,
    } = this.props;
    const preview: any = this.state.preview;

    const documentsFound: IDocument[] = [];
    documents &&
      documents.map((document: IDocument, i: number) => {
        if (
          documentsType &&
          document.documentTypeId &&
          document.documentStatus !== DocumentStatus.CANCELED &&
          documentsType.indexOf(document.documentTypeId) > -1
        ) {
          documentsFound.push(document);
        }
      });

    return (
      <div className={`proof-outer ${hasError ? ' has-error' : ''}`}>
        {documentsFound.map((document: IDocument, i: number) => {
          return (
            <div className="document-proof" key={`${i}`}>
              {document.documentStatus === DocumentStatus.PENDING && (
                <Icon value={IconValue.Clock} />
              )}
              {document.documentStatus === DocumentStatus.VALIDATED && (
                <Icon value={IconValue.Checkmark} />
              )}
              {document.fileName}
              <div
                onClick={onCancel && onCancel.bind(null, document.documentId)}
                className="document-proof-remove"
              >
                <Icon value={IconValue.Cross} />
              </div>
            </div>
          );
        })}
        {!documentsFound ||
          (documentsFound.length === 0 && (
            <>
              {!childrenAfter && children}
              <Dropzone
                maxSize={10000000}
                accept="image/jpeg, image/jpg, image/png, application/pdf, image/bmp, image/gif, image/png"
                onDrop={this.handleDrop}
                onDropAccepted={this.handleDropAccepted}
                // onDropRejected={this.handleDropRejected}
              >
                {({ getRootProps, isDragActive, ...props }) => {
                  return (
                    <div
                      {...getRootProps({
                        // onClick: this.handleDisableClick
                      })}
                      className={`proof-wrapper active${
                        hasError ? ' has-error' : ''
                      }${isDragActive ? ' upload-visible' : 'upload-hidden'}`}
                    >
                      {preview ? (
                        <div className="proof-preview">
                          <Icon
                            onClick={this.handleRemove}
                            className="proof-preview-close"
                            value={IconValue.Cross}
                          />
                          <div className="preview-content">
                            {preview.indexOf('data:application/pdf;base64') >
                            -1 ? (
                              <PDF file={preview} />
                            ) : (
                              <img src={preview} />
                            )}
                          </div>
                        </div>
                      ) : (
                        <UploadDisplay
                          active
                          small
                          multiple={false}
                          title={title}
                          extension={'kyc.beneficiary.upload_extension'}
                          onCustomRequest={this.handleCustomRequest}
                          onBeforeUpload={this.handleBeforeUpload}
                        />
                      )}
                    </div>
                  );
                }}
              </Dropzone>
              {childrenAfter && children}
            </>
          ))}
        <div className="ant-form-explain">
          {hasError && error_text && <FormattedMessage id={error_text} />}
        </div>
      </div>
    );
  }
}

export default Proof;
