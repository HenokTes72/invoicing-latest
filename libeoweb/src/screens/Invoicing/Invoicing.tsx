import { Button } from 'components/Button';
import { Content } from 'components/Layout';
import { IPartners } from 'context/Partners/types';
import { IUser } from 'context/User/types';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import './Invoicing.module.less';

/**
 * Please define types for (props:any) callback
 */

interface IProps {
  me: IUser;
  partners: IPartners;
  onSavePreferences: (props: any) => void; // save json preferences
  onSavePdf: (props: any) => void; // request backend to generate pdf
  onSave: (props: any) => void; // on save/update invoice
}
interface IState {}

class Invoicing extends React.PureComponent<IProps, IState> {
  state = {};

  render() {
    const { me, partners, onSavePreferences, onSavePdf, onSave } = this.props;

    return (
      <Content className="invoicing">
        <Button onClick={onSave.bind(null, {})}>
          <FormattedMessage id="invoicing.btn.test" />
        </Button>
      </Content>
    );
  }
}

export default Invoicing;
