import { Layout } from 'antd';
import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import Invoicing from './Invoicing';
import me from './static/me.json';
import partners from './static/partners.json';

interface IProps extends RouteComponentProps {}

class InvoicingScreen extends React.PureComponent<IProps, {}> {
  state = {};

  handleOnSave: (props: any) => void;
  handleOnPreferences: (props: any) => void;
  handleOnPdf: (props: any) => void;
  constructor(props: IProps) {
    super(props);

    this.handleOnSave = this.onSave;
    this.handleOnPreferences = this.Preferences;
    this.handleOnPdf = this.Pdf;
  }

  async onSave(props: any) {
    try {
      await fetch('/api', {
        // fake api call
        body: props && JSON.stringify(props),
        method: 'POST',
      });
    } catch (e) {
      throw new Error(e);
    }
  }

  async Preferences(props: any) {
    try {
      await fetch('/api', {
        // fake api call
        body: props && JSON.stringify(props),
        method: 'POST',
      });
    } catch (e) {
      throw new Error(e);
    }
  }

  async Pdf(props: any) {
    try {
      await fetch('/api/pdf-url', {
        // fake api call
        body: props && JSON.stringify(props),
        method: 'POST',
      });
    } catch (e) {
      throw new Error(e);
    }
  }

  render() {
    return (
      <Layout>
        <Layout.Content>
          <Invoicing
            me={me.data.me as any}
            partners={partners.data.partners as any}
            onSave={this.handleOnSave}
            onSavePreferences={this.handleOnPreferences}
            onSavePdf={this.handleOnPdf}
          />
        </Layout.Content>
      </Layout>
    );
  }
}

export default InvoicingScreen;
