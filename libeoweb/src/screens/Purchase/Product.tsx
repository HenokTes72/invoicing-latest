import { Col, Row } from 'antd';
import { IInvoice } from 'context/Invoice/types';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import currencies from 'utils/currencies';
import Card from './Card';
import './Product.css';

interface IProps {
  invoice: IInvoice;
}

interface IState {
  devises?: any;
}

class Product extends React.PureComponent<IProps, IState> {
  state = {
    devises: undefined,
  };

  constructor(props: any) {
    super(props);
  }

  async componentDidMount() {
    const devises = await currencies.all();
    this.setState({ devises });
  }

  render() {
    const { invoice } = this.props;
    const { devises } = this.state;
    const currency: any =
      typeof devises !== 'undefined' ? devises[invoice.currency] : {};

    return (
      <Card title="invoice.detail.product_title">
        {invoice && (
          <>
            <Row className="row" type="flex">
              <Col className="row-head" span={8}>
                <FormattedMessage id="invoice.detail.product_ht" />
              </Col>
              <Col className="row-head center" span={8}>
                <FormattedMessage id="invoice.detail.product_vat" />
              </Col>
              <Col className="row-head right" span={8}>
                <FormattedMessage id="invoice.detail.product_tt" />
              </Col>
            </Row>
            <Row className="row" type="flex">
              <Col className="row-body-dark" span={8}>
                {invoice.totalWoT} {currency.symbol}
              </Col>
              <Col className="row-body-dark center" span={8}>
                {Math.round((invoice.total - invoice.totalWoT) * 100) / 100}{' '}
                {currency.symbol}
              </Col>
              <Col className="row-body-dark right" span={8}>
                {invoice.total} {currency.symbol}
              </Col>
            </Row>
          </>
        )}
      </Card>
    );
  }
}

export default Product;
