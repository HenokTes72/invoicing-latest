import { Col, Row } from 'antd';
import { BtnType, Button } from 'components/Button';
import Infinity from 'components/Infinity';
import { Content, RightSideBar } from 'components/Layout';
import { Empty, List } from 'components/Table';
import { Heading } from 'components/Typo';
import * as Balance from 'context/Balance';
import { ICompany, IKycStatus, KycStatus } from 'context/Company/types';
import * as Invoice from 'context/Invoice';
import { IInvoice, InvoiceStatus } from 'context/Invoice/types';
import * as Invoices from 'context/Invoices';
import * as Upload from 'context/Upload';
import * as User from 'context/User';
import moment, { Moment } from 'moment';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { RouteComponentProps } from 'react-router';
import history from 'store/history';
import Detail from './Detail';
import Dialogs, { dialogsType } from './Dialogs';
import SelectContact from './SelectContact';
import './Styles.css';

interface IProps
  extends Upload.InjectedProps,
    User.InjectedProps,
    RouteComponentProps {}
interface IState {
  selectedInvoices?: IInvoice;
  modalVisible?: dialogsType;
  toPayInvoices?: IInvoice;
  code?: string;
  date?: Moment;
}

class PurchaseBills extends React.PureComponent<IProps, IState> {
  state = {
    code: undefined,
    date: undefined,
    modalVisible: undefined,
    selectedInvoices: undefined,
    toPayInvoices: undefined,
  };

  handleUpload: () => void;
  handleOnPayoutContacts: (
    payoutContacts?: (invoiceId: string, contactsIds?: string[]) => void,
    contactsIds?: string[],
  ) => void;
  handleRowClick: (invoice: IInvoice) => void;
  handleClose: () => void;
  handleProcessPay: (
    payout?: (
      invoiceId: string,
      date?: Date,
      code?: string,
    ) => Promise<IInvoice | undefined>,
    checkBalance?: (id: string) => Promise<boolean>,
    updateStatus?: (
      id: string,
      status: InvoiceStatus,
      message?: string,
    ) => Promise<IInvoice | undefined>,
    invoice?: IInvoice,
    type?: dialogsType,
    code?: string,
    date?: Moment,
  ) => void;
  handleOnDelete: (
    remove?: (invoice: IInvoice[], message?: string) => void,
    invoice?: IInvoice,
  ) => void;
  handleOnChangeStatus: (
    updateStatus?: (
      id: string,
      status: InvoiceStatus,
      message?: string,
    ) => Promise<IInvoice | undefined>,
    invoice?: IInvoice,
  ) => void;

  constructor(props: any) {
    super(props);

    this.handleClose = this.close.bind(this);
    this.handleOnPayoutContacts = this.onPayoutContacts.bind(this);
    this.handleProcessPay = this.processPay.bind(this);
    this.handleRowClick = this.rowClick.bind(this);
    this.handleUpload = this.upload.bind(this);
    this.handleOnDelete = this.onDelete.bind(this);
    this.handleOnChangeStatus = this.onChangeStatus.bind(this);
  }

  async onPayoutContacts(
    payoutContacts?: (invoiceId: string, contactsIds?: string[]) => void,
    contactsIds?: string[],
  ) {
    const toPayInvoices: any = this.state.toPayInvoices;
    if (toPayInvoices && payoutContacts) {
      await payoutContacts(toPayInvoices.id, contactsIds);
    }
    this.setState({
      selectedInvoices: undefined,
      toPayInvoices: undefined,
    });
  }

  async onChangeStatus(
    updateStatus?: (
      id: string,
      status: InvoiceStatus,
      message?: string,
    ) => Promise<IInvoice | undefined>,
    invoice?: IInvoice,
  ) {
    let updatedInvoice: IInvoice | undefined;
    if (invoice && updateStatus) {
      updatedInvoice = await updateStatus(invoice.id, InvoiceStatus.Scanned);
    }

    invoice && history.push(`/invoice/draft/${invoice.id}`);
    this.setState({
      selectedInvoices: this.state.selectedInvoices
        ? updatedInvoice
        : undefined,
    });
  }

  onDelete(
    remove?: (invoice: IInvoice[], message?: string) => void,
    invoice?: IInvoice,
  ) {
    invoice && remove && remove([invoice], 'invoice.actions.delete');
    this.setState({ selectedInvoices: undefined });
  }

  async processPay(
    payout?: (
      invoiceId: string,
      date?: Date,
      code?: string,
    ) => Promise<IInvoice | undefined>,
    checkBalance?: (id: string) => Promise<boolean>,
    updateStatus?: (
      id: string,
      status: InvoiceStatus,
      message: string,
    ) => Promise<IInvoice | undefined>,
    invoice?: IInvoice,
    type?: dialogsType,
    code?: string,
    date?: Moment,
  ) {
    const { user } = this.props;
    const currentCompany: ICompany =
      user && user.data && user.data.me && user.data.me.currentCompany;

    if (invoice && invoice.status === InvoiceStatus.Planned && updateStatus) {
      let updatedInvoice: IInvoice | undefined;
      if (invoice && updateStatus) {
        updatedInvoice = await updateStatus(
          invoice.id,
          InvoiceStatus.ToPay,
          'invoice.status.cancel_success',
        );
      }

      return this.setState({
        code: undefined,
        modalVisible: undefined,
        selectedInvoices: this.state.selectedInvoices
          ? updatedInvoice
          : undefined,
        toPayInvoices: undefined,
      });
    }

    let canPay: boolean = true;
    let modalVisible: dialogsType | undefined;
    let validatedCode: string | undefined = this.state.code;
    let toPayInvoices: IInvoice | undefined = invoice;
    switch (type) {
      case 'start':
        validatedCode = undefined;
        canPay = false;
        toPayInvoices = undefined;
        break;
      case 'onboarding':
        validatedCode = undefined;
        canPay = false;
        toPayInvoices = undefined;
        break;
      case 'waiting':
        if (invoice && invoice.total > 2000) {
          canPay = false;
          modalVisible = 'code';
        }
        break;
      case 'transfert':
        modalVisible = undefined;
        break;
      case 'code':
        validatedCode = code;
        break;
      case 'refused':
        if (invoice && invoice.total > 2000) {
          canPay = false;
          modalVisible = 'code';
        }
        break;
      default:
        canPay = false;

        if (
          (!currentCompany || !currentCompany.kycStatus) &&
          currentCompany.kycStep !== 'IBAN'
        ) {
          modalVisible = 'start';
        } else if (
          KycStatus[currentCompany.kycStatus] < KycStatus[IKycStatus.PENDING]
        ) {
          modalVisible = 'onboarding';
        } else if (currentCompany.kycStatus === IKycStatus.PENDING) {
          modalVisible = 'waiting';
        } else if (currentCompany.kycStatus === IKycStatus.REFUSED) {
          modalVisible = 'refused';
        } else if (invoice && invoice.total > 2000) {
          modalVisible = 'code';
        } else {
          canPay = true;
        }
        break;
    }

    let selectedInvoices: IInvoice | undefined;
    if (
      type !== 'transfert' &&
      modalVisible !== 'code' &&
      currentCompany &&
      currentCompany.kycStatus !== IKycStatus.REFUSED &&
      canPay &&
      checkBalance &&
      invoice
    ) {
      canPay = await checkBalance(invoice.id);
      if (!canPay) {
        modalVisible = 'transfert';
      }

      // can pay here
      if (toPayInvoices && payout) {
        selectedInvoices = await payout(
          toPayInvoices.id,
          (date && date.toDate()) ||
            this.state.date ||
            moment(toPayInvoices.dueDate).toDate(),
          code,
        );
        if (!selectedInvoices) {
          return this.setState({
            code: undefined,
            date: undefined,
            modalVisible: undefined,
            selectedInvoices: undefined,
            toPayInvoices: undefined,
          });
        }
      }
    }

    const newState = {
      code: validatedCode,
      date,
      modalVisible,
      selectedInvoices,
      toPayInvoices,
    };
    this.setState(newState);
  }

  upload = () => {
    const { upload } = this.props;
    const setVisibility = upload && upload.setVisibility;
    if (setVisibility) {
      setVisibility(true);
    }
  };

  rowClick(invoice: IInvoice) {
    this.setState({ selectedInvoices: invoice });
  }

  close() {
    this.setState({
      code: undefined,
      modalVisible: undefined,
      selectedInvoices: undefined,
      toPayInvoices: undefined,
    });
  }

  more = async (fetchMore: any, total: number, length: number) => {
    if (fetchMore) {
      await fetchMore({
        updateQuery: (prev: any, { fetchMoreResult }: any) => {
          if (!fetchMoreResult.invoices) {
            return prev;
          }

          return {
            ...prev,
            invoices: {
              ...prev.invoices,
              rows: [...prev.invoices.rows, ...fetchMoreResult.invoices.rows],
            },
          };
        },
        variables: {
          limit: 10,
          offset: length || 0,
        },
      });
    }
  };

  render() {
    const { modalVisible } = this.state;
    let selectedInvoices: any;
    let toPayInvoices: any;

    let sidebar: React.ReactNode;
    if (typeof this.state.selectedInvoices !== 'undefined') {
      selectedInvoices = this.state.selectedInvoices;
    }
    if (typeof this.state.toPayInvoices !== 'undefined') {
      toPayInvoices = this.state.toPayInvoices;
    }

    return (
      <Balance.Provider>
        <Balance.Consumer>
          {({ balance }) => {
            return (
              <Invoices.Provider
                offset={0}
                limit={12}
                filters={{
                  enabled: true,
                  status: [
                    InvoiceStatus.ToPay,
                    InvoiceStatus.Planned,
                    InvoiceStatus.Paid,
                  ],
                }}
              >
                <Invoices.Consumer>
                  {data => {
                    const dataInvoices = data.invoices && data.invoices.data;
                    const remove = data.invoices && data.invoices.delete;
                    const updateStatus =
                      data.invoices && data.invoices.updateStatus;

                    const invoices = dataInvoices && dataInvoices.invoices;

                    const hasMore =
                      invoices &&
                      invoices.rows &&
                      invoices.total &&
                      invoices.rows.length < invoices.total;

                    return (
                      <Invoice.Provider>
                        <Invoice.Consumer>
                          {({ invoice }) => {
                            const generateCode =
                              invoice && invoice.generateCode;
                            const payout = invoice && invoice.payout;
                            const payoutContacts =
                              invoice && invoice.payoutContacts;

                            if (!modalVisible && toPayInvoices) {
                              sidebar = (
                                <SelectContact
                                  onSubmit={this.handleOnPayoutContacts.bind(
                                    null,
                                    payoutContacts,
                                  )}
                                  invoice={toPayInvoices}
                                />
                              );
                            }
                            const actions = [
                              {
                                handle: this.handleOnChangeStatus.bind(
                                  null,
                                  updateStatus,
                                ),
                                type: 'control',
                              },
                              {
                                type: 'download',
                              },
                              {
                                handle: this.handleOnDelete.bind(null, remove),
                                type: 'delete',
                              },
                              {
                                handle: this.handleProcessPay.bind(
                                  null,
                                  payout,
                                  balance && balance.checkBalance,
                                  updateStatus,
                                ),
                                type: 'pay-now',
                              },
                              {
                                handle: this.handleProcessPay.bind(
                                  null,
                                  payout,
                                  balance && balance.checkBalance,
                                  updateStatus,
                                ),
                                type: 'pay-later',
                              },
                            ];

                            if (selectedInvoices) {
                              sidebar = (
                                <Invoice.Provider id={selectedInvoices.id}>
                                  <Invoice.Consumer>
                                    {props => {
                                      return (
                                        <Detail
                                          onChangeStatus={this.handleProcessPay.bind(
                                            null,
                                            payout,
                                            balance && balance.checkBalance,
                                            updateStatus,
                                            selectedInvoices,
                                          )}
                                          actions={actions}
                                          invoice={
                                            (props.invoice &&
                                              props.invoice.data &&
                                              props.invoice.data.invoice) ||
                                            selectedInvoices
                                          }
                                        />
                                      );
                                    }}
                                  </Invoice.Consumer>
                                </Invoice.Provider>
                              );
                            }

                            return (
                              <RightSideBar
                                closable
                                onClose={this.handleClose}
                                sidebar={sidebar}
                              >
                                <Infinity
                                  hasMore={hasMore}
                                  loadMore={this.more.bind(
                                    null,
                                    data.invoices &&
                                      data.invoices.data &&
                                      data.invoices.data.fetchMore,
                                    (invoices && invoices.total) || 0,
                                    invoices && invoices.rows
                                      ? invoices.rows.length
                                      : 0,
                                  )}
                                >
                                  <Content>
                                    <Dialogs
                                      onClose={this.handleProcessPay.bind(
                                        null,
                                        payout,
                                        balance && balance.checkBalance,
                                        updateStatus,
                                        toPayInvoices,
                                      )}
                                      invoiceId={
                                        toPayInvoices && toPayInvoices.id
                                      }
                                      generateCode={generateCode}
                                      modalVisible={modalVisible}
                                    />
                                    <Row type="flex">
                                      <Heading
                                        button="dashboard.header.upload_btn"
                                        onClick={this.handleUpload}
                                        title="purchase.control.bills_title"
                                        description={
                                          invoices && invoices.total > 0
                                            ? 'purchase.control.bills_number_of_import_to_control'
                                            : undefined
                                        }
                                        descriptionVariables={{
                                          count: invoices && invoices.total,
                                        }}
                                      />
                                    </Row>
                                    <Row>
                                      <Col>
                                        <List
                                          empty={
                                            <Empty>
                                              <Button
                                                type={BtnType.Primary}
                                                onClick={this.upload}
                                              >
                                                <FormattedMessage id="purchase.control.upload_more" />
                                              </Button>
                                            </Empty>
                                          }
                                          selectedId={
                                            selectedInvoices &&
                                            selectedInvoices.id
                                          }
                                          onCtaClick={this.handleProcessPay.bind(
                                            null,
                                            payout,
                                            balance && balance.checkBalance,
                                            updateStatus,
                                          )}
                                          invoices={invoices}
                                          className={
                                            !sidebar
                                              ? 'list-large'
                                              : 'list-small'
                                          }
                                          onClickRow={this.handleRowClick}
                                          selectable
                                          headers={
                                            !sidebar
                                              ? [
                                                  { key: 'number' },
                                                  { key: 'receiverTitle' },
                                                  { key: 'dueDate' },
                                                  { key: 'companyEmitter' },
                                                  { key: 'total' },
                                                  { key: 'status' },
                                                  { key: 'cta' },
                                                  {
                                                    actions,
                                                    key: 'actions',
                                                  },
                                                ]
                                              : [
                                                  { key: 'number' },
                                                  { key: 'dueDate' },
                                                  { key: 'companyEmitter' },
                                                  { key: 'total' },
                                                  { key: 'status' },
                                                ]
                                          }
                                        />
                                      </Col>
                                    </Row>
                                  </Content>
                                </Infinity>
                              </RightSideBar>
                            );
                          }}
                        </Invoice.Consumer>
                      </Invoice.Provider>
                    );
                  }}
                </Invoices.Consumer>
              </Invoices.Provider>
            );
          }}
        </Balance.Consumer>
      </Balance.Provider>
    );
  }
}

export default Upload.hoc()(User.hoc()(PurchaseBills));
