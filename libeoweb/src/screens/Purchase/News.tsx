import { Col, Row } from 'antd';
import { Icon } from 'components/Assets';
import { IconValue } from 'components/Assets/Icon';
import { IHistories } from 'context/Common/types';
import { InvoiceStatus } from 'context/Invoice/types';
import moment from 'moment';
import * as React from 'react';
import { FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';
import Card from './Card';
import './News.module.less';

interface IProps extends InjectedIntlProps {
  history: IHistories;
}
interface IState {}

class News extends React.PureComponent<IProps, IState> {
  state = {};

  constructor(props: any) {
    super(props);
  }

  render() {
    const { history, intl } = this.props;

    const rows =
      history &&
      history.rows &&
      history.rows.map((status, i) => {
        let icon: React.ReactNode;
        let visible: boolean = false;
        let label: string = `invoice.status.${status.event}_${
          status.params.status
        }`;

        switch (status.params.status) {
          case InvoiceStatus.Importing:
            icon = <Icon value={IconValue.Buy} />;
            visible = true;
            break;
          case InvoiceStatus.Imported:
            icon = <Icon value={IconValue.CloudUpload} />;
            visible = true;
            break;
          case InvoiceStatus.Scanned:
            if (status.params.oldStatus === InvoiceStatus.ToPay) {
              label = `${label}_EDITED`;
            }
            icon = <Icon value={IconValue.Search} />;
            visible = true;
            break;
          case InvoiceStatus.ToPay:
            if (status.params.oldStatus === InvoiceStatus.Planned) {
              label = `${label}_CANCEL`;
            }
            icon = (
              <Icon
                value={
                  status.params.oldStatus === InvoiceStatus.Planned
                    ? IconValue.Cross
                    : IconValue.ThumbsUp
                }
              />
            );
            visible = true;
            break;
          case InvoiceStatus.BusinessValidation:
            icon = <Icon value={IconValue.CloudUpload} />;
            visible = true;
            break;
          case InvoiceStatus.Planned:
            icon = <Icon value={IconValue.Clock} />;
            visible = true;
            break;
          case InvoiceStatus.MarkedAsPaidByReceiver:
            icon = <Icon value={IconValue.CloudUpload} />;
            visible = true;
            break;
        }

        return visible ? (
          <Row key={`${i}`} className="row news-row" type="flex">
            <Col>{icon}</Col>
            <Col
              style={{
                flex: 1,
              }}
            >
              <div>
                <span className="value">
                  <FormattedMessage
                    id={label}
                    values={{
                      firstname: status.user.firstname,
                      lastname: status.user.lastname,
                      paymentAt: moment(status.params.paymentAt).format(
                        'DD/MM/YYYY',
                      ),
                    }}
                  />
                </span>
              </div>
              <div>
                <span className="date">
                  {moment(status.createdAt).format('DD/MM/YYYY hh:mm')}
                </span>
              </div>
            </Col>
          </Row>
        ) : null;
      });

    return <Card title="invoice.detail.news_title">{rows}</Card>;
  }
}

export default injectIntl(News);
