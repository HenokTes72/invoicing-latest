import { Icon } from 'components/Assets';
import { IconValue } from 'components/Assets/Icon';
import { Div } from 'components/Typo';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import currencies from 'utils/currencies';

interface IProps {
  amount: number;
  currency: string;
}

interface IState {
  devises?: any;
}

class BalanceCard extends React.PureComponent<IProps, IState> {
  state = {
    devises: undefined,
  };

  async componentDidMount() {
    const devises = await currencies.all();
    this.setState({ devises });
  }

  render() {
    const { amount, currency } = this.props;
    const devises: any = this.state.devises;
    const value = devises && devises[currency] && devises[currency].symbol;

    return (
      <div className="balance-card">
        <Div
          css={{
            bold: true,
            fontSize: '12px',
            lightColor: true,
          }}
        >
          <FormattedMessage id="balance.card.your_balance" />
        </Div>
        <Div
          css={{
            bold: true,
            fontSize: '35px',
          }}
        >
          {amount} {value}
        </Div>
        <Div
          className="footer"
          css={{
            bold: true,
            fontSize: '11px',
            primaryColor: true,
            uppercase: true,
          }}
        >
          <FormattedMessage id="balance.card.tansfert" />
          <div className="dot3-wrapper">
            <Icon value={IconValue.Dots3} />
          </div>
        </Div>
      </div>
    );
  }
}

export default BalanceCard;
