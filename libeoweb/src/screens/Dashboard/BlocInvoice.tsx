import { Col } from 'antd';
import { IconValue } from 'components/Assets/Icon';
import { CondensedCard } from 'components/Card';
import { InvoiceStatus } from 'context/Invoice/types';
import * as Invoices from 'context/Invoices';
import * as React from 'react';

interface IProps {
  status: InvoiceStatus[];
  color?: string;
  icon: IconValue;
  description: string;
}
interface IState {}

class BlocInvoice extends React.PureComponent<IProps, IState> {
  state = {};

  render() {
    const { status, description, color, icon } = this.props;
    return (
      <Invoices.Provider offset={0} limit={1} count={status}>
        <Invoices.Consumer>
          {data => {
            const count =
              data.invoices &&
              data.invoices.count &&
              data.invoices.count.invoices;

            return (
              <Col span={6}>
                <CondensedCard
                  center
                  className="bloc-invoice-count"
                  color={color}
                  title={count ? count.total : ''}
                  description={description}
                  icon={icon}
                />
              </Col>
            );
          }}
        </Invoices.Consumer>
      </Invoices.Provider>
    );
  }
}

export default BlocInvoice;
