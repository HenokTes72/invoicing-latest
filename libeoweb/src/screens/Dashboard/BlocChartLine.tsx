import { Card } from 'components/Card';
import { Reline } from 'components/Chart';
import * as Balance from 'context/Balance';
import { IInvoice, InvoiceStatus } from 'context/Invoice/types';
import * as Invoices from 'context/Invoices';
import moment from 'moment';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';

interface IProps {}
interface IState {}

class BlocChart extends React.PureComponent<IProps, IState> {
  state = {};

  render() {
    return (
      <Balance.Provider balance>
        <Balance.Consumer>
          {({ balance }) => {
            const amount =
              balance && balance.data && balance.data.balance
                ? balance.data.balance.currentBalance
                : 0;

            // const amount:number = 500;

            return (
              <Invoices.Provider
                offset={0}
                limit={100}
                filters={{
                  enabled: true,
                  status: [InvoiceStatus.ToPay, InvoiceStatus.Planned],
                }}
              >
                <Invoices.Consumer>
                  {dataInvoices => {
                    const invoices =
                      dataInvoices.invoices &&
                      dataInvoices.invoices.data &&
                      dataInvoices.invoices.data.invoices;

                    const rows =
                      invoices && invoices.rows
                        ? invoices.rows.sort(
                            (a: IInvoice, b: IInvoice) =>
                              new Date(a.dueDate).getTime() -
                              new Date(b.dueDate).getTime(),
                          )
                        : [];

                    let payout = amount;
                    const today = moment(new Date());
                    const lines = [
                      {
                        x: moment(today)
                          .subtract(1, 'days')
                          .format('DD/MM/YYYY'),
                        y: payout,
                      },
                    ];
                    rows.map((row: IInvoice) => {
                      const currentDate = moment(row.dueDate).format(
                        'DD/MM/YYYY',
                      );
                      const dueDate: moment.Moment = moment(row.dueDate);
                      if (dueDate.isSameOrAfter(today)) {
                        payout = Math.round((payout - row.total) * 100) / 100;
                        lines.push({
                          x: currentDate,
                          y: payout,
                        });
                      }
                    });

                    return (
                      <Card
                        title={
                          <FormattedMessage id="dashboard.chart.line_title" />
                        }
                        titleAlign="left"
                        center
                        shadow
                      >
                        <div className="chart-dashboard-line">
                          <div className="chart-dashboard-solde">
                            <FormattedMessage
                              id="dashboard.chart.line_solde"
                              values={{ solde: amount }}
                            />
                          </div>
                          <div className="chart-dashboard-description">
                            <FormattedMessage
                              id="dashboard.chart.line_solde_waiting"
                              values={{ solde: payout }}
                            />
                          </div>
                          <div className="chart-wrapper-outer">
                            <Reline lines={lines} amount={amount} />
                          </div>
                        </div>
                      </Card>
                    );
                  }}
                </Invoices.Consumer>
              </Invoices.Provider>
            );
          }}
        </Balance.Consumer>
      </Balance.Provider>
    );
  }
}

export default BlocChart;
