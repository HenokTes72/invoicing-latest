import { Col, Row, Steps } from 'antd';
import { IconValue } from 'components/Assets/Icon';
import { Button } from 'components/Button';
import { Content } from 'components/Layout';
import { Heading } from 'components/Typo';
import { ICompany, IKycStatus, KycStatus } from 'context/Company/types';
import { InvoiceStatus } from 'context/Invoice/types';
import * as Invoices from 'context/Invoices';
import * as Upload from 'context/Upload';
import * as User from 'context/User';
import { IUser } from 'context/User/types';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { Link } from 'react-router-dom';
import BlocChartLine from './BlocChartLine';
import BlocChartPie from './BlocChartPie';
import BlocInvoice from './BlocInvoice';
import BlocOnboardingProgress from './BlocOnboardingProgress';
import BlocScanFacture from './BlocScanFacture';
import './Dashboard.module.less';

interface IProps extends User.InjectedProps, Upload.InjectedProps {}
interface IState {}

class Dashboard extends React.PureComponent<IProps, IState> {
  state = {};

  handleUpload: () => void;

  constructor(props: any) {
    super(props);

    this.handleUpload = this.upload.bind(this);
  }

  upload() {
    const { upload } = this.props;
    const setVisibility = upload && upload.setVisibility;
    if (setVisibility) {
      setVisibility(true);
    }
  }

  render() {
    const { user } = this.props;
    const me: IUser = user && user.data && user.data.me;
    const currentCompany: ICompany = me && me.currentCompany;

    const showBlocOnboarding: boolean =
      !currentCompany || currentCompany.kycStatus !== IKycStatus.VALIDATED;

    return (
      <Content className="dashboard">
        <Row
          type="flex"
          style={{
            justifyContent: 'flex-end',
          }}
        >
          {me &&
            (showBlocOnboarding ? (
              <Heading
                title="dashboard.incomplete.header_title"
                titleVariables={me}
                description="dashboard.incomplete.header_description"
                button="dashboard.header.upload_btn"
                onClick={this.handleUpload}
              />
            ) : (
              <Heading
                title="dashboard.complete.header_title"
                titleVariables={me}
                description="dashboard.complete.header_description"
                button="dashboard.header.upload_btn"
                onClick={this.handleUpload}
              />
            ))}
        </Row>
        {showBlocOnboarding && (
          <Row gutter={28} style={{ marginTop: 28 }}>
            <Col span={18}>
              <BlocOnboardingProgress />
            </Col>
          </Row>
        )}
        <Invoices.Provider
          offset={0}
          limit={1}
          count={[
            InvoiceStatus.Importing,
            InvoiceStatus.Imported,
            InvoiceStatus.Scanning,
            InvoiceStatus.Scanned,
            InvoiceStatus.ToPay,
            InvoiceStatus.Planned,
            InvoiceStatus.Paid,
          ]}
        >
          <Invoices.Consumer>
            {data => {
              const invoices =
                data.invoices &&
                data.invoices.count &&
                data.invoices.count.invoices;

              if (!invoices) {
                return;
              }

              return invoices && invoices.total > 0 ? (
                <>
                  <Row gutter={28} style={{ marginTop: 28 }}>
                    <Link to="/purchase/draft">
                      <BlocInvoice
                        status={[InvoiceStatus.Scanning, InvoiceStatus.Scanned]}
                        color="primary"
                        description="dashboard.bloc.invoice_to_control"
                        icon={IconValue.Search}
                      />
                    </Link>
                    <Link to="/purchase/bills">
                      <BlocInvoice
                        status={[InvoiceStatus.ToPay, InvoiceStatus.Planned]}
                        color="red"
                        description="dashboard.bloc.invoice_to_pay"
                        icon={IconValue.WalletOut}
                      />
                    </Link>
                  </Row>
                  <Row type="flex" gutter={28} style={{ marginTop: 28 }}>
                    <Col span={12}>
                      <BlocChartLine />
                    </Col>
                    <Col span={12}>
                      <BlocChartPie />
                    </Col>
                  </Row>
                </>
              ) : (
                <Row gutter={28} style={{ marginTop: 28 }}>
                  <Col span={18}>
                    <BlocScanFacture />
                  </Col>
                </Row>
              );
            }}
          </Invoices.Consumer>
        </Invoices.Provider>
      </Content>
    );
  }
}

export default User.hoc()(Upload.hoc()(Dashboard));
