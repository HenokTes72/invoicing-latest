import { Card } from 'components/Card';
import { Repie } from 'components/Chart';
import { IInvoice, InvoiceStatus } from 'context/Invoice/types';
import * as Invoices from 'context/Invoices';
import moment from 'moment';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';

interface IProps {}
interface IState {}

class BlocChart extends React.PureComponent<IProps, IState> {
  state = {};

  render() {
    return (
      <Invoices.Provider
        offset={0}
        limit={100}
        filters={{
          enabled: true,
          status: [InvoiceStatus.ToPay, InvoiceStatus.Planned],
        }}
      >
        <Invoices.Consumer>
          {dataInvoices => {
            const invoices =
              dataInvoices.invoices &&
              dataInvoices.invoices.data &&
              dataInvoices.invoices.data.invoices;

            const rows =
              invoices && invoices.rows
                ? invoices.rows.sort(
                    (a: IInvoice, b: IInvoice) =>
                      new Date(a.dueDate).getTime() -
                      new Date(b.dueDate).getTime(),
                  )
                : [];

            const today = moment(new Date());
            const data: any[] = [];
            rows.map((row: IInvoice) => {
              const dueDate: moment.Moment = moment(row.dueDate);
              if (dueDate.isSameOrAfter(today)) {
                data.push(row);
              }
            });

            return (
              <Card
                title={<FormattedMessage id="dashboard.chart.pie_title" />}
                titleAlign="left"
                center
                shadow
              >
                <div className="chart-dashboard-pie">
                  <div className="chart-wrapper-outer">
                    <Repie rows={data} />
                  </div>
                </div>
              </Card>
            );
          }}
        </Invoices.Consumer>
      </Invoices.Provider>
    );
  }
}

export default BlocChart;
