import { SignUpSuccess } from 'components/Sign';
import * as React from 'react';
import { RouteComponentProps } from 'react-router';

const SignUpSuccessScreen: React.FunctionComponent<
  RouteComponentProps
> = () => <SignUpSuccess />;

export default SignUpSuccessScreen;
