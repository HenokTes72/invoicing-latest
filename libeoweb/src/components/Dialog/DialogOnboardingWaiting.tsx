import img from 'assets/images/onboarding-waiting.svg';
import { IconValue } from 'components/Assets/Icon';
import * as React from 'react';
import Dialog, { IDialogProps, IDialogState } from './Dialog';
const Img: any = img;

/**
 * @props
 */
interface IProps extends IDialogProps {
  iban?: string;
}
interface IState extends IDialogState {}

class DialogOnboardingWaiting extends React.PureComponent<IProps, IState> {
  static defaultProps = {
    className: 'alert',
    closable: true,
    description: 'dialog.onboarding_waiting.description',
    footerClick: () => {},
    footerIcon: IconValue.Information,
    footerTitle: 'dialog.onboarding_waiting.contact',
    icon: IconValue.Lock,
    img: <Img />,
    title: 'dialog.onboarding_waiting.title',
  };

  footerClick(e: React.MouseEvent<any>) {}

  render() {
    const { iban, className, ...rest } = this.props;

    return (
      <Dialog className={`onboarding-waiting-dialog ${className}`} {...rest} />
    );
  }
}

export default DialogOnboardingWaiting;
