import img from 'assets/images/complete-onboarding.svg';
import { IconValue } from 'components/Assets/Icon';
import * as React from 'react';
import Dialog, { IDialogProps, IDialogState } from './Dialog';
const Img: any = img;

/**
 * @props
 */
interface IProps extends IDialogProps {
  iban?: string;
}
interface IState extends IDialogState {}

class DialogOnboarding extends React.PureComponent<IProps, IState> {
  static defaultProps = {
    className: 'alert',
    closable: true,
    description: 'dialog.onboarding.description',
    icon: IconValue.Alert,
    img: <Img />,
    link: '/kyc',
    linkTitle: 'dialog.onboarding.link',
    title: 'dialog.onboarding.title',
  };

  render() {
    const { iban, className, ...rest } = this.props;
    return <Dialog className={`onboarding-dialog ${className}`} {...rest} />;
  }
}

export default DialogOnboarding;
