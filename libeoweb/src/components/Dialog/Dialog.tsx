import { Modal } from 'antd';
import { ModalProps } from 'antd/lib/modal';
import { Icon } from 'components/Assets';
import { IconValue } from 'components/Assets/Icon';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { Link } from 'react-router-dom';

/**
 * @props
 */
export interface IDialogProps extends ModalProps {
  invoiceId?: string;
  description?: string;
  icon?: IconValue;
  img?: any;
  link?: string;
  linkTitle?: string;
  title?: string;
  footerIcon?: IconValue;
  footerTitle?: string;
  footerVisible?: boolean;
  footerClick?: () => void;
}
export interface IDialogState {}

class Dialog extends React.PureComponent<IDialogProps, IDialogState> {
  static defaultProps: Partial<IDialogProps> = {
    footerVisible: true,
  };

  render() {
    const {
      children,
      title,
      closable,
      description,
      img,
      icon,
      link,
      linkTitle,
      className,
      footerVisible,
      footerIcon,
      footerTitle,
      footerClick,
      ...rest
    } = this.props;

    return (
      <Modal
        closable={false}
        className={`dialog${className ? ` ${className}` : ''}`}
        title={
          title && (
            <>
              {icon && <Icon value={icon} />}
              <FormattedMessage id={title} />
            </>
          )
        }
        footer={null}
        {...rest}
      >
        {closable && (
          <div onClick={this.props.onCancel} className="close-dialog">
            <Icon value={IconValue.Cross} />
          </div>
        )}
        {img && <div className="img-dialog">{img}</div>}
        {description && (
          <div className="description-dialog">
            <FormattedMessage id={description} />
          </div>
        )}
        <div className="body-dialog">
          {children}
          {linkTitle && link && (
            <div className="ant-btn ant-btn-primary">
              <Link to={link}>
                <FormattedMessage id={linkTitle} />
              </Link>
            </div>
          )}
        </div>
        {footerVisible && (
          <div
            onClick={footerClick || this.props.onCancel}
            className="later-dialog"
          >
            <Icon value={footerIcon || IconValue.TimeReverse} />
            <FormattedMessage id={footerTitle || 'dialog.footer.do_it_later'} />
          </div>
        )}
      </Modal>
    );
  }
}

export default Dialog;
