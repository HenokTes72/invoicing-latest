import Form, { FormComponentProps } from 'antd/lib/form';
import img from 'assets/images/insufficient-money.svg';
import { IconValue } from 'components/Assets/Icon';
import { Code } from 'components/Form';
import Iban from 'components/Form/Iban';
import * as Invoice from 'context/Invoice';
import * as React from 'react';
import { compose } from 'react-apollo';
import Dialog, { IDialogProps, IDialogState } from './Dialog';
const Img: any = img;

/**
 * @props
 */
interface IProps extends IDialogProps, FormComponentProps {
  invoiceId?: string;
  onComplete?: (code: string) => void;
  generateCode?: (id: string) => void;
}
interface IState extends IDialogState {
  loading: boolean;
}

class DialogCode extends React.PureComponent<IProps, IState> {
  static defaultProps = {
    className: 'alert',
    description: 'dialog.code.description',
    footerIcon: IconValue.Clockwise,
    footerTitle: 'dialog.code.send',
    icon: IconValue.Wallet,
    img: <Img />,
    title: 'dialog.code.title',
  };

  state = {
    loading: false,
  };

  handleFooterClick: () => void;
  handleOnComplete: (value?: string) => void;
  constructor(props: IProps) {
    super(props);
    this.handleOnComplete = this.complete.bind(this);
    this.handleFooterClick = this.footerClick.bind(this);
  }

  footerClick = async () => {
    this.setState({ loading: true });
    await this.generateCode();
    this.setState({ loading: false });
  };

  complete(value?: string) {
    const { onComplete } = this.props;
    value && onComplete && onComplete(value);
  }

  generateCode = async () => {
    const { generateCode, invoiceId } = this.props;
    if (generateCode && invoiceId) {
      await generateCode(invoiceId);
    }
  };

  componentDidMount() {
    this.generateCode();
  }

  render() {
    const { onComplete, className, form, ...rest } = this.props;
    const { loading } = this.state;

    return (
      <Dialog
        footerClick={this.handleFooterClick}
        className={`code-dialog ${className} ${loading ? ' loading' : ''}`}
        {...rest}
        footerTitle={
          !loading ? rest.footerTitle : `${rest.footerTitle}_loading`
        }
      >
        <Code onComplete={this.handleOnComplete} form={form} />
      </Dialog>
    );
  }
}

export default compose(Form.create({}))(DialogCode);
