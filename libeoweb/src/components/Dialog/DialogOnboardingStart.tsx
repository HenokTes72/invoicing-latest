import img from 'assets/images/onboarding-start.svg';
import { IconValue } from 'components/Assets/Icon';
import * as React from 'react';
import Dialog, { IDialogProps, IDialogState } from './Dialog';
const Img: any = img;

/**
 * @props
 */
interface IProps extends IDialogProps {
  iban?: string;
}
interface IState extends IDialogState {}

class DialogOnboardingStart extends React.PureComponent<IProps, IState> {
  static defaultProps = {
    className: 'info',
    closable: true,
    description: 'dialog.onboarding_start.description',
    icon: IconValue.Rocket,
    img: <Img />,
    link: '/kyc',
    linkTitle: 'dialog.onboarding.link',
    title: 'dialog.onboarding_start.title',
  };

  render() {
    const { iban, className, ...rest } = this.props;

    return (
      <Dialog className={`onboarding-start-dialog ${className}`} {...rest} />
    );
  }
}

export default DialogOnboardingStart;
