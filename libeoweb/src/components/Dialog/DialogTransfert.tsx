import img from 'assets/images/insufficient-money.svg';
import { IconValue } from 'components/Assets/Icon';
import Iban from 'components/Form/Iban';
import * as React from 'react';
import Dialog, { IDialogProps, IDialogState } from './Dialog';
const Img: any = img;

/**
 * @props
 */
interface IProps extends IDialogProps {
  iban?: string;
}
interface IState extends IDialogState {}

class DialogTransfert extends React.PureComponent<IProps, IState> {
  static defaultProps = {
    className: 'alert',
    closable: true,
    description: 'dialog.transfert.description',
    icon: IconValue.Wallet,
    img: <Img />,
    title: 'dialog.transfert.title',
  };

  render() {
    const { iban, className, ...rest } = this.props;

    return (
      <Dialog className={`transfert-dialog ${className}`} {...rest}>
        <Iban />
      </Dialog>
    );
  }
}

export default DialogTransfert;
