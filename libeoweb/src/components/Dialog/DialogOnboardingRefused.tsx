import img from 'assets/images/onboarding-refused.svg';
import { IconValue } from 'components/Assets/Icon';
import * as React from 'react';
import Dialog, { IDialogProps, IDialogState } from './Dialog';
const Img: any = img;

/**
 * @props
 */
interface IProps extends IDialogProps {
  iban?: string;
}
interface IState extends IDialogState {}

class DialogOnboardingRefused extends React.PureComponent<IProps, IState> {
  static defaultProps = {
    className: 'alert',
    closable: true,
    description: 'dialog.onboarding_refused.description',
    footerClick: () => {},
    img: <Img />,
    link: '#',
    linkTitle: 'dialog.onboarding_refused.contact',
    title: 'dialog.onboarding_refused.title',
  };

  footerClick(e: React.MouseEvent<any>) {}

  render() {
    const { iban, className, ...rest } = this.props;

    return (
      <Dialog className={`onboarding-refused-dialog ${className}`} {...rest} />
    );
  }
}

export default DialogOnboardingRefused;
