import './Styles.module.less';

import Dialog from './Dialog';
import DialogCode from './DialogCode';
import DialogOnboarding from './DialogOnboarding';
import DialogOnboardingWaiting from './DialogOnboardingWaiting';
import DialogOnboardingStart from './DialogOnboardingStart';
import DialogOnboardingRefused from './DialogOnboardingRefused';
import DialogTransfert from './DialogTransfert';

export {
  Dialog,
  DialogCode,
  DialogOnboarding,
  DialogOnboardingWaiting,
  DialogOnboardingStart,
  DialogOnboardingRefused,
  DialogTransfert
};
