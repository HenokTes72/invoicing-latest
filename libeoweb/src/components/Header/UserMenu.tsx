import { Col, Menu, Row } from 'antd';
import * as Auth from 'context/Auth';
import { ICompany } from 'context/Company/types';
import * as User from 'context/User';
import * as React from 'react';
import { FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';

import { IconValue } from 'components/Assets/Icon';

import { Icon } from 'components/Assets';
import MenuItem from './MenuItem';
import SubMenuItem from './SubMenuItem';

const MenuDivider = Menu.Divider;

interface IProps
  extends Auth.InjectedProps,
    User.InjectedProps,
    InjectedIntlProps {}

interface IState {}

class UserMenu extends React.PureComponent<IProps, IState> {
  state = {};

  handleSignout: () => void;

  constructor(props: any) {
    super(props);
    this.handleSignout = this.signout.bind(this);
  }

  async signout() {
    await (this.props.auth &&
      this.props.auth.signout &&
      this.props.auth.signout());
  }

  render() {
    const { user } = this.props;
    const me = user && user.data && user.data.me;
    const currentCompany =
      me && me.currentCompany ? me.currentCompany : { id: null };
    const companies = me && me.companies && me.companies.rows;

    return (
      <Menu
        className="user-menu"
        mode="inline"
        style={{ height: '100%', borderRight: 0 }}
      >
        {/* menu solde */}
        <MenuItem
          key={'solde:1'}
          className="header-solde-title"
          title="header.menu.solde_libeo"
          href="/balance"
        />
        <MenuDivider />
        {/* menu profil */}
        <SubMenuItem
          key="profil:2"
          icon={IconValue.Profile}
          title="header.menu.profile"
        >
          <Menu.Item key="profil:2:1" className="with-list">
            <FormattedMessage id="header.menu.profile_info" />
          </Menu.Item>
          <Menu.Item key="profil:2:2" className="with-list">
            <FormattedMessage id="header.menu.profile_contacts" />
          </Menu.Item>
          <Menu.Item key="profil:2:3" className="with-list">
            <FormattedMessage id="header.menu.profile_notif" />
          </Menu.Item>
        </SubMenuItem>
        {/* menu parameters */}
        <SubMenuItem
          key="parameters:3"
          icon={IconValue.Home}
          title="header.menu.params"
        />
        {/* menu companies */}
        <SubMenuItem
          key="companies:4"
          icon={IconValue.Change}
          title="header.menu.change_company"
        >
          {companies &&
            companies.map((company: ICompany, i: number) => {
              return (
                <Menu.Item
                  key={`companies:4:${i}`}
                  className={`menu-item-company ${
                    company.id === currentCompany.id ? ' bold' : ''
                  }`}
                >
                  {company.name || company.brandName}
                </Menu.Item>
              );
            })}
          <Menu.Item
            key={`companies:4:${companies.length}`}
            className="menu-item-company"
            title="header.menu.add_company"
          >
            <Icon value={IconValue.Plus} />
            <FormattedMessage id="header.menu.add_company" />
          </Menu.Item>
        </SubMenuItem>
        {/* menu share */}
        <MenuItem
          key="setting:5"
          icon={IconValue.Share}
          title="header.menu.share_libeo"
        />
        {/* menu help */}
        <MenuItem
          key="help:6"
          icon={IconValue.Help}
          title="header.menu.help_and_support"
        />
        {/* menu logout */}
        <MenuItem
          key="logout:7"
          className="user-menu-item-signout"
          onClick={this.handleSignout}
          icon={IconValue.Blog}
          title="header.menu.signout"
        />
      </Menu>
    );
  }
}

export default Auth.hoc()(User.hoc()(injectIntl(UserMenu)));
