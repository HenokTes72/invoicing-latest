import { Col, Menu, Row } from 'antd';
import SubMenu from 'antd/lib/menu/SubMenu';
import { IconValue } from 'components/Assets/Icon';
import { Span } from 'components/Typo';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import Caret from './Caret';
import UserIcon from './UserIcon';

interface IProps {
  className?: string;
  children?: React.ReactNode;
  icon: IconValue;
  title: string;
  onClick?: () => void;
}

class SubMenuItem extends React.PureComponent<IProps, {}> {
  render() {
    const { className, icon, title, children, onClick, ...props } = this.props;

    return (
      <SubMenu
        onTitleClick={onClick}
        className={`user-menu-list${className ? ` ${className}` : ''}`}
        {...props}
        key={title}
        title={
          <Span className="user-menu-item">
            <UserIcon value={icon} />
            <FormattedMessage id={title} />
            <Caret />
          </Span>
        }
      >
        {children}
      </SubMenu>
    );
  }
}

export default SubMenuItem;
