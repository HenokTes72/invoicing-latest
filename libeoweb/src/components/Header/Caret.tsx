import { Icon } from 'components/Assets';
import { IconValue } from 'components/Assets/Icon';
import * as React from 'react';
import cssVariables from 'utils/cssVariables';

class Caret extends React.PureComponent<{}, {}> {
  render() {
    return <Icon className="user-menu-caret" value={IconValue.ChevronRight} />;
  }
}

export default Caret;
