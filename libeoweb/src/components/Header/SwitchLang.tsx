import { Button, Menu, Popover } from 'antd';
import { I18NConsumer, InjectedI18NContextProps } from 'context/I18NContext';
import * as React from 'react';
import { ILocale, locales } from 'services/I18N';

interface IProps extends InjectedI18NContextProps {}

class SwitchLang extends React.PureComponent<IProps> {
  changeLocale = (locale: string) => {
    this.props.setLocale(locale);
  };

  renderLang = (locale: ILocale) => {
    return (
      <Menu.Item key={locale.value}>
        <Button
          style={{ margin: '0 20px' }}
          onClick={this.changeLocale.bind(this, locale.value)}
        >
          {locale.label}
        </Button>
      </Menu.Item>
    );
  };

  displayCurrentLang = (locale: ILocale) => {
    const { appLocale } = this.props;
    if (appLocale.locale.includes(locale.value)) {
      return <span key={locale.value}>{locale.label}</span>;
    }
    return null;
  };

  render() {
    return (
      <Popover
        content={
          <Menu mode="inline" style={{ height: '100%', borderRight: 0 }}>
            {locales.map(this.renderLang)}
          </Menu>
        }
        trigger="click"
      >
        <Button style={{ margin: '0 20px' }}>
          <React.Fragment>
            {locales.map(this.displayCurrentLang)}
          </React.Fragment>
        </Button>
      </Popover>
    );
  }
}

export default React.forwardRef((props: any, ref: any) => (
  <I18NConsumer>
    {(i18nContextProps: InjectedI18NContextProps) => (
      <SwitchLang {...props} {...i18nContextProps} ref={ref} />
    )}
  </I18NConsumer>
));
