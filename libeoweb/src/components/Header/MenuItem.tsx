import { Col, Menu, Row } from 'antd';
import { IconValue } from 'components/Assets/Icon';
import { Span } from 'components/Typo';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { Link } from 'react-router-dom';
import UserIcon from './UserIcon';

interface IProps {
  className?: string;
  children?: React.ReactNode;
  icon?: IconValue;
  renderRight?: React.ReactNode;
  title?: string;
  href?: string;
  onClick?: () => void;
}

class MenuItem extends React.PureComponent<IProps, {}> {
  render() {
    const {
      className,
      title,
      href,
      renderRight,
      icon,
      onClick,
      ...props
    } = this.props;

    return (
      <Menu.Item
        onClick={onClick}
        className={`user-menu-list${className ? ` ${className}` : ''}`}
        {...props}
      >
        <Row
          type="flex"
          style={{
            flexDirection: 'row',
          }}
        >
          <Col style={{ flex: 1, lineHeight: '14px' }}>
            <Span className="user-menu-item">
              <Link to={href ? href : '#'}>
                {icon && <UserIcon value={icon} />}
                {title && <FormattedMessage id={title} />}
              </Link>
            </Span>
          </Col>
          {renderRight && <Col>{renderRight}</Col>}
        </Row>
      </Menu.Item>
    );
  }
}

export default MenuItem;
