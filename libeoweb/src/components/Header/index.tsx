import { Col, Layout, Row } from 'antd';
import * as React from 'react';
import { isMobile } from 'react-device-detect';
import AuthenticatedNavBar from './AuthenticatedNavBar';
import GuestNavBar from './GuestNavBar';
import './Header.module.less';

import { Icon } from 'components/Assets';

import { IconValue } from 'components/Assets/Icon';
import { Div } from 'components/Typo';
import { NavLink } from 'react-router-dom';

const { Header } = Layout;

interface IProps {
  authenticated: boolean;
}

const AppHeader: React.FunctionComponent<IProps> = ({ authenticated }) => (
  <Header className="Header">
    <Row
      style={{ height: '100%' }}
      type="flex"
      justify="space-between"
      align="middle"
    >
      <Col xs={24} sm={4}>
        <NavLink to="/">
          <Div
            className="logo"
            css={{
              flex: true,
              row: true,
            }}
            style={{
              justifyContent: 'center',
              width: '100px',
            }}
          >
            <Icon value={IconValue.Logo} />
            <span className="logo-beta">Beta</span>
          </Div>
        </NavLink>
      </Col>
      {!isMobile && (
        <Col sm={20}>
          {!authenticated && <GuestNavBar />}
          {authenticated && <AuthenticatedNavBar />}
        </Col>
      )}
    </Row>
  </Header>
);

export default AppHeader;
