import { Icon } from 'components/Assets';
import { IconValue } from 'components/Assets/Icon';
import * as React from 'react';

interface IProps {
  value: IconValue;
}

class UserMenuIcon extends React.PureComponent<IProps, {}> {
  render() {
    return <Icon className="user-menu-icon" value={this.props.value} />;
  }
}

export default UserMenuIcon;
