import { Avatar, Badge, Button, Card, Input, Popover, Row } from 'antd';
import * as User from 'context/User';
import * as React from 'react';
import './Menu.module.less';
import './SearchBox.css';
// import SwitchLang from './SwitchLang';
import UserMenu from './UserMenu';

import { Icon } from 'components/Assets';
import { IconValue } from 'components/Assets/Icon';
import Menubar from 'components/Sidebar/Menubar';

const { Meta } = Card;

interface IProps extends User.InjectedProps {}

class AuthenticatedNavBar extends React.PureComponent<IProps> {
  handlePopupContainer: () => HTMLElement;
  private searchInput = React.createRef<Input>();

  constructor(props: any) {
    super(props);

    this.handlePopupContainer = this.popupContainer.bind(this);
  }

  popupContainer() {
    const el: HTMLElement | null = document.querySelector('.header-card');
    if (el) {
      return el;
    }
    return document.body;
  }

  render() {
    const { user } = this.props;
    const me = user && user.data && user.data.me;
    const currentCompany =
      user && user.data && user.data.me && user.data.me.currentCompany;

    if (me) {
      return (
        <Row type="flex" justify="space-between" align="middle">
          <div id="search-box" style={{ flex: 1 }}>
            {/* <Input
              style={{
                paddingLeft: '15px'
              }}
              placeholder="Recherche..."
              prefix={
                <Icon
                  value={IconValue.Search}
                  style={{
                    height: '21px',
                    marginRight: '10px',
                    width: '21px'
                  }}
                />
              }
              ref={this.searchInput}
            /> */}
          </div>
          <Button className="btn-notification" style={{ margin: '0 20px' }}>
            <Badge>
              <Icon
                value={IconValue.Bell}
                style={{
                  height: '21px',
                  width: '21px',
                }}
              />
            </Badge>
          </Button>
          {/* <SwitchLang /> */}
          <Popover
            content={<Menubar />}
            trigger="click"
            placement="bottomRight"
            getPopupContainer={this.handlePopupContainer}
          >
            <Card
              className="header-card"
              bordered={false}
              bodyStyle={{ padding: 0 }}
            >
              <Meta
                avatar={
                  <Avatar>
                    {me &&
                      `${me.firstname &&
                        me.firstname.substring(0, 1)}${me.lastname &&
                        me.lastname.substring(0, 1)}`}
                  </Avatar>
                }
                title={
                  <div>
                    {me.firstname} {me.lastname}
                    <div className="header-company-name">
                      {currentCompany && currentCompany.name}
                    </div>
                  </div>
                }
              />
            </Card>
            <Icon
              value={IconValue.ChevronDown}
              style={{
                height: '20px',
                position: 'absolute',
                right: 0,
                top: '50%',
                transform: 'translate(0, -50%)',
                width: '15px',
              }}
            />
          </Popover>
        </Row>
      );
    }
    return null;
  }
}

const ComposedAuthenticatedNavBar = User.hoc()(AuthenticatedNavBar);

export default React.forwardRef((props: any, ref: any) => (
  <ComposedAuthenticatedNavBar {...props} ref={ref} />
));
