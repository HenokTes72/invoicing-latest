import hello from '-!svg-react-loader!assets/images/undraw_hello_aeia.svg';
import { Form } from 'antd';
import { FormComponentProps } from 'antd/lib/form';
import { Alert } from 'components/Alert';
import { IAlertProps } from 'components/Alert/Alert';
import { Email, Password, Submit } from 'components/Form';
import * as AlertCtx from 'context/Alert';
import { IAlert, IAlertType } from 'context/Alert/types';
import * as Auth from 'context/Auth';
import * as React from 'react';
import { compose } from 'react-apollo';
import {
  FormattedHTMLMessage,
  FormattedMessage,
  InjectedIntlProps,
  injectIntl,
} from 'react-intl';
import { errorOrSuccess } from 'utils';
import { Wrapper } from './';
const Hello: any = hello;

interface IProps
  extends FormComponentProps,
    InjectedIntlProps,
    Auth.InjectedProps,
    AlertCtx.InjectedProps {
  hash?: string;
}

interface IState {
  error?: string;
  info?: string;
}

class SignupForm extends React.PureComponent<IProps, IState> {
  state = {
    error: undefined,
    info: undefined,
  };

  handleSubmit: (e: React.FormEvent) => void;

  constructor(props: any) {
    super(props);

    this.handleSubmit = this.submit.bind(this);
  }

  submit = async (e: React.FormEvent) => {
    e.preventDefault();
    this.props.form.validateFields(async (err, values) => {
      const f = this.props.form;

      if (!err) {
        if (this.props.auth) {
          this.setState({ error: undefined });

          const error = await (this.props.auth.signin &&
            this.props.auth.signin(values));
          this.setState({ error });
        }
      }
    });
  };

  componentWillUnmount() {
    const { alert } = this.props;
    alert && alert.reset && alert.reset();
  }

  async componentDidMount() {
    const { hash } = this.props;
    if (hash) {
      await ((this.props.auth && this.props.auth.activate(hash)) || false);
    }
  }

  resend = async () => {
    this.props.form.validateFields(async (err, values) => {
      if (!err) {
        if (this.props.auth) {
          this.setState({ error: undefined });
          await (this.props.auth &&
            this.props.auth.resendActivate(values.email));

          this.setState({ info: 'signin.form.check_your_email' });
        }
      }
    });
  };

  render() {
    const { form, intl, hash } = this.props;
    const { error, info } = this.state;

    let alert: any;
    if (error) {
      alert = (
        <Alert
          background={false}
          alertItem={{
            dismiss: false,
            id: `${error}`,
            message: `${error}`,
            type: IAlertType.Error,
          }}
          message={error && <FormattedHTMLMessage id={error} />}
          type="error"
        />
      );

      if (error === 'api.error.user.disabled') {
        alert = (
          <div
            style={{
              cursor: 'pointer',
            }}
            onClick={this.resend}
          >
            {alert}
          </div>
        );
      }
    }

    const email: any = top.location.search.replace(/\?/, '').split('&');
    let defaultEmail: string | undefined;
    email.map((search: string) => {
      if (search.indexOf('email') === 0) {
        defaultEmail = search.split('=').pop();
      }
    });

    return (
      <Wrapper img={<Hello />} title="signin.title">
        {hash && <FormattedMessage id="signin.form.login_to_validate_token" />}
        {alert}
        {info && (
          <Alert
            background={false}
            alertItem={{
              dismiss: false,
              id: `${info}`,
              message: `${info}`,
              type: IAlertType.Success,
            }}
            message={info && <FormattedHTMLMessage id={info} />}
            type="success"
          />
        )}
        <Form className="form-signin" onSubmit={this.handleSubmit}>
          <Email
            validateTrigger="onSubmit"
            defaultValue={defaultEmail}
            id="email"
            label={<FormattedMessage id="signin.form.email" />}
            rules={[
              {
                message: intl.formatMessage({ id: 'signin.form.email_error' }),
                pattern: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                required: true,
              },
            ]}
            form={form}
          />
          <Password
            validateTrigger="onSubmit"
            id="password"
            label={<FormattedMessage id="signin.form.password" />}
            rules={[
              {
                message: intl.formatMessage({
                  id: 'signin.form.password_error',
                }),
                required: true,
              },
            ]}
            form={form}
          />
          <Submit
            className="signin-submit"
            label={<FormattedMessage id="signin.form.submit" />}
          />
        </Form>
      </Wrapper>
    );
  }
}

export default compose(
  Form.create({}),
  injectIntl,
  Auth.hoc(),
  AlertCtx.hoc(),
)(SignupForm);
