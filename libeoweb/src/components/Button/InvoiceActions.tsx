import { Form, Popover } from 'antd';
import { FormComponentProps } from 'antd/lib/form';
import { Icon } from 'components/Assets';
import { IconValue } from 'components/Assets/Icon';
import { Date } from 'components/Form';
import { Div } from 'components/Typo';
import { IInvoice, InvoiceStatus, NInvoiceStatus } from 'context/Invoice/types';
import moment, { Moment } from 'moment';
import * as React from 'react';
import { compose } from 'react-apollo';
import { FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';
import { Link, RouteComponentProps, withRouter } from 'react-router-dom';
import config from 'utils/config';
import './InvoiceActions.module.less';

/**
 * @props
 */
interface IProps
  extends RouteComponentProps,
    InjectedIntlProps,
    FormComponentProps {
  actions: Array<{
    type: string;
    handle?: (invoice: IInvoice, ...args: any[]) => void;
  }>;
  invoice: IInvoice;
}

/**
 * @state
 */
interface IState {
  open: boolean;
  picker: boolean;
}

class InvoiceActions extends React.PureComponent<IProps, IState> {
  state = {
    open: false,
    picker: false,
  };

  handleClick: (
    handle?: (invoice: IInvoice) => void,
    e?: React.MouseEvent<HTMLDivElement>,
  ) => void;
  handleOpenPicker: (e: React.MouseEvent<HTMLDivElement>) => void;
  handleParentDatePicker: () => HTMLElement;
  handlePayLater: (
    handle?: (invoice: IInvoice) => void,
    date?: Moment,
    dateString?: string,
  ) => void;
  HandleStopPropagation: (e: React.MouseEvent<HTMLDivElement>) => void;
  handleVisibility: (visible: boolean) => void;

  constructor(props: any) {
    super(props);

    this.handleClick = this.click.bind(this);
    this.handleOpenPicker = this.openPicker.bind(this);
    this.handleParentDatePicker = this.parentDatePicker.bind(this);
    this.handlePayLater = this.payLater.bind(this);
    this.HandleStopPropagation = this.stopPropagation.bind(this);
    this.handleVisibility = this.visibility.bind(this);
  }

  parentDatePicker(): HTMLElement {
    const parentDatePicker = document.querySelector('#parentDatePicker');
    return parentDatePicker ? (parentDatePicker as HTMLElement) : document.body;
  }

  payLater(
    handle?: (invoice: IInvoice, ...args: any[]) => void,
    date?: Moment,
    dateString?: string,
  ) {
    this.setState({
      open: false,
      picker: false,
    });
    handle && handle(this.props.invoice, null, null, date);
  }

  stopPropagation(e: React.MouseEvent<HTMLDivElement>) {
    e.stopPropagation();
  }

  click(
    handle?: (invoice: IInvoice) => void,
    e?: React.MouseEvent<HTMLDivElement>,
  ) {
    e && e.stopPropagation();
    handle && handle(this.props.invoice);
  }

  openPicker(e: React.MouseEvent<HTMLDivElement>) {
    e && e.stopPropagation();
    this.setState({
      picker: true,
    });
  }

  visibility(visible: boolean) {
    this.setState({
      open: visible,
      picker: false,
    });
  }

  render() {
    const { invoice, actions } = this.props;
    const { picker } = this.state;

    const content = actions.map(action => {
      switch (action.type) {
        case 'control':
          return (
            invoice &&
            invoice.status === InvoiceStatus.ToPay && (
              <Div
                key="control"
                onClick={this.handleClick.bind(null, action.handle) as any}
                className="popover-row"
              >
                <Icon value={IconValue.Edit} />
                <FormattedMessage id="actions.invoice.control" />
              </Div>
            )
          );
        case 'download':
          return (
            <a
              key="download"
              href={`${(window as any).__LIBEO__.api}/static/${
                invoice.filepath
              }`}
              download={invoice.filename}
              target="_blank"
            >
              <Div onClick={this.HandleStopPropagation} className="popover-row">
                <Icon value={IconValue.Download} />
                <FormattedMessage id="actions.invoice.download" />
              </Div>
            </a>
          );
        case 'delete':
          return (
            invoice &&
            NInvoiceStatus[invoice.status] <=
              NInvoiceStatus[InvoiceStatus.ToPay] && (
              <Div
                key="delete"
                onClick={this.handleClick.bind(null, action.handle) as any}
                className="popover-row"
              >
                <Icon value={IconValue.Trash} />
                <FormattedMessage id="actions.invoice.delete" />
              </Div>
            )
          );
        case 'pay-now':
          return (
            invoice &&
            invoice.status !== InvoiceStatus.Planned &&
            invoice.status !== InvoiceStatus.Paid && (
              <Div
                key="pay-now"
                onClick={this.handlePayLater.bind(
                  null,
                  action.handle,
                  moment(),
                  moment().toString(),
                )}
                className="popover-row"
              >
                <Icon value={IconValue.Wallet} />
                <FormattedMessage id="actions.invoice.pay_now" />
              </Div>
            )
          );
        case 'pay-later':
          return (
            invoice &&
            invoice.status !== InvoiceStatus.Planned &&
            invoice.status !== InvoiceStatus.Paid && (
              <Div
                key="pay-later"
                onClick={this.handleOpenPicker}
                className="popover-row"
              >
                {picker && (
                  <div id="parentDatePicker">
                    <Date
                      onChangeDate={this.handlePayLater.bind(
                        null,
                        action.handle,
                      )}
                      getCalendarContainer={this.handleParentDatePicker}
                      open={true}
                      id="invoiceDate"
                      label={
                        <FormattedMessage id="purchase.control.billing_date" />
                      }
                      form={this.props.form}
                    />
                  </div>
                )}
                <Icon value={IconValue.Wallet} />
                <FormattedMessage id="actions.invoice.pay_later" />
              </Div>
            )
          );
      }
    });

    return (
      <Popover
        onVisibleChange={this.handleVisibility}
        overlayClassName="invoice-actions"
        content={content}
        trigger="click"
        placement="bottomLeft"
      >
        <div
          onClick={this.HandleStopPropagation}
          className={`popover-invoice-icon${this.state.open ? ' open' : ''}`}
        >
          <Icon value={IconValue.Dots3} />
        </div>
      </Popover>
    );
  }
}

export default compose(
  withRouter,
  Form.create({}),
  injectIntl,
)(InvoiceActions);
