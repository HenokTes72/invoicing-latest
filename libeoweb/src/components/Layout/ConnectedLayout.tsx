import { Col, Layout, Row } from 'antd';
import { Alert } from 'components/Alert';
import Header from 'components/Header';
import * as AlertCtx from 'context/Alert';
import * as User from 'context/User';
import * as React from 'react';
import { isMobile } from 'react-device-detect';
import { FormattedMessage } from 'react-intl';

const ProfileSidebar = React.lazy(() =>
  import('components/Sidebar/ProfileSidebar'),
);
const MainSidebar = React.lazy(() => import('components/Sidebar/MainSidebar'));

const { Content } = Layout;

export interface IConnectedLayoutOptions {
  sidebar?: boolean;
  subSidebar?: boolean;
}

interface IProps extends AlertCtx.InjectedProps, IConnectedLayoutOptions {
  children: React.ReactNode;
}

interface IProps extends User.InjectedProps {}

class ConnectedLayout extends React.PureComponent<IProps> {
  static defaultProps = {
    sidebar: true,
    subSidebar: false,
  };

  componentDidMount() {
    const zeSnippet = document.querySelector('#ze-snippet');
    if (!zeSnippet) {
      const script = document.createElement('script');
      script.id = 'ze-snippet';
      script.src =
        'https://static.zdassets.com/ekr/snippet.js?key=9ebb1afa-5a9e-496f-a9a9-ee9f7fb7d0df';

      (document as any).head.appendChild(script);
    }
  }

  render() {
    const { children, alert, subSidebar, sidebar } = this.props;
    const errors = alert && alert.errors ? alert.errors : [];
    const successes = alert && alert.successes ? alert.successes : [];

    return (
      <Layout style={{ minHeight: '100vh' }}>
        <Header authenticated={true} />
        <Layout>
          <Row
            style={{
              background: '#fff',
              display: 'flex',
              height: '100%',
              width: '100%',
            }}
          >
            {sidebar && (
              <React.Suspense fallback={<div>Loading...</div>}>
                <MainSidebar />
              </React.Suspense>
            )}
            {subSidebar && (
              <React.Suspense fallback={<div>Loading...</div>}>
                <ProfileSidebar />
              </React.Suspense>
            )}
            <Col
              style={{
                minHeight: 'calc(100vh - 64px)',
                width: '100%',
              }}
            >
              {errors &&
                errors.map((error, i) => (
                  <Alert
                    className="floating"
                    key={`${i}`}
                    alertItem={error}
                    closable
                    background
                    message={
                      error.message && <FormattedMessage id={error.message} />
                    }
                    type="error"
                  />
                ))}
              {successes &&
                successes.map((success, i) => (
                  <Alert
                    className="floating"
                    key={`${i}`}
                    alertItem={success}
                    closable
                    background
                    message={
                      success.message && (
                        <FormattedMessage id={success.message} />
                      )
                    }
                    type="success"
                  />
                ))}
              <div
                style={{
                  display: 'flex',
                  flexDirection: 'column',
                  height: '100%',
                  minHeight: '100%',
                }}
              >
                {children}
              </div>
            </Col>
          </Row>
        </Layout>
      </Layout>
    );
  }
}

export default React.memo(User.hoc()(AlertCtx.hoc()(ConnectedLayout)));
