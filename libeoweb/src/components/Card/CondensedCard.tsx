import { Col } from 'antd';
import { CardProps } from 'antd/lib/card';
import { Icon } from 'components/Assets';
import { IconValue } from 'components/Assets/Icon';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import Card from './Card';

/**
 * @props
 */
interface IProps {
  center?: boolean;
  className?: string;
  title?: React.ReactNode;
  description?: string;
  color?: string;
  icon?: IconValue;
}

/**
 * @state
 *
 * error
 */
interface IState {}

/**
 * @class Submit
 *
 */
class CondensedCard extends React.PureComponent<IProps, IState> {
  render() {
    const { className, center, title, description, icon, color } = this.props;

    return (
      <Card
        center
        className={`condensed-card${className ? ` ${className}` : ''}`}
        shadow
      >
        <div className="condensed-card-left">
          <div className="condensed-card-title">{title}</div>
          {description && (
            <div className="condensed-card-description">
              <FormattedMessage id={description} />
            </div>
          )}
        </div>
        <div className={`condensed-card-right${color ? ` ${color}` : ''}`}>
          <Icon value={icon} />
        </div>
      </Card>
    );
  }
}

export default CondensedCard;
