import { Avatar, Card } from 'antd';
import { CardProps } from 'antd/lib/card';
import { Icon } from 'components/Assets';
import { IconValue } from 'components/Assets/Icon';
import * as React from 'react';

/**
 * @props
 */
interface IProps extends CardProps {
  center?: boolean;
  avatar?: React.ReactNode | string;
  editable?: boolean;
  onEdit?: () => void;
  removable?: boolean;
  onRemove?: () => void;
  shadow?: boolean;
  title?: React.ReactNode;
  titleAlign?: 'left' | 'right';
}

/**
 * @state
 *
 * error
 */
interface IState {}

/**
 * @class Submit
 *
 */
class Default extends React.PureComponent<IProps, IState> {
  onRemove = (e: React.MouseEvent<HTMLDivElement>) => {
    e.stopPropagation();
    e.preventDefault();
    const { onRemove } = this.props;
    onRemove && onRemove();
  };

  render() {
    const {
      avatar,
      center,
      children,
      className,
      editable,
      onEdit,
      removable,
      shadow,
      title,
      titleAlign,
      ...rest
    } = this.props;

    return (
      <Card
        className={`card-item${center ? ' center ' : ''}${
          shadow ? ' shadow ' : ''
        } ${className ? ` ${className}` : ''}`}
        {...rest}
      >
        {editable && (
          <div onClick={onEdit} className="card-edit">
            <Icon value={IconValue.Pencil} />
          </div>
        )}
        {removable && (
          <div onClick={this.onRemove} className="card-remove">
            <Icon value={IconValue.Trash} />
          </div>
        )}
        {avatar && <Avatar className="card-avatar">{avatar}</Avatar>}
        {title && (
          <div className={`card-title${titleAlign ? ` ${titleAlign}` : ''}`}>
            {title}
          </div>
        )}
        <div className="card-children">{children}</div>
      </Card>
    );
  }
}

export default Default;
