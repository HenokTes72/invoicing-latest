import PrivateRoute from './PrivateRoute';
import KycRoute from './KycRoute';
import PublicRoute from './PublicRoute';
import ExternalRoute from './ExternalRoute';

export { KycRoute, PrivateRoute, PublicRoute, ExternalRoute };
