import { Form } from 'antd';
import { BtnType, Button } from 'components/Button';
import * as React from 'react';

const FormItem = Form.Item;

/**
 * @props
 */
interface IProps {
  className?: string;
  label: string | React.ReactNode;
}

/**
 * @state
 *
 * error
 */
interface IState {}

/**
 * @class Submit
 *
 * Used by antd form event on submit
 */
class Submit extends React.PureComponent<IProps, IState> {
  render() {
    const { label, className } = this.props;

    return (
      <FormItem
        className={`form-item form-item-submit${
          className ? ` ${className}` : ''
        }`}
      >
        <Button type={BtnType.Primary} submit>
          {label}
        </Button>
      </FormItem>
    );
  }
}

export default Submit;
