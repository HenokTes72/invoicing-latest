import { Col, Popover, Row } from 'antd';
import { Icon } from 'components/Assets';
import { IconValue } from 'components/Assets/Icon';
import { BtnType, Button } from 'components/Button';
import { Div, H1 } from 'components/Typo';
import * as React from 'react';
import { FormattedHTMLMessage, FormattedMessage } from 'react-intl';
import './Heading.module.less';

interface IProps {
  onClick?: () => void;
  right?: React.ReactNode;
  button?: string;
  buttonCta?: string;
  title?: string;
  icon?: IconValue;
  center?: boolean;
  small?: boolean;
  titleVariables?: any;
  description?: string;
  descriptionVariables?: any;
}

class Heading extends React.PureComponent<IProps, {}> {
  render() {
    const {
      title,
      icon,
      center,
      small,
      titleVariables,
      description,
      button,
      right,
      buttonCta,
      onClick,
      descriptionVariables,
    } = this.props;

    return (
      <div
        style={{
          display: 'block',
          width: '100%',
        }}
      >
        <Row
          style={{
            display: 'flex',
            flex: 1,
            flexDirection: 'row',
          }}
        >
          <Col
            className={`heading${center ? ' center' : ''}${
              small ? ' small' : ''
            }`}
          >
            {title && (
              <H1>
                <FormattedHTMLMessage id={title} values={titleVariables} />
                {icon && <Icon className="heading-icon" value={icon} />}
              </H1>
            )}
            {description && (
              <Div className="heading-description">
                <FormattedMessage
                  id={description}
                  values={descriptionVariables}
                />
              </Div>
            )}
          </Col>
          {buttonCta ? (
            <Popover
              placement="leftTop"
              content={<FormattedMessage id={buttonCta} />}
            >
              <div>
                {button && (
                  <Button onClick={onClick}>
                    <FormattedMessage id={button} />
                  </Button>
                )}
              </div>
            </Popover>
          ) : (
            button && (
              <Button onClick={onClick}>
                <FormattedMessage id={button} />
              </Button>
            )
          )}
          {right}
        </Row>
      </div>
    );
  }
}

export default Heading;
