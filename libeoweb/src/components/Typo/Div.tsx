import * as React from 'react';
import IDefault from './Default';

import './Div.module.less';

class Div extends IDefault {
  static defaultProps = {
    ...IDefault.defaultProps,
    className: 'Div',
    tag: 'div',
  };
}

export default Div;
