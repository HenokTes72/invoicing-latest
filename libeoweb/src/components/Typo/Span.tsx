import * as React from 'react';
import IDefault from './Default';

import './Span.module.less';

class Span extends IDefault {
  static defaultProps = {
    ...IDefault.defaultProps,
    className: 'Span',
    tag: 'span',
  };
}

export default Span;
