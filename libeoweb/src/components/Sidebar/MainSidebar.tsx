import { ClickParam } from 'antd/lib/menu';
import { IconValue } from 'components/Assets/Icon';
import * as React from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import Sidebar from './Sidebar';

interface IProps extends RouteComponentProps {}

interface IState {}

class MainSidebar extends React.PureComponent<IProps, IState> {
  render() {
    return (
      <div
        className="sidebar-wrapper main-sidebar-wrapper"
        style={{
          display: 'flex',
          flexDirection: 'row',
        }}
      >
        <Sidebar
          sticky
          name="main"
          className="main-sidebar"
          stripe
          items={[
            {
              icon: IconValue.Pulse,
              key: '',
              title: 'sidebar.link.dashboard',
            },
            {
              icon: IconValue.Sell,
              items: [
                {
                  key: 'draft',
                  title: 'sidebar.link.purchases_imports',
                },
                {
                  key: 'bills',
                  title: 'sidebar.link.purchases_bills',
                },
                {
                  disabled: true,
                  key: 'quotation',
                  title: 'sidebar.link.purchases_quotations',
                },
              ],
              key: 'purchase',
              title: 'sidebar.link.purchases',
            },
            {
              disabled: true,
              icon: IconValue.Buy,
              items: [
                {
                  disabled: true,
                  key: 'draft',
                  title: 'sidebar.link.sales_drafts',
                },
                {
                  disabled: true,
                  key: 'bills',
                  title: 'sidebar.link.sales_quotations',
                },
                {
                  disabled: true,
                  key: 'quotation',
                  title: 'sidebar.link.sales_bills',
                },
              ],
              key: 'sell',
              title: 'sidebar.link.sales',
            },
            {
              disabled: true,
              icon: IconValue.Stat,
              key: 'statistics',
              title: 'sidebar.link.stats',
            },
            {
              icon: IconValue.Network,
              key: 'network',
              title: 'sidebar.link.network',
            },
          ]}
        />
      </div>
    );
  }
}

export default withRouter(MainSidebar);
