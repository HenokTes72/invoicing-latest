import { ClickParam } from 'antd/lib/menu';
import { IconValue } from 'components/Assets/Icon';
import * as React from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import Sidebar from './Sidebar';

interface IProps extends RouteComponentProps {}

interface IState {}

class ProfileSidebar extends React.PureComponent<IProps, IState> {
  render() {
    return (
      <div
        className="sidebar-wrapper sub-sidebar-wrapper"
        style={{
          display: 'flex',
          flexDirection: 'row',
        }}
      >
        <Sidebar
          sticky
          name="profile"
          className="sub-sidebar"
          stripe
          items={[
            {
              icon: IconValue.Profile,
              items: [
                {
                  key: 'informations',
                  title: 'account.submenu.profile_informations',
                },
                {
                  disabled: true,
                  key: 'contacts',
                  title: 'account.submenu.profile_contacts',
                },
                {
                  disabled: true,
                  key: 'notifications',
                  title: 'account.submenu.profile_notifications',
                },
              ],
              key: 'profile',
              title: 'account.submenu.profile',
            },
            {
              icon: IconValue.Briefcase,
              items: [
                {
                  key: 'informations',
                  title: 'account.submenu.company_informations',
                },
                {
                  disabled: true,
                  key: 'proof',
                  title: 'account.submenu.company_proof',
                },
                {
                  disabled: true,
                  key: 'bank',
                  title: 'account.submenu.company_bank',
                },
                {
                  disabled: true,
                  key: 'collaborators',
                  title: 'account.submenu.company_collaborators',
                },
                {
                  key: 'accounting',
                  title: 'account.submenu.company_accounting',
                },
              ],
              key: 'company',
              title: 'account.submenu.company',
            },
            {
              icon: IconValue.Rocket,
              key: 'balance',
              title: 'account.submenu.balance',
            },
          ]}
        />
      </div>
    );
  }
}

export default withRouter(ProfileSidebar);
