import { IconValue } from 'components/Assets/Icon';
import * as Auth from 'context/Auth';
import * as BalanceCtx from 'context/Balance';
import { ICompany } from 'context/Company/types';
import * as User from 'context/User';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { RouteComponentProps, withRouter } from 'react-router';
import currencies from 'utils/currencies';
import './Menubar.module.less';
import Sidebar from './Sidebar';

interface IProps
  extends RouteComponentProps,
    Auth.InjectedProps,
    User.InjectedProps {}

interface IState {
  devises: any;
}

class Menubar extends React.PureComponent<IProps, IState> {
  state = {
    devises: {},
  };

  handleSignout: () => void;
  constructor(props: any) {
    super(props);

    this.handleSignout = this.signout.bind(this);
  }

  async componentDidMount() {
    const devises = await currencies.all();
    this.setState({ devises });
  }

  async signout() {
    await (this.props.auth &&
      this.props.auth.signout &&
      this.props.auth.signout());
  }

  render() {
    const { user } = this.props;
    const devises: any = this.state.devises;
    const companies =
      user &&
      user.data &&
      user.data.me &&
      user.data.me.companies &&
      user.data.me.companies.rows;

    return (
      <div
        className="sidebar-wrapper sub-sidebar-wrapper"
        style={{
          display: 'flex',
          flexDirection: 'row',
        }}
      >
        <Sidebar
          name="menu"
          className="menu-bar"
          collapsable={false}
          contrast
          items={[
            {
              dividerAfter: true,
              key: 'balance',
              right: (
                <BalanceCtx.Provider balance>
                  <BalanceCtx.Consumer>
                    {({ balance }) =>
                      balance &&
                      balance.data &&
                      balance.data.balance &&
                      `${balance.data.balance.currentBalance}${devises[
                        balance.data.balance.currency
                      ] && devises[balance.data.balance.currency].symbol}`
                    }
                  </BalanceCtx.Consumer>
                </BalanceCtx.Provider>
              ),
              title: (
                <span className="header-balance">
                  <FormattedMessage id="header.menu.balance" />
                </span>
              ),
            },
            {
              icon: IconValue.Profile,
              items: [
                {
                  key: 'informations',
                  title: 'header.menu.profile_informations',
                },
                {
                  disabled: true,
                  key: 'contacts',
                  title: 'header.menu.profile_contacts',
                },
                {
                  disabled: true,
                  key: 'notifications',
                  title: 'header.menu.profile_notifications',
                },
              ],
              key: 'profile',
              title: 'header.menu.profile',
            },
            {
              icon: IconValue.Briefcase,
              items: [
                {
                  key: 'informations',
                  title: 'header.menu.company_informations',
                },
                {
                  disabled: true,
                  key: 'proof',
                  title: 'header.menu.company_proof',
                },
                {
                  disabled: true,
                  key: 'bank',
                  title: 'header.menu.company_bank',
                },
                {
                  disabled: true,
                  key: 'collaborators',
                  title: 'header.menu.company_collaborators',
                },
                {
                  key: 'accounting',
                  title: 'header.menu.company_accounting',
                },
              ],
              key: 'company',
              title: 'header.menu.company',
            },
            {
              disabled: true,
              icon: IconValue.Change,
              items:
                companies &&
                companies.map((company: ICompany) => ({
                  key: company.id,
                  title: company.name || company.brandName,
                })),
              key: 'switch_company',
              title: 'header.menu.switch_company',
            },
            {
              disabled: true,
              dividerBefore: true,
              icon: IconValue.Share,
              key: 'share',
              title: 'header.menu.share',
            },
            {
              disabled: true,
              icon: IconValue.Help,
              key: 'help',
              title: 'header.menu.help',
            },
            {
              icon: IconValue.Blog,
              key: 'logout',
              onClick: this.handleSignout,
              reverse: true,
              title: 'header.menu.logout',
            },
          ]}
        />
      </div>
    );
  }
}

export default Auth.hoc()(User.hoc()(withRouter(Menubar)));
