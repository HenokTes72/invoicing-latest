import moment from 'moment';
import * as React from 'react';
import {
  Area,
  AreaChart,
  CartesianGrid,
  ComposedChart,
  Line,
  LineChart,
  ReferenceLine,
  ResponsiveContainer,
  XAxis,
  YAxis,
} from 'recharts';

interface IProps {
  lines: any[];
  amount: number;
}
interface IState {
  lines: any[];
  data: any[];
}

class Reline extends React.PureComponent<IProps, IState> {
  static processLines(lines: any): any {
    const data: any[] = [];

    const startDate: moment.Moment = moment(lines[0].x);

    lines.map((line: any, i: number) => {
      const dueDate: moment.Moment = moment(line.x);
      const formattedLine = {
        x: line.x,
        // x: moment(line.x).format('MMM'),
        // x: moment(line.x).format('DD-MM-YYYY'),
        // x: dueDate.diff(startDate, 'days'),
        y: line.y,
      };

      data.push(formattedLine);
    });

    return {
      data,
    };
  }

  static getDerivedStateFromProps(props: IProps, state: IState) {
    if (JSON.stringify(props.lines) !== JSON.stringify(state.lines)) {
      const { data } = Reline.processLines(props.lines);

      return {
        amount: props.amount,
        data,
        lines: props.lines,
      };
    }
    return state;
  }
  state = {
    data: [],
    lines: [],
  };

  gradientArea(id?: string, fade?: boolean) {
    const { data } = this.state;
    const dataMax = Math.max(...data.map((i: any) => i.y));
    const dataMin = Math.min(...data.map((i: any) => i.y));
    const average =
      100 - (Math.abs(dataMin) * 100) / (Math.abs(dataMax) + Math.abs(dataMin));

    if (dataMin > 0) {
      return [
        <stop
          key={id ? `${id}-1` : '1'}
          offset="0%"
          stopColor="#5DBE42"
          stopOpacity="1"
        />,
        <stop
          key={id ? `${id}-2` : '2'}
          offset="100%"
          stopColor="#5DBE42"
          stopOpacity={fade ? '0' : '1'}
        />,
      ];
    }
    if (dataMax <= 0) {
      return [
        <stop
          key={id ? `${id}-2` : '2'}
          offset="0%"
          stopColor="#FF5F5F"
          stopOpacity={fade ? '0' : '1'}
        />,
        <stop
          key={id ? `${id}-1` : '1'}
          offset="100%"
          stopColor="#FF5F5F"
          stopOpacity="1"
        />,
      ];
    }
    return [
      <stop
        key={id ? `${id}-1` : '1'}
        offset="0%"
        stopColor="#5DBE42"
        stopOpacity="1"
      />,
      <stop
        key={id ? `${id}-2` : '2'}
        offset={`${average}%`}
        stopColor="#5DBE42"
        stopOpacity={fade ? '0' : '1'}
      />,
      <stop
        key={id ? `${id}-3` : '3'}
        offset={`${average}%`}
        stopColor="#FF5F5F"
        stopOpacity={fade ? '0' : '1'}
      />,
      <stop
        key={id ? `${id}-4` : '4'}
        offset="100%"
        stopColor="#FF5F5F"
        stopOpacity="1"
      />,
    ];
  }

  render() {
    const { data } = this.state;
    const tickTimes: string[] = [];
    const tickFormatter = (tick: any) => {
      const month = moment(tick).format('DD-MM');
      if (tickTimes.indexOf(month) === -1) {
        tickTimes.push(month);
        return month;
      }
      return month;
    };

    return (
      <div className="chart-wrapper">
        <ResponsiveContainer width="100%" height="100%">
          <AreaChart data={data}>
            <XAxis
              tickLine={false}
              tickFormatter={tickFormatter}
              // scale="time"
              // type="number"
              // tick={false}
              stroke="#ECECEC"
              dataKey="x"
            />
            <YAxis stroke="#ECECEC" dataKey="y" />
            {/* <Tooltip /> */}
            <defs>
              <linearGradient id="areaColor" x1="0" y1="0" x2="0" y2="1">
                {this.gradientArea('area-chart', true)}
              </linearGradient>
            </defs>
            <ReferenceLine
              y="0"
              stroke="#ECECEC"
              label=""
              strokeDasharray="3 3"
            />
            <Area
              type="monotone"
              dataKey="y"
              stroke={0}
              fill="url(#areaColor)"
            />
          </AreaChart>
        </ResponsiveContainer>
        <div className="absolute-line-chart">
          <ResponsiveContainer width="100%" height="100%">
            <LineChart data={data}>
              <XAxis stroke="transparent" dataKey="x" />
              <YAxis stroke="transparent" dataKey="y" />
              <defs>
                <linearGradient id="strokeColor" x1="0" y1="0" x2="0" y2="1">
                  {this.gradientArea('line-chart', false)}
                </linearGradient>
              </defs>
              <Line
                dot={false}
                type="monotone"
                dataKey="y"
                stroke="url(#strokeColor)"
              />
            </LineChart>
          </ResponsiveContainer>
        </div>
      </div>
    );
  }
}

export default Reline;
