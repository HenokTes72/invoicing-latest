import { Col, Row } from 'antd';
import moment from 'moment';
import * as React from 'react';
import CountUp from 'react-countup';
import { FormattedMessage } from 'react-intl';
import { Cell, Pie, PieChart, ResponsiveContainer } from 'recharts';

interface IProps {
  rows: any[];
}
interface IState {
  rows: any[];
  data: any[];
  empty: any[];
  total: number;
}

class Repie extends React.PureComponent<IProps, IState> {
  static processLines(lines: any): any {
    let total: number = 0;

    const today: moment.Moment = moment(new Date());
    const data = [
      {
        color: '#FF5F5F',
        id: 'dashboard.chart.pie_legend_5',
        name: '1-5',
        solde: 0,
        value: 0,
      },
      {
        color: '#FF7960',
        id: 'dashboard.chart.pie_legend_20',
        name: '5-20',
        solde: 0,
        value: 0,
      },
      {
        color: '#0053FA',
        id: 'dashboard.chart.pie_legend_45',
        name: '20-45',
        solde: 0,
        value: 0,
      },
      {
        color: '#51DAF3',
        id: 'dashboard.chart.pie_legend_more',
        name: '+45',
        solde: 0,
        value: 0,
      },
    ];

    lines.map((line: any, i: number) => {
      const dueDate: moment.Moment = moment(line.dueDate);
      const days: number = dueDate.diff(today, 'days');
      if (days < 5) {
        data[0].value += 1;
        data[0].solde += Math.round(line.total * 100) / 100;
      } else if (days < 20) {
        data[1].value += 1;
        data[1].solde += Math.round(line.total * 100) / 100;
      } else if (days < 45) {
        data[2].value += 1;
        data[2].solde += Math.round(line.total * 100) / 100;
      } else {
        data[3].value += 1;
        data[3].solde += Math.round(line.total * 100) / 100;
      }
      total += Math.round(line.total * 100) / 100;
    });

    return {
      data,
      total,
    };
  }

  static getDerivedStateFromProps(props: IProps, state: IState) {
    if (JSON.stringify(props.rows) !== JSON.stringify(state.rows)) {
      const { data, total } = Repie.processLines(props.rows);

      return {
        data,
        lines: props.rows,
        total,
      };
    }
    return state;
  }

  state = {
    data: [],
    empty: [
      {
        color: '#AFAFAF',
        id: 'dashboard.chart.empty',
        name: '',
        solde: 0,
        value: 1,
      },
    ],
    rows: [],
    total: 0,
  };

  render() {
    const { data, total, empty } = this.state;

    const pieData: any[] = data.length > 0 ? data : empty;

    return (
      <div className="chart-wrapper">
        <Row
          style={{
            alignItems: 'center',
            height: '100%',
            justifyContent: 'center',
          }}
          type="flex"
        >
          <Col span={12}>
            {pieData.map((entry: any, i) => {
              return (
                <div key={`${i}`} className="pie-legend">
                  <span
                    key={`${i}`}
                    style={{
                      background: entry.color,
                    }}
                    className="pie-legend-color"
                  />
                  <FormattedMessage id={entry.id} />
                  <FormattedMessage
                    id="dashboard.chart.pie_legend_solde"
                    values={{ solde: entry.solde }}
                  />
                </div>
              );
            })}
          </Col>
          <Col
            style={{
              height: '100%',
              position: 'relative',
              width: 200,
            }}
          >
            <span className="pie-total">
              <CountUp end={total} />
              <FormattedMessage
                id="dashboard.chart.total"
                values={{ solde: total }}
              />
            </span>
            <ResponsiveContainer width="100%" height="100%">
              <PieChart>
                <Pie
                  data={pieData}
                  innerRadius={75}
                  outerRadius={100}
                  paddingAngle={0.1}
                  dataKey="value"
                >
                  {pieData.map((entry: any, index) => {
                    return <Cell key={`cell-${index}`} fill={entry.color} />;
                  })}
                </Pie>
              </PieChart>
            </ResponsiveContainer>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Repie;
