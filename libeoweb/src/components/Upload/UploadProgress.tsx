import UploadInvoice from 'assets/images/upload-invoice.svg';
import * as Upload from 'context/Upload';
import * as React from 'react';
import { FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';
import './UploadWrapper.module.less';

import { Icon } from 'components/Assets';
import { IconValue } from 'components/Assets/Icon';
import { isMobile } from 'react-device-detect';
import { Link } from 'react-router-dom';
import history from 'store/history';

interface IProps extends InjectedIntlProps, Upload.InjectedProps {}

interface IState {
  success: boolean;
  visible: boolean;
  filesUploading: File[];
  blacklist: string[];
}

class UploadProgress extends React.PureComponent<IProps, IState> {
  static getDerivedStateFromProps(props: IProps, state: IState) {
    const upload: any = props.upload;
    if (upload.filesUploading !== state.filesUploading) {
      let success = true;
      upload.filesUploading &&
        upload.filesUploading.map((file: any) => {
          if (file.loading) {
            success = false;
          }
        });
      if (upload.filesUploading.length > 0) {
        return {
          filesUploading: [
            ...(upload.filesUploading ? upload.filesUploading : []),
            ...(upload.queu ? upload.queu : []),
          ],
          success,
          visible: true,
        };
      } else if (state.visible) {
        upload.clearFilesUploading();
        return {
          success: false,
          visible: false,
        };
      }
    }
    return null;
  }

  state = {
    blacklist: ['/onboarding', '/invoice/draft', '/purchase/draft'],
    filesUploading: [],
    success: false,
    visible: false,
  };

  componentDidUpdate(props: IProps, state: IState) {
    if (this.state.success && !state.success) {
      setTimeout(() => {
        let redirect = true;
        this.state.blacklist &&
          this.state.blacklist.map(item => {
            if (top.location.pathname.indexOf(item) > -1) {
              redirect = false;
            }
          });

        if (redirect) {
          history.push('/purchase/draft');
        }
        this.close();
      }, 3000);
    }
  }

  close = async () => {
    const upload: any = this.props.upload;
    this.setState({
      success: false,
      visible: false,
    });
    if (upload.clearFilesUploading) {
      upload.clearFilesUploading();
    }
  };

  render() {
    const { intl } = this.props;
    const { visible, filesUploading, success } = this.state;

    return (
      <div className="upload-drawer-outer">
        <div
          className={`upload-drawer-inner ${visible ? 'visible' : 'hidden'}`}
        >
          <div className="upload-drawer-header">
            <span className="upload-invoice">
              <UploadInvoice />
            </span>
            <div>
              <div className="upload-drawer-header-description">
                <FormattedMessage id="common.upload.drawer_description" />
              </div>
              <div className="upload-drawer-header-title">
                <FormattedMessage id="common.upload.drawer_title" />
              </div>
            </div>
            <div className="upload-drawer-header-close">
              <Icon value={IconValue.Cross} onClick={this.close} />
            </div>
          </div>
          <div className="upload-drawer-rows">
            {success && isMobile && (
              <div className="upload-drawer-row-finished">
                <FormattedMessage id="common.upload.success_mobile" />
              </div>
            )}
            {filesUploading &&
              filesUploading.map((file: any, i) => {
                let size: number = Math.round(file.size / 10) / 100;
                let mo: boolean = false;
                if (size > 1000) {
                  size = Math.round(file.size / 10000) / 100;
                  mo = true;
                }

                return (
                  <Link
                    key={`${i}`}
                    to={file.id ? `/invoice/draft/${file.id}` : '#'}
                  >
                    <div className="upload-drawer-row">
                      <div className="upload-drawer-row-loader">
                        <div
                          className={`circle-loader${
                            file.loading === false && !file.error
                              ? ' load-complete'
                              : ''
                          }${file.error ? ' load-error' : ''}`}
                        >
                          <div className="checkmark" />
                        </div>
                      </div>
                      <div className="upload-drawer-row-title">
                        {file.name}
                        {file.error && (
                          <div className="upload-drawer-row-error">
                            {<FormattedMessage id={file.error} />}
                          </div>
                        )}
                      </div>
                      <div className="upload-drawer-row-size">
                        {intl.formatMessage(
                          {
                            id: mo
                              ? 'common.upload.size_mo'
                              : 'common.upload.size_ko',
                          },
                          { size },
                        )}
                      </div>
                    </div>
                  </Link>
                );
              })}
          </div>
        </div>
      </div>
    );
  }
}

export default injectIntl(Upload.hoc()(UploadProgress));
