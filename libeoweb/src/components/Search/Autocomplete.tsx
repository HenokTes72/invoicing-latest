import { Tag } from 'antd';
import { BtnType, Button } from 'components/Button';
import { Search } from 'components/Form';
import { InputRules, IType } from 'components/Form/Default';
import Infinity from 'components/Infinity';
import { IInputAddress } from 'context/Addresses/types';
import { ICompany, ICompanyStatus, IInputCompany } from 'context/Company/types';
import * as React from 'react';
import { FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';
import NotFound from './NotFound';

export interface IAutocompleteProps extends InjectedIntlProps {
  placeholder?: string;
  form?: any;
  footer?: React.ReactNode;
  onSelect?: (item: IInputCompany) => void;
  onSearch?: (value: string) => void;
  onValueChange?: (value: string) => void;
  loadMore?: (more: any) => Promise<void>;
  onFocus?: () => void;
  onBlur?: () => void;
  onLeave?: () => void;
  rows?: ICompany[];
  rules?: InputRules[];
  hasMore?: boolean;
  loading?: boolean;
  inline?: boolean;
  type: string;
}

export interface IAutocompleteState {
  value: string;
  open?: boolean;
}

class SearchAutocomplete extends React.PureComponent<
  IAutocompleteProps,
  IAutocompleteState
> {
  state = {
    open: false,
    value: '',
  };

  handleSetWrapperRef: (node: HTMLDivElement) => void;
  handleSearch: (value: string) => void;
  handleChange: (node: React.ChangeEvent<Element>) => void;
  handleFocus?: (event: React.FocusEvent<HTMLInputElement>) => any;
  handleBlur?: (event: React.FocusEvent<HTMLInputElement>) => any;
  handleClickOutside?: (event: Event) => void;
  handleClickFooter?: () => void;

  wrapperRef?: HTMLDivElement;

  constructor(props: any) {
    super(props);

    this.handleClickOutside = this.clickOutside.bind(this);
    this.handleClickFooter = this.clickFooter.bind(this);
    this.handleSetWrapperRef = this.setWrapperRef.bind(this);
    this.handleSearch = this.search.bind(this);
    this.handleChange = this.change.bind(this);
    this.handleFocus = this.focus.bind(this);
    this.handleBlur = this.blur.bind(this);
  }

  setWrapperRef(node: HTMLDivElement) {
    if (node) {
      this.wrapperRef = node;
    }
  }

  clickOutside(event: Event) {
    const target: HTMLElement = event.target as HTMLElement;
    if (target && this.wrapperRef && !this.wrapperRef.contains(target)) {
      this.props.onLeave && this.props.onLeave();
      this.setState({ open: false });
    }

    return true;
  }

  clickFooter() {
    this.setState({
      open: false,
      value: '',
    });
  }

  blur(event: React.FocusEvent<HTMLInputElement>) {
    this.props.onBlur && this.props.onBlur();
  }

  focus(event: React.FocusEvent<HTMLInputElement>) {
    this.props.onFocus && this.props.onFocus();
    this.setState({ open: true });
  }

  search(value: string): void {
    const { onSearch } = this.props;
    this.setState({ value });
    onSearch && onSearch(value);
  }

  change(node: React.ChangeEvent<Element>): void {
    const { onValueChange } = this.props;
    const target: any = node.currentTarget;
    onValueChange && onValueChange(target.value);
  }

  select(company: ICompany) {
    const { onSelect } = this.props;

    const addresses: IInputAddress[] =
      company.addresses && company.addresses.rows
        ? company.addresses.rows.map(address => {
            return {
              address1: address.address1,
              address2: address.address2,
              city: address.city,
              country: address.country,
              siret: address.siret,
              zipcode: address.zipcode,
            };
          })
        : [];

    const inputCompany: IInputCompany = {
      addresses,
      brandName: company.brandName,
      capital: company.capital,
      category: company.category,
      incorporationAt: company.incorporationAt,
      legalForm: company.legalForm,
      naf: company.naf,
      nafNorm: company.nafNorm,
      name: company.name,
      numberEmployees: company.numberEmployees,
      phone: company.phone,
      siren: company.siren,
      siret: company.siret,
      vatNumber: company.vatNumber,
    };

    onSelect && onSelect(inputCompany);
    this.setState({ open: false });
  }

  componentDidMount() {
    document.addEventListener('click', this
      .handleClickOutside as EventListener);
  }

  componentWillUnmount() {
    this.wrapperRef = undefined;
    document.addEventListener('click', this
      .handleClickOutside as EventListener);
    const { onSearch } = this.props;
    this.setState({ value: '' });
    onSearch && onSearch('');
  }

  renderButton(company: ICompany, type: string): React.ReactNode {
    let ButtonStatus: React.ReactNode = (
      <Button onClick={this.select.bind(this, company)} type={BtnType.Default}>
        <FormattedMessage id="search.company.btn_add" />
      </Button>
    );

    if (type === 'invoices') {
      return (ButtonStatus = (
        <Button
          onClick={this.select.bind(this, company)}
          type={BtnType.Default}
        >
          {company.status === ICompanyStatus.already ? (
            <FormattedMessage id="search.company.btn_invoice_choose" />
          ) : (
            <FormattedMessage id="search.company.btn_invoice_add" />
          )}
        </Button>
      ));
    }
    switch (company.status) {
      case ICompanyStatus.self:
        if (type === 'companies' || type === 'partners') {
          ButtonStatus = (
            <Button disabled={true}>
              <FormattedMessage id="search.company.btn_self" />
            </Button>
          );
        }
        break;
      case ICompanyStatus.already:
        if (type === 'partners') {
          ButtonStatus = (
            <Button disabled={true}>
              <FormattedMessage id="search.company.btn_already" />
            </Button>
          );
        } else {
          ButtonStatus = (
            <Button disabled={true}>
              <FormattedMessage id="search.company.btn_exist" />
            </Button>
          );
        }
        break;
      case ICompanyStatus.exist:
        if (type === 'companies') {
          ButtonStatus = (
            <Button disabled={true}>
              <FormattedMessage id="search.company.btn_exist" />
            </Button>
          );
        }
        break;
      case ICompanyStatus.unknown:
        ButtonStatus = (
          <Button
            onClick={this.select.bind(this, company)}
            type={BtnType.Default}
          >
            <FormattedMessage id="search.company.btn_add" />
          </Button>
        );
        break;
    }

    return ButtonStatus;
  }

  renderSearch() {
    const { type, rows, loading, hasMore, loadMore, footer } = this.props;
    const { open, value } = this.state;

    let dataSource: any = [];
    if (!loading && rows && rows.length > 0) {
      dataSource = rows.map((item: ICompany, i: number) => {
        let concatenedAddress: string | null = null;
        if (item.addresses && item.addresses.rows && item.addresses.rows[0]) {
          const address = item.addresses.rows[0];
          concatenedAddress = `${
            address.address1 ? `${address.address1} ` : ''
          }${address.zipcode ? `${address.zipcode} ` : ''}${
            address.city ? `${address.city} ` : ''
          }${address.country ? `${address.country}` : ''}`;
        }

        return (
          <div className="search-result-item" key={`${i}`}>
            <div className="option-info">
              <div className="company-title">{item.name}</div>
              {concatenedAddress && (
                <div className="company-address">{concatenedAddress}</div>
              )}
              <div className="company-siret">
                <FormattedMessage id="search.company.siret" />: {item.siret}
              </div>
              <div>
                {item.status === ICompanyStatus.already && (
                  <Tag
                    style={{
                      marginTop: 5,
                    }}
                  >
                    <FormattedMessage id="search.company.tag_partner" />
                  </Tag>
                )}
              </div>
            </div>
            <div className="option-cta">{this.renderButton(item, type)}</div>
          </div>
        );
      });
    } else if (
      !loading &&
      rows &&
      rows.length === 0 &&
      value &&
      value.length > 0
    ) {
      dataSource = [
        <div key="0" className="search-result-item">
          <NotFound />
        </div>,
      ];
    }

    return (
      dataSource.length > 0 &&
      open && (
        <div className="search-result-outer">
          <div className="search-result">
            <Infinity hasMore={hasMore} loadMore={loadMore} useWindow={false}>
              {dataSource}
            </Infinity>
          </div>
          {footer && (
            <div
              onClick={this.handleClickFooter}
              className="search-result-footer"
            >
              {footer}
            </div>
          )}
        </div>
      )
    );
  }

  render() {
    const { form, rules, intl, type, inline } = this.props;

    return (
      <div
        ref={this.handleSetWrapperRef}
        className={`search-autocomplete ${type}`}
      >
        <Search
          rules={rules}
          form={form}
          type={IType.Search}
          onSearch={this.handleSearch}
          onChange={this.handleChange}
          onBlur={this.handleBlur}
          onFocus={this.handleFocus}
          enterButton={intl.formatMessage({
            id: 'search.company.btn_search',
          })}
          className={'input-autocomplete'}
          autoComplete={'off'}
          placeholder={
            !inline && this.props.placeholder
              ? intl.formatMessage({
                  id: this.props.placeholder,
                })
              : undefined
          }
          label={
            inline && this.props.placeholder
              ? intl.formatMessage({
                  id: this.props.placeholder,
                })
              : null
          }
        >
          {this.renderSearch()}
        </Search>
      </div>
    );
  }
}

export default injectIntl(SearchAutocomplete);
