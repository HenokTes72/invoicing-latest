import { ITreezorConfig } from './interfaces/treezor/config.interface';
import { IStrategy } from './interfaces/strategy.interface';
import { ICreateWalletParams } from './interfaces/treezor/wallet.interface';
import { ICreateUserParams, IUpdateUserParams, IUserParams, IUser } from './interfaces/treezor/user.interface';
import { IBalanceParams } from './interfaces/treezor/balance.interface';
import { ITransactionParams } from './interfaces/treezor/transaction.interface';
import { IBusinessParams, IBusinesses } from './interfaces/treezor/business.interface';
import { IDocumentParams, IDocuments, IDocument } from './interfaces/treezor/document.interface';
import { ICreateBeneficiaryParams, IBeneficiary } from './interfaces/treezor/beneficiary.interface';
import { ICreatePayoutParams, IPayout } from './interfaces/treezor/payout.interface';
import { Utils, MutationMethod } from './utils.service';
import { ICreateTaxResidenceParams, ITaxResidence, IUpdateTaxResidenceParams } from './interfaces/treezor/taxresidence.interface';

export class TreezorService implements IStrategy {
  private readonly utils: Utils;

  constructor(config: ITreezorConfig) {
    this.utils = new Utils(config);
  }

  /**
   * Create a single user
   */
  public async createUser(data: ICreateUserParams): Promise<any> {
    return this.utils.mutation('/users', { body: data });
  }

  /**
   * Update a single user
   */
  public async updateUser(data: IUpdateUserParams): Promise<any> {
    return this.utils.mutation(`/users/${data.userId}`, { body: data, method: MutationMethod.PUT });
  }

  /**
   * Change user's status to CANCELED
   */
  public async removeUser(data: any): Promise<IUser> {
    const { userId } = data;
    delete data.userId;

    return this.utils.mutation(`/users/${userId}`, { body: data, method: MutationMethod.DELETE });
  }

  /**
   * Create a new tax residence
   */
  public async createTaxResidence(data: ICreateTaxResidenceParams): Promise<ITaxResidence> {
    return this.utils.mutation('/taxResidences', { body: data });
  }

  /**
   * Update a residence already created
   */
  public async updateTaxResidence(data: IUpdateTaxResidenceParams): Promise<ITaxResidence> {
    const { taxResidenceId } = data;
    delete data.taxResidenceId;

    return this.utils.mutation(`/taxResidences/${taxResidenceId}`, { body: data, method: MutationMethod.PUT });
  }

  /**
   * Create a single wallet for a user
   */
  public async createWallet(data: ICreateWalletParams): Promise<any> {
    return this.utils.mutation('/wallets', { body: data, objectIdKey: 'walletId' });
  }

  /**
   * Create a document for a beneficiary
   */
  public async createDocument(data: any): Promise<any> {
    if (!data.file) {
      return null;
    }

    const file = await data.file;
    const base64 = new Promise((resolve, reject) => {
      const chunks = [];
      file.createReadStream()
        .on('error', reject)
        .on('data', (chunk: any) => chunks.push(chunk))
        .on('close', () => resolve(Buffer.concat(chunks).toString('base64')));
    });

    data.fileContentBase64 = await base64;
    delete data.file;

    return this.utils.mutation('/documents', { form: data, objectIdKey: 'documentId', sign: false });
  }

  /**
   * Create a beneficiary bank account
   */
  public async createBeneficiary(data: ICreateBeneficiaryParams): Promise<IBeneficiary> {
    return this.utils.mutation('/beneficiaries', { body: data });
  }

  /**
   * Create a new pay out in the system
   */
  public async createPayout(data: ICreatePayoutParams): Promise<IPayout> {
    return this.utils.mutation('/payouts', { body: data });
  }

  /**
   * Change pay out status to CANCELED. A VALIDATED pay out can't be canceled
   */
  public async deletePayout(payoutId: number): Promise<any> {
    return this.utils.mutation(`/payouts/${payoutId}`, { method: MutationMethod.DELETE });
  }

  /**
   * Remove a document from the system
   *
   * @param   {number}              documentId  - Document's internal id
   *
   * @return  {Promise<IDocument>}              - Return a document cancelled
   */
  public async deleteDocument(documentId: number): Promise<IDocument> {
    return this.utils.mutation(`/documents/${documentId}`, { method: MutationMethod.DELETE });
  }

  /**
   * Get all transactions
   */
  public async getTransactions(params: ITransactionParams): Promise<any> {
    return this.utils.query('/transactions', { qs: params });
  }

  /**
   * Get all balances
   */
  public async getBalances(params: IBalanceParams): Promise<any> {
    return this.utils.query('/balances', { qs: params });
  }

  /**
   * Get business informations with siren
   */
  public async getBusinessInformations(params: IBusinessParams): Promise<IBusinesses> {
    return this.utils.query('/businessinformations', { qs: params, sign: false });
  }

  /**
   * Get all users
   */
  public async getBeneficiaries(params: IUserParams): Promise<any> {
    return this.utils.query('/users', { qs: params });
  }

  /**
   * Get a beneficiary bank account from the system
   */
  public async getBeneficiary(beneficiaryId: number): Promise<any> {
    return this.utils.query(`/beneficiaries/${beneficiaryId}`, {})
      .then(res => {
        const { beneficiaries } = res;
        const [ beneficiary ] = beneficiaries;
        Promise.resolve(beneficiary);
      });
  }

  /**
   * Get all documents
   */
  public async getDocuments(params: IDocumentParams): Promise<IDocuments> {
    return this.utils.query('/documents', { qs: params });
  }

  /**
   * Get a single user
   */
  public async getUser(params: any): Promise<any> {
    const { userId } = params;
    delete params.userId;

    return this.utils.query(`/users/${userId}`, { qs: params })
      .then(res => {
        const { users } = res;
        const [ user ] = users;
        Promise.resolve(user);
      });
  }

  /**
   * Read the informations of a residence that match with id
   */
  public async getTaxResidence(userId: number, country: string): Promise<any> {
    return this.utils.query(`/taxResidences`, { qs: { userId } })
      .then(res => {
        const { taxResidences } = res;

        Promise.resolve(taxResidences.find((taxResidence: any) => {
          if (country === taxResidence.country && !taxResidence.isDeleted) {
            return taxResidence;
          }
        }));
      });
  }

  /**
   * Perform a KYC review for given user
   */
  public async kycReview(data: any): Promise<any> {
    const { userId } = data;
    delete data.userId;

    return this.utils.mutation(`/users/${userId}/Kycreview/`, { body: data, method: MutationMethod.PUT });
  }
}
