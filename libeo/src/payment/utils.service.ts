import * as uuid from 'uuid';
import * as crypto from 'crypto';
import * as rp from 'request-promise-native';
import { ITreezorConfig } from './interfaces/treezor/config.interface';
import { getRepository } from 'typeorm';
import { WebhooksService } from '../common/services/webhooks.service';
import { Webhook } from '../common/entities/webhook.entity';

export enum MutationMethod {
  POST   = 'POST',
  PUT    = 'PUT',
  DELETE = 'DELETE',
}

export interface MutationOptions {
  body?: any;
  form?: any;
  formData?: any;
  method?: MutationMethod;
  objectIdKey?: string;
  sign?: boolean;
}

export interface QueryOptions {
  qs?: any;
  sign?: boolean;
}

export class Utils {
  private readonly config: ITreezorConfig;
  private readonly defaultOptions: any;
  private readonly webhook: WebhooksService;

  constructor(config: ITreezorConfig) {
    this.config = config;
    this.webhook = new WebhooksService(getRepository(Webhook));
    this.defaultOptions = {
      json: true,
      method: 'POST',
      headers: {
        'Authorization': 'Bearer ' + config.token,
        'Content-Type': 'application/json',
      },
    };
  }

  /**
   * Reset default options
   */
  private resetOptions() {
    delete this.defaultOptions.qs;
    delete this.defaultOptions.body;
    delete this.defaultOptions.formData;
    this.defaultOptions.method = 'POST';
  }

  /**
   * Set global properties for security calls
   */
  private setGlobalProperties(props: any, sign: boolean = true): any {
    props.accessTag = uuid.v4();
    props.accessUserId = this.config.userId || null;
    if (sign) {
      props.accessSignature = this.getSignature(props);
    }

    return props;
  }

  /**
   * Set call options
   */
  private setMutationCallOptions(endpoint: string, options: MutationOptions, data: any): any {
    const callOptions = Object.assign(this.defaultOptions, {
      uri: this.config.baseUrl + endpoint,
    });

    if (options.method) {
      callOptions.method = options.method;
    }

    if (options.formData) {
      callOptions.formData = data;
    } else if (options.form) {
      callOptions.headers['Content-Type'] = 'application/x-www-form-urlencoded';
      callOptions.form = data;
    } else {
      callOptions.body = data;
    }

    return callOptions;
  }

  /**
   * Create and return a new signature for call API
   */
  private getSignature(value: any): string {
    return crypto
      .createHmac('sha256', this.config.secretKey)
      .update(JSON.stringify(value))
      .digest()
      .toString('base64');
  }

  /**
   * Call of type POST/PUT/DELETE
   */
  public async mutation(endpoint: string, options: MutationOptions): Promise<any> {
    this.resetOptions();
    const data = this.setGlobalProperties(options.body || options.form || options.formData || {}, options.sign || null);
    const callOptions = this.setMutationCallOptions(endpoint, options, data);

    // Save call
    this.webhook.createWebhook({
      accessTag: data.accessTag,
      requestPayload: data,
    });

    // Call
    return rp(callOptions)
      .then(res => {
        const keys = Object.keys(res);
        if (keys.length === 0) {
          return Promise.resolve();
        }

        const [ object ] = res[keys[0]];
        this.webhook.updateWebhook({ accessTag: data.accessTag }, { object_id: object[options.objectIdKey] || object.userId, responsePayload: res });
        return Promise.resolve(object);
      })
      .catch(err => {
        this.webhook.updateWebhook({ accessTag: data.accessTag }, { responsePayload: err.error });
        let { error } = err;
        const { errors } = error;
        [ error ] = errors;
        err.message = error.errorMessage;
        return Promise.reject(err);
      });
  }

  /**
   * Call of type GET
   */
  public async query(endpoint: string, options: QueryOptions): Promise<any> {
    this.resetOptions();
    const callOptions = Object.assign(this.defaultOptions, {
      uri: this.config.baseUrl + endpoint,
      qs: this.setGlobalProperties(options.qs, options.sign || null),
      method: 'GET',
    });

    return rp(callOptions)
    .catch(err => {
      let { error } = err;
      const { errors } = error;
      [ error ] = errors;
      err.message = error.errorMessage;
      return Promise.reject(err);
    });
  }
}
