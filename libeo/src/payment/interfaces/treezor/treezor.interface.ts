export interface ITreezor {
  accessSignature?: string;
  accessTag?: string;
  accessUserId?: number;
  accessUserIp?: string;
}

export enum SortOrder {
  DESC = 'DESC',
  ASC  = 'ASC',
}
