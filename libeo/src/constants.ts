const value: string = (!process.env.DOMAIN) ? 'Local' : (process.env.DATABASE_NAME === 'libeopp') ? 'Sandbox' : 'Production';
export const environmentZendesk: any = { id: 360001210059, value };
