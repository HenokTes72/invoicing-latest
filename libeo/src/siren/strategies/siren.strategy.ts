import { Strategy, Companies, Company } from '../interfaces/strategy.interface';
import { HttpException, HttpStatus } from '@nestjs/common';
import { fetch } from 'apollo-server-env';
import * as moment from 'moment';

enum Type {
  SIRET = 'siret',
  SIREN = 'siren',
  QUERY = 'raisonSociale',
}

export class SirenStrategy implements Strategy {
  private bearerToken: string = process.env.SIREN_BEARER_TOKEN;
  private readonly basicToken: string = process.env.SIREN_BASIC_TOKEN;
  public readonly baseUrl: string = process.env.SIREN_API_URL;
  public readonly endpoint: string = this.baseUrl + '/entreprises/sirene/V3';

  /**
   * Get key for search value on field
   */
  private getPrefix(str: string): string {
    const regex: any = /(^[\d+]{1,9}$)|(^[\d+]{9,14}$)|(^[a-z\d\-_\s]+$)/i;
    let prefix: string = '';
    const m: string[] = regex.exec(str);

    if (m !== null) {
      m.forEach((match, groupIndex) => {
        if (match && groupIndex === 1) {
          prefix = Type.SIREN;
        } else if (match && groupIndex === 2) {
          prefix = Type.SIRET;
        } else if (match && groupIndex === 3) {
          prefix = Type.QUERY;
        }
      });
    }

    return prefix;
  }

  /**
   * Serialize object of response API
   */
  private serialize(data: any): Company {
    const addressNumber: string = (data.adresseEtablissement.numeroVoieEtablissement) ? data.adresseEtablissement.numeroVoieEtablissement + ' ' : '';
    const addressNumberComplement: string = (data.adresseEtablissement.indiceRepetitionEtablissement) ? data.adresseEtablissement.indiceRepetitionEtablissement + ' ' : '';
    const addressStreet: string = (data.adresseEtablissement.typeVoieEtablissement && data.adresseEtablissement.libelleVoieEtablissement) ? data.adresseEtablissement.typeVoieEtablissement + ' ' + data.adresseEtablissement.libelleVoieEtablissement : '';

    return {
      siren: data.siren || null,
      siret: data.siret || null,
      name: data.uniteLegale.denominationUniteLegale || null,
      naf: data.uniteLegale.activitePrincipaleUniteLegale || null,
      nafNorm: data.uniteLegale.nomenclatureActivitePrincipaleUniteLegale || null,
      brandName: data.uniteLegale.denominationUsuelle1UniteLegale || null,
      numberEmployees: data.uniteLegale.trancheEffectifsUniteLegale || null,
      legalForm: data.uniteLegale.categorieJuridiqueUniteLegale || null,
      category: data.uniteLegale.categorieEntreprise || null,
      incorporationAt: (data.dateCreationEtablissement) ? new Date(data.dateCreationEtablissement) : null,
      vatNumber: (data.siren) ? 'FR' + (12 + 3 * (data.siren % 97)) % 97 + data.siren : null,
      addresses: {
        total: 1,
        rows: [{
          siret: data.siret || null,
          address1: addressNumber + addressNumberComplement + addressStreet,
          address2: null,
          zipcode: data.adresseEtablissement.codePostalEtablissement || null,
          city: data.adresseEtablissement.libelleCommuneEtablissement || null,
          country: 'France',
        }],
      },
    };
  }

  /**
   * Error handling
   */
  private async handleErrors(response: any, func: string, param: string): Promise<any> {
    // Refresh token if unauthorize
    if (response.status === HttpStatus.UNAUTHORIZED) {
      this.bearerToken = await this.refreshToken();
      return this[func](param);
    }

    if (response.status !== HttpStatus.OK) {
      throw new HttpException(response.statusText, response.status);
    }
  }

  /**
   * Refresh bearer token
   */
  private async refreshToken(): Promise<string> {
    const response: any = await fetch(this.baseUrl + '/token', {
      method: 'POST',
      credentials: 'include',
      body: 'grant_type=client_credentials',
      headers: {
        'Authorization': 'Basic ' + this.basicToken,
        'Content-Type': 'application/x-www-form-urlencoded',
      },
    });

    if (response.status !== HttpStatus.OK) {
      throw new HttpException(response.statusText, response.status);
    }

    const { access_token }: any = await response.json();

    return access_token;
  }

  /**
   * Research on the SIRENE API
   */
  public async search(query: string, orderBy?: string, limit?: number, offset?: number): Promise<Companies> {
    const date = moment().format('YYYY-MM-DD');
    const fields = [
      'siren',
      'siret',
      'denominationUniteLegale',
      'activitePrincipaleUniteLegale',
      'nomenclatureActivitePrincipaleUniteLegale',
      'denominationUsuelle1UniteLegale',
      'trancheEffectifsUniteLegale',
      'categorieJuridiqueUniteLegale',
      'categorieEntreprise',
      'dateCreationEtablissement',
      'numeroVoieEtablissement',
      'indiceRepetitionEtablissement',
      'typeVoieEtablissement',
      'libelleVoieEtablissement',
      'codePostalEtablissement',
      'libelleCommuneEtablissement',
    ];

    const prefix: string = this.getPrefix(query);
    let additionalConditions: string = ' AND etatAdministratifUniteLegale:"A" AND etatAdministratifUniteLegale:"A"';

    if (prefix !== Type.SIRET) {
      additionalConditions += ' AND etablissementSiege:"true"';
    }

    if (prefix === Type.QUERY) {
      let words = query.split(' ');
      const nbWords = words.length;
      words = words.map((word, index) => {
        if (index === 0) {
          return `${Type.QUERY}:"*${word}"`;
        } else if (index === (nbWords - 1)) {
          return `${Type.QUERY}:"${word}*"`;
        } else {
          return `${Type.QUERY}:"*${word}"`;
        }
      });
      query = words.join(' AND ');
    } else {
      query = `${prefix}:"${query}"`;
    }

    const url: string = `${this.endpoint}/siret?q=${query}${additionalConditions}&champs=${fields.join()}&debut=${(offset || 0)}&nombre=${(limit || 10)}&date=${date}`;

    const response: any = await fetch(url, {
      method: 'GET',
      headers: {
        Authorization: 'Bearer ' + this.bearerToken,
      },
    });

    await this.handleErrors(response, 'search', query);

    const json = await response.json();
    const companies: Company[] = [];

    await json.etablissements.forEach(async (element, index) => {
      companies.push(this.serialize(element));
    });

    return {
      total: json.header.total,
      rows: companies,
    };
  }
}
