import { SirenStrategy } from './strategies/siren.strategy';

export class SirenService {
  private readonly strategy: any = null;

  constructor(type: string) {
    if (type === 'siren') {
      this.strategy = new SirenStrategy();
    }
  }

  public async search(query: string, orderBy?: string, limit?: number, offset?: number): Promise<any> {
    return this.strategy.search(query, orderBy, limit, offset);
  }
}
