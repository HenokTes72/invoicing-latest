import { Resolver, Mutation, Args, Context } from '@nestjs/graphql';
import { HttpException, UseGuards, HttpStatus } from '@nestjs/common';
import { AuthService } from './auth.service';
import { UsersService } from '../common/services/users.service';
import { SignUpPayload } from './interfaces/signup.dto';
import { SignInPayload } from './interfaces/signin.dto';
import { User } from '../common/entities/user.entity';
import { GqlAuthGuard } from './guards/jwt-auth.guard';

@Resolver('User')
export class AuthResolvers {
  constructor(
    private readonly authService: AuthService,
    private readonly usersService: UsersService,
  ) { }

  @Mutation()
  async signup(@Context() ctx: any, @Args('input') input: SignUpPayload): Promise<User> {
    if (input.password !== input.passwordConfirmation) {
      throw new HttpException('api.error.signup.password_mismatch', HttpStatus.BAD_REQUEST);
    }

    if (input.cgu !== true) {
      throw new HttpException('api.error.signup.cgu', HttpStatus.BAD_REQUEST);
    }

    let user: User = await this.usersService.findOneByEmail(input.email);
    if (user) {
      throw new HttpException('api.error.signup_email_exist', HttpStatus.BAD_REQUEST);
    }

    delete input.cgu;
    user = await this.usersService.createUser(input);

    const baseUrl = (ctx.req.headers && ctx.req.headers.origin) ? ctx.req.headers.origin : null;
    this.usersService.confirmationToken(user, baseUrl);

    return user;
  }

  @Mutation()
  async signin(@Args('input') input: SignInPayload): Promise<User> {
    const user: User = await this.usersService.findOneByEmail(input.email);
    if (!user) {
      throw new HttpException('api.error.signin', HttpStatus.BAD_REQUEST);
    }

    if (user.enabled === false) {
      throw new HttpException('api.error.user.disabled', HttpStatus.UNAUTHORIZED);
    }

    if (user.blocked === true) {
      throw new HttpException('api.error.user.blocked', HttpStatus.UNAUTHORIZED);
    }

    if (!await this.usersService.compareHash(input.password, user.password)) {
      throw new HttpException('api.error.signin', HttpStatus.BAD_REQUEST);
    }

    user.lastLogin = new Date();
    user.token = this.authService.createToken(user.email);
    return user.save();
  }

  @Mutation()
  @UseGuards(new GqlAuthGuard())
  async logout(@Context() ctx: any): Promise<boolean> {
    const user: User = ctx.req.user;
    user.token = null;
    await user.save();

    return true;
  }
}
