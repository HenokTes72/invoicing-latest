import { Matches, IsNotEmpty, IsEmail } from 'class-validator';

export class SignUpPayload {
  @IsNotEmpty()
  @Matches(/^[a-zA-Z-\s]+$/)
  firstname: string;

  @IsNotEmpty()
  @Matches(/^[a-zA-Z-\s]+$/)
  lastname: string;

  @IsNotEmpty()
  @IsEmail()
  email: string;

  @IsNotEmpty()
  @Matches(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\!\"\#\$\%\&\'\(\)\*\+\,\-\.\/\:\;\<\=\>\?\@\[\\\]\^\_\`\{\|\}\~])(?=.{8,})/)
  password: string;

  @IsNotEmpty()
  @Matches(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\!\"\#\$\%\&\'\(\)\*\+\,\-\.\/\:\;\<\=\>\?\@\[\\\]\^\_\`\{\|\}\~])(?=.{8,})/)
  passwordConfirmation: string;

  cgu: boolean;
}
