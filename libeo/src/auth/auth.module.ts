import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { ConfigModule, ConfigService } from 'nestjs-config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthService } from './auth.service';
import { JwtStrategy } from './jwt.strategy';
import { CommonModule } from '../common/common.module';
import { User } from '../common/entities/user.entity';
import { Company } from '../common/entities/company.entity';
import { UsersService } from '../common/services/users.service';
import { CompaniesService } from '../common/services/companies.service';
import { ContactsService } from '../common/services/contacts.service';
import { EmailsService } from '../common/services/emails.service';
import { TokenGeneratorService } from '../common/services/token-generator.service';
import { AuthResolvers } from './auth.resolvers';
import { AccountingPreferencesService } from '../common/services/accounting-preferences.service';

@Module({
  imports: [
    PassportModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => configService.get('passport'),
      inject: [ConfigService],
    }),
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => configService.get('jwt'),
      inject: [ConfigService],
    }),
    TypeOrmModule.forFeature([User, Company]),
    CommonModule,
  ],
  providers: [
    AuthService,
    JwtStrategy,
    UsersService,
    CompaniesService,
    ContactsService,
    EmailsService,
    AuthResolvers,
    TokenGeneratorService,
    AccountingPreferencesService,
  ],
})
export class AuthModule { }
