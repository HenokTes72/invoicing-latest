import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';
import * as fs from 'fs';
import { join } from 'path';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();
  app.useStaticAssets(join(__dirname, '..', 'public'), {
    index: false,
    redirect: false,
  });
  app.useGlobalPipes(new ValidationPipe());
  const port = process.env.PORT || 9000;
  if (port && (typeof port === 'string') && fs.existsSync(port) && fs.lstatSync(port).isSocket()) {
    fs.unlinkSync(port);
  }
  await app.listen(port);
}
bootstrap();
