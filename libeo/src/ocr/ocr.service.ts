import * as fs from 'fs';
import { JenjiStrategy } from './strategies/jenji.strategy';

export class OcrService {
  private readonly strategy: any = null;
  private isLoadFile: boolean = false;

  constructor(type: string, config: any) {
    if (type === 'jenji') {
      this.strategy = new JenjiStrategy(config);
    }

    if (this.strategy === null) {
      throw new Error('api.error.ocr.strategy_notfound');
    }
  }

  /**
   * Load file
   */
  public async loadFile(filePath: string): Promise<any> {
    if (!fs.existsSync(filePath)) {
      throw new Error('api.error.ocr.file_notfound');
    }

    await this.strategy.loadFile(filePath);
    this.isLoadFile = true;
  }

  /**
   * Get data of API call
   */
  public async getData(): Promise<any> {
    if (this.isLoadFile === false) {
      throw new Error('api.error.ocr.file_load');
    }

    return this.strategy.getData();
  }
}
