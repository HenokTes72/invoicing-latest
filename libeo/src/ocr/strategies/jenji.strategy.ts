import * as fs from 'fs';
import * as url from 'url';
import * as request from 'request';
import * as rp from 'request-promise-native';
import { Config } from '../interfaces/config.interface';
import { Strategy } from '../interfaces/strategy.interface';

export class JenjiStrategy implements Strategy {
  private readonly config: Config;
  private file: any;

  constructor(config: Config) {
    this.config = config;
  }

  /**
   * Get basic token for API call
   */
  private getBasicToken(): string {
    return Buffer.from(this.config.username + ':' + this.config.apiKey).toString('base64');
  }

  /**
   * Load file
   */
  public async loadFile(filePath: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.file = fs.createReadStream(filePath);
      this.file.on('ready', resolve);
      this.file.on('error', reject);
    });
  }

  /**
   * Get data of API
   */
  public async getData(): Promise<any> {
    const defaultOptions = {
      json: true,
      method: 'POST',
      headers: {
        'Authorization': 'Basic ' + this.getBasicToken(),
        'Content-Type': 'multipart/form-data',
      },
      formData: {
        file: this.file,
      },
    };

    return Promise.all([
      rp(Object.assign({ uri: url.resolve(this.config.baseUrl, '/s/extract/multipart') }, defaultOptions)),
      rp(Object.assign({ uri: url.resolve(this.config.baseUrl, '/s/raw/multipart') }, defaultOptions)),
    ]);
  }
}
