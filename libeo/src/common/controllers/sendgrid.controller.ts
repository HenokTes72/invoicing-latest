
import { Repository, Any, IsNull } from 'typeorm';
import { UseInterceptors, FileInterceptor, Controller, Res, HttpStatus, Request, Body, Post, Param, HttpException, Get, MulterModule, UploadedFiles, FilesInterceptor } from '@nestjs/common';
// import { AnyFileInterceptor } from '../interceptors/anyfile.interceptor';
import { InjectRepository } from '@nestjs/typeorm';
import { ParsedMailDto } from '../dto/inbound_email.dto';
import { CreateInvoiceDto } from '../dto/invoices.dto';
import { InvoicesService } from '../services/invoices.service';
import { UsersService } from '../services/users.service';
import { Invoice } from '../entities/invoice.entity';
import { WebhooksService } from '../services/webhooks.service';
import { CompaniesService } from '../services/companies.service';
import { PaymentsService } from '../services/payments.service';
import { Company } from '../entities/company.entity';
import { PaymentRepository } from '../repositories/payment.repository';
import { User } from '../entities/user.entity';
// import { Companies } from 'src/siren/ interfaces/strategy.interface';
// import { variablesInOperation } from 'apollo-utilities';
// import { Readable } from 'stream';
// import * as streamifier from 'streamifier';
// import WriteStream from 'fs-capacitor';
import * as fs from 'fs';
// import stream = require('stream');

const acceptedTypes = ['image/jpeg', 'image/jpg', 'image/png', 'application/pdf'];
const attachmentStatus = [];
// const capacitor = new WriteStream();

@Controller('api/v1/sendgrid')
export class SendgridController {
  constructor(
    @InjectRepository(Company)
    private readonly companyRepository: Repository<Company>,
    private readonly paymentRepository: PaymentRepository,
    private readonly webhooksService: WebhooksService,
    private readonly paymentsService: PaymentsService,
    private readonly companiesService: CompaniesService,
    private readonly invoicesService: InvoicesService,
    private readonly usersService: UsersService,
  ) { }

  @Post('email')
  // @UseInterceptors(AnyFileInterceptor())
  public async newIncomingEmail(@UploadedFiles() files: any, @Body() MyBody: ParsedMailDto, @Request() req: any) {
    // regex pour récupér l'identifiant de l'email utilisé
    const companyLibeoEmail = MyBody.to.substring(0, MyBody.to.indexOf('@box.sandbox.libeo.io'));
    const emailSender = MyBody.from.substring(MyBody.from.indexOf('<') + 1, MyBody.from.indexOf('>'));
    let emailConfirmation = '';
    const invoiceUser: User = await this.usersService.findOneByEmail(emailSender);
    const receivingCompany: Company = await this.companyRepository.findOne({ where: { libeoEmail: companyLibeoEmail } });
    if (receivingCompany == null) { emailConfirmation = 'COMPANY_NOT_FOUND'; } else {
      if (files === undefined || files === 0) { emailConfirmation = 'NO_ATTACHEMENT'; } else {
        let nbNotAccepted = 0;
        files.forEach(async attachment => {
          // Check if extension is acceptable
          if (acceptedTypes.indexOf(attachment.mimetype) < 0) {
            attachmentStatus[attachment.originalname] = 'NOT_ACCEPTED';
            nbNotAccepted += 1;
          } else {
            attachmentStatus[attachment.originalname] = 'ACCEPTED';
            // Prep a file for Upload
            // tslint:disable-next-line: no-console
            console.log(attachment.buffer);
            fs.writeFileSync(`${attachment.originalname}`, attachment.buffer);
            /* function myreadstream(buffer) {
              console.log(fs.createReadStream(buffer));
              return new fs.createReadStream(buffer);
              }
              */
              /*console.log(Buffer.isBuffer(attachment.buffer));
            console.log(Buffer.isEncoding(attachment.encoding));*/
            /* const invoiceData: CreateInvoiceDto = {
              file: {
                createReadStream: myreadstream(attachment.buffer), // fs.createReadStream() // myreadstream(`${attachment.originalname}`), // createReadStream(`/public/tmp/${attachment.originalname}`);,
                filename: attachment.originalname,
                mimetype: attachment.mimetype,
                encoding: attachment.encoding,
              },
            };
            */
            // tslint:disable-next-line: no-console
            // console.log(invoiceData);
            // const invoice: Invoice = await this.invoicesService.createInvoice(invoiceUser, invoiceData);
          }
        });
        if (nbNotAccepted > 0) { emailConfirmation = 'PARTIAL'; } else { emailConfirmation = 'SUCCESS'; }
      }
    }
    // tslint:disable-next-line: no-console
    // console.log(files);
    // tslint:disable-next-line: no-console
    console.log(receivingCompany.id);
    // tslint:disable-next-line: no-console
    // console.log(invoiceUser.id);
    // tslint:disable-next-line: no-console
    console.log(emailConfirmation);
    // tslint:disable-next-line: no-console
    console.log(attachmentStatus);

    return 'All good';
  }
}

/* A gerer
- Aucun email trouvé => créer le contact
- Aucune compagnie trouvé
*/
