import * as Zendesk from 'zendesk-node-api';
import { Repository } from 'typeorm';
import { Controller, Res, HttpStatus, Body, Post, HttpException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { IWebhook } from '../interfaces/webhook.interface';
import { WebhooksService } from '../services/webhooks.service';
import { PaymentsService } from '../services/payments.service';
import { PaymentStatus, Payment } from '../entities/payment.entity';
import { InvoiceStatus } from '../entities/invoice.entity';
import { Company, CompanyKycStatus } from '../entities/company.entity';
import { Webhook } from '../entities/webhook.entity';
import { PaymentRepository } from '../repositories/payment.repository';
import { TreezorService } from '../../payment/treezor.service';
import { environmentZendesk } from '../../constants';
import { User } from '../entities/user.entity';

@Controller('api/v1/treezor')
export class TreezorController {
  constructor(
    @InjectRepository(Company)
    private readonly companyRepository: Repository<Company>,
    private readonly paymentRepository: PaymentRepository,
    private readonly webhooksService: WebhooksService,
    private readonly paymentsService: PaymentsService,
  ) {}

  @Post('webhook')
  public async webhook(@Res() res: any, @Body() body: IWebhook) {
    const webhook: Webhook = await this.webhooksService.updateWebhook({ objectId: body.object_id, objectPayload: null }, body);
    const treezor: TreezorService = new TreezorService({
      baseUrl: process.env.TREEZOR_API_URL,
      token: process.env.TREEZOR_TOKEN,
      secretKey: process.env.TREEZOR_SECRET_KEY,
    });

    if (webhook.object === 'payout') {
      const { payouts } = webhook.objectPayload;
      const [ payout ] = payouts;

      if (payout.payoutStatus === 'VALIDATED' || payout.payoutStatus === 'CANCELED') {
        const payment: Payment = await this.paymentRepository.findOne({ where: { treezorPayoutId: webhook.objectId }, relations: ['invoice'] });
        if (payment) {
          const companyReceiver: Company = await this.companyRepository.findOne({ where: { id: payment.invoice.companyReceiver.id }, relations: ['claimer'] });
          const claimer: User = companyReceiver.claimer;

          // Check if payout validated
          if (payout.payoutStatus === 'VALIDATED') {
              payment.treezorValidationAt = new Date();
              payment.status = PaymentStatus.TREEZOR_WH_VALIDATED;
              payment.invoice.status = InvoiceStatus.PAID;
              await payment.save();
          }

          // Payment canceled by Treezor
          if (payout.payoutStatus === 'CANCELED' && payment.status !== PaymentStatus.CANCELLED) {
            const zendesk = new Zendesk({
              url: process.env.ZENDESK_API_URL,
              email: process.env.ZENDESK_API_EMAIL,
              token: process.env.ZENDESK_API_TOKEN,
            });

            zendesk.tickets.create({
              type: 'incident',
              priority: 'urgent',
              requester: { name: claimer.fullName || null, email: claimer.email || null },
              subject: 'Paiement en erreur',
              comment: { body: `Le paiement ${webhook.objectId} a été envoyé sans solde à treezor et n'a pas pu être accepté.\nLe paiement sera retenté automatiquement.` },
              custom_fields: [environmentZendesk],
            });
          }
        }
      }
    }

    // Kyc review refused
    if (webhook.object === 'user') {
      const { users } = webhook.objectPayload;

      const zendesk: Zendesk = new Zendesk({
        url: process.env.ZENDESK_API_URL,
        email: process.env.ZENDESK_API_EMAIL,
        token: process.env.ZENDESK_API_TOKEN,
      });

      await Promise.all(users.map(async (user: any) => {
        if (user.userTypeId !== '2') {
          return;
        }

        const company: Company = await this.companyRepository.findOne({ where: { treezorUserId: user.userId }, relations: ['claimer'] });
        if (!company) {
          return;
        }

        if (user.isFreezed === '1' && company.isFreezed === false) {
          company.isFreezed = true;
          await company.save();

          zendesk.tickets.create({
            type: 'incident',
            priority: 'hight',
            requester: { name: company.claimer.fullName, email: company.claimer.email },
            subject: 'Blocage compagnie',
            comment: { body: `La compagnie ${company.name || company.brandName } a été gelée par Treezor. \n "CompagnieID ": (${company.id})` },
            custom_fields: [environmentZendesk],
          });
        }

        if (user.isFreezed === '0' && company.isFreezed === true) {
          company.isFreezed = false;
          await company.save();

          zendesk.tickets.create({
            type: 'incident',
            priority: 'hight',
            requester: { name: company.claimer.fullName, email: company.claimer.email },
            subject: 'Déblocage compagnie',
            comment: { body: `La compagnie ${company.name || company.brandName } a été dégelée par Treezor \n "CompagnieID ": (${company.id})` },
            custom_fields: [environmentZendesk],
          });
        }

        if (user.kycReview === '3') { // REFUSED
          company.kycStatus = CompanyKycStatus.REFUSED;
          company.treezorKycLevel = user.kycLevel;
          company.kycComment = user.kycReviewComment;
          await company.save();

          zendesk.tickets.create({
            type: 'incident',
            priority: 'hight',
            requester: { name: company.claimer.fullName, email: company.claimer.email },
            subject: 'KYC refusé',
            comment: { body: `Le KYC a été refusée pour l'entreprise ${company.name || company.brandName } et doit être investigué : \n"kycLevel": ${user.kycLevel},\n"kycReview": ${user.kycReview},\n"kycReviewComment": ${user.kycReviewComment}, \n "CompagnieID ": (${company.id})` },
            custom_fields: [environmentZendesk],
          });
        } else if (user.kycReview === '2') { // VALIDATED
          company.kycStatus = CompanyKycStatus.VALIDATED;
          company.treezorKycLevel = user.kycLevel;
          company.kycComment = user.kycReviewComment;
          await company.save();
        }
      }));
    }

    // Update Libeo balance from payin
    if (webhook.object === 'payin') {
      const { payins } = webhook.objectPayload;

      payins.forEach(async (payin: any) => {
        const company = await this.companyRepository.findOne({ where: { treezorUserId: payin.userId }, relations: ['claimer'] });
        if (company) {
          // Update Libeo balance
          await this.paymentsService.updateLibeoBalance(company);

          const { users } = await treezor.getBeneficiaries({
            userTypeId: 1,
            userStatus: 'VALIDATED',
            parentUserId: company.treezorUserId,
          });

          let sendKyc: boolean = true;
          users.forEach((user: any) => {
            if (user.country === 'US' || user.birthCountry === 'US' || user.nationality === 'US') {
              sendKyc = false;
            }
          });

          if (sendKyc) {
            if ([CompanyKycStatus.PENDING, CompanyKycStatus.REFUSED, CompanyKycStatus.VALIDATED].indexOf(company.kycStatus) === -1) {
              treezor.kycReview({ userId: payin.userId })
                .then(result => {
                  company.kycStatus = CompanyKycStatus.PENDING;
                  company.save();
                })
                .catch(err => {
                  throw new HttpException(err.message, err.statusCode);
                });
              }
          }
        }
      });
    }

    res.status(HttpStatus.OK).json(webhook);
  }
}
