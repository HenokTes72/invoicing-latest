import * as fs from 'fs';
import * as path from 'path';
import * as uuid from 'uuid';
import * as ExifTransformer from 'exif-be-gone';
import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InvoiceExtension } from '../entities/invoice.entity';
import { File } from '../interfaces/file.interface';

@Injectable()
export class FileService {
  private readonly options: any;
  private readonly mimetype: string;
  private readonly file: File;
  public originalFilename: string;
  public filename: string;

  constructor(file: File, options?: any) {
    const { filename, mimetype } = file;

    this.originalFilename = filename;
    this.mimetype = mimetype;
    this.file = file;
    this.options = Object.assign({ dir: __dirname + '/../../../public/static' }, options);
    this.filename = this.generateFilename();
  }

  /**
   * Create root directory if is not exist
   */
  public createDirectory(): void {
    if (!fs.existsSync(this.options.dir)) {
      fs.mkdirSync(this.options.dir, { recursive: true });
    }
  }

  /**
   * Upload the file
   * TODO: Set a size limit
   */
  public async upload(): Promise<any> {
    this.createDirectory();

    const filePath = path.join(this.options.dir, this.filename);
    const fsStream = fs.createWriteStream(filePath);

    return new Promise((resolve, reject) => {
      this.file.createReadStream()
        .pipe(new ExifTransformer())
        .pipe(fsStream)
        .on('error', reject)
        .on('finish', () => resolve({
          filePath,
          originalFilename: this.originalFilename,
          mimetype: this.mimetype,
        }));
    });
  }

  /**
   * Get extension of file by originalFilename
   */
  public getExtension(): string {
    const extension = /(?:\.([^.]+))?$/.exec(this.originalFilename)[1];

    if (!extension) {
      throw new HttpException('api.error.upload_extension_notfound', HttpStatus.BAD_REQUEST);
    }

    if (!(Object as any).values(InvoiceExtension).includes(this.mimetype)) {
      throw new HttpException('api.error.upload_extension_unauthorized', HttpStatus.BAD_REQUEST);
    }

    return extension;
  }

  /**
   * Generate a filename
   */
  public generateFilename(): string {
    const newFilename = uuid.v4() + '.' + this.getExtension();
    return newFilename;
  }
}
