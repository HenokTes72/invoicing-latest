import * as fs from 'fs';
import * as path from 'path';
import * as shortid from 'shortid';
import * as Zendesk from 'zendesk-node-api';
import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, In } from 'typeorm';
import { Company, CompanyStatus, CompanyKycStatus } from '../entities/company.entity';
import { SirenService } from '../../siren/siren.service';
import { Partner } from '../entities/partner.entity';
import { User } from '../entities/user.entity';
import { Email } from '../entities/email.entity';
import { List } from '../interfaces/common.interface';
import { Contact } from '../entities/contact.entity';
import { CreateOrUpdateCompanyDto } from '../dto/companies.dto';
import { TreezorService } from '../../payment/treezor.service';
import { IDocument } from '../../payment/interfaces/treezor/document.interface';
import { ITaxResidence } from '../../payment/interfaces/treezor/taxresidence.interface';
import { IBusiness, IBusinesses } from '../../payment/interfaces/treezor/business.interface';
import { IComplementaryInfos } from '../interfaces/company.interface';
import { environmentZendesk } from '../../constants';
import { AccountingPreference, AccountingPreferenceType } from '../entities/accounting-preference.entity';

@Injectable()
export class CompaniesService {
  constructor(
    @InjectRepository(Company)
    private readonly companyRepository: Repository<Company>,
    @InjectRepository(Partner)
    private readonly partnerRepository: Repository<Partner>,
    @InjectRepository(Contact)
    private readonly contactRepository: Repository<Contact>,
    @InjectRepository(Email)
    private readonly emailRepository: Repository<Email>,
    @InjectRepository(AccountingPreference)
    private readonly accountingPreferenceRepository: Repository<AccountingPreference>,
  ) { }

  /**
   * Create accounting preferences
   *
   * @param   {Company}        company  - Current company user
   *
   * @return  {Promise<void>}
   */
  private async createAccountingPreferences(company: Company): Promise<void> {
    if (!company.claimer) {
      return;
    }

    await this.accountingPreferenceRepository.save([
      {
        enabled: true,
        id: 'b55e2f75-9b69-4b99-8fd6-2df3380a4c35',
        order: 1,
        company,
        key: 'Journal de banque',
        value: 'BAN',
        description: null,
        type: AccountingPreferenceType.LEDGER_BANK
      },
      {
        enabled: true,
        id: 'f63cd914-f4b3-402e-8146-d76beea81d20',
        order: 0,
        company,
        key: 'Journal d\'achat',
        value: 'ACH',
        description: null,
        type: AccountingPreferenceType.LEDGER_PURCHASE
      },
      {
        enabled: true,
        id: '5cbbff20-9713-4993-b839-d42468a3c14c',
        order: 2,
        company,
        key: 'Journal de vente',
        value: 'VEN',
        description: null,
        type: AccountingPreferenceType.LEDGER_SALES
      },
      {
        enabled: true,
        id: '3ccb491a-114f-4630-ae10-86d126669d37',
        order: 0,
        company,
        key: 'Compte TVA déductible',
        value: '445660',
        description: null,
        type: AccountingPreferenceType.VAT_ACCOUNT
      },
      {
        enabled: true,
        id: '9e432feb-d41b-4c9a-a9b4-ffc07f3cf8d5',
        order: 0,
        company,
        key: 'Compte banque Libeo',
        value: '512000',
        description: null,
        type: AccountingPreferenceType.BANK_ACCOUNT_TREEZOR
      }
    ]);
  }

  /**
   * Create a company shell (empty)
   */
  public async createCompanyShell(user: User): Promise<Company> {
    const company: Company = this.companyRepository.create();
    await this.companyRepository.save(company);
    user.currentCompany = company;
    await user.save();

    return company;
  }

  /**
   * Create or update a comapny
   */
  public async createOrUpdateCompany(user: User, data?: CreateOrUpdateCompanyDto, id?: string): Promise<Company> {
    if (!data) {
      return this.createCompanyShell(user);
    }

    if (id) {
      let myCompany: Company = await this.companyRepository.findOne({ id });
      if (!myCompany) {
        throw new HttpException('api.error.company.not_found', HttpStatus.NOT_FOUND);
      }

      myCompany = Object.assign(myCompany, data);
      await this.companyRepository.save(myCompany);

      return myCompany;
    }

    let company: Company = await this.findOneBySiren(data.siren);
    if (company) {
      throw new HttpException('api.error.company.already', HttpStatus.BAD_REQUEST);
    }

    // Cast incorporationAt
    if (data && data.incorporationAt) {
      data.incorporationAt = new Date(+data.incorporationAt);
    }

    const currentCompany: Company = user.currentCompany;
    if (!currentCompany) {
      // Create and set currentCompany from a user (User without company)
      company = await this.companyRepository.create(data);
      company.claimer = user;
      await company.save();
      user.currentCompany = company;
      await user.save();
    } else if (currentCompany.siret === null) {
      // Update currentCompany from a user (User with company shell)
      company = Object.assign(currentCompany, data);
      company.claimer = user;
      await company.save();
    } else {
      // Create a new company without set currentCompany from user (User with company)
      company = await this.companyRepository.create(data);
      company.claimer = user;
      await company.save();
    }

    // Create a email for contact
    const email: Email = new Email();
    email.email = user.email;
    email.visibleOnlyCompany = company.id;
    await this.emailRepository.save(email);

    // Create a contact for claimer user
    const contact: Contact = new Contact();
    contact.firstname = user.firstname;
    contact.lastname = user.lastname;
    contact.visibleOnlyCompany = company.id;
    contact.emails = [email];
    contact.user = user;
    contact.company = company;
    await this.contactRepository.save(contact);

    await this.createAccountingPreferences(company);

    return company;
  }

  /**
   * Search companies on a external API by name/siret/siren
   */
  public async searchCompanies(query: string, orderBy?: string, limit?: number, offset?: number) {
    return new SirenService('siren').search(query, orderBy, limit, offset);
  }

  /**
   * Get a single company by ID
   */
  public async findOneById(id: string): Promise<Company> {
    return this.companyRepository.findOne({ id });
  }

  /**
   * Get a single company by siren
   */
  public async findOneBySiren(siren: string): Promise<Company> {
    return this.companyRepository.findOne({ siren });
  }

  /**
   * Get a single company by siret
   */
  public async findOneBySiret(siret: string): Promise<Company> {
    return this.companyRepository.findOne({ siret });
  }

  /**
   * Get current company by user
   */
  public async getCurrentCompanyByUser(user: User): Promise<Company> {
    if (!user.currentCompany) {
      return this.createCompanyShell(user);
    }

    return user.currentCompany;
  }

  /**
   * Get a status for a single company
   */
  public async getStatus(user: User, company: Company): Promise<CompanyStatus> {
    if (!user) {
      return null;
    }

    const currentCompany = user.currentCompany;
    if (currentCompany && currentCompany.siret === company.siret) {
      return CompanyStatus.SELF;
    }

    const companyPartner = await this.companyRepository.findOne({ siret: company.siret });
    const nbPartners = await this.partnerRepository.count({ companyInitiator: currentCompany, companyPartner });
    if (nbPartners > 0) {
      return CompanyStatus.ALREADY;
    }

    if (await this.findOneBySiret(company.siret)) {
      return CompanyStatus.EXIST;
    }

    return CompanyStatus.UNKNOWN;
  }

  /**
   * Get companies by user
   */
  public async findByUser(user: User, orderBy?: string, limit?: number, offset?: number): Promise<List> {
    const contacts: Contact[] = await this.contactRepository.find({ user: { id: user.id } });
    if (contacts.length === 0) {
      return {
        total: 0,
        rows: [],
      };
    }

    const companyIds: string[] = contacts.map(contact => {
      if (contact.company) {
        return contact.company.id;
      }

      return null;
    });

    const result = await Promise.all([
      await this.companyRepository.count({ where: { id: In(companyIds) } }),
      await this.companyRepository.find({ where: { id: In(companyIds) }, take: limit, skip: offset }),
    ]);

    return {
      total: result[0],
      rows: result[1],
    };
  }

  /**
   * Get (Generate) a contact for a company
   */
  public async getContract(company: Company): Promise<string> {
    const rootDirectory: string = path.resolve(`${__dirname}/../../../public/static`);
    const destinationDirectory: string = path.resolve(`${rootDirectory}/companies/${company.id}`);

    if (!fs.existsSync(destinationDirectory)) {
      fs.mkdirSync(destinationDirectory, { recursive: true });
    }

    try {
      // We copy the contract but later we will have to generate it.
      await fs.copyFileSync(`${rootDirectory}/contract/contract.pdf`, `${destinationDirectory}/contract.pdf`);
    } catch (err) {
      throw new HttpException('api.error.company.contract', HttpStatus.BAD_REQUEST);
    }

    return `companies/${company.id}/contract.pdf`;
  }

  /**
   * Sign the contract of company
   */
  public async signContract(user: User): Promise<boolean> {
    let company: Company = user.currentCompany;
    if (!company) {
      throw new HttpException('api.error.company.notfound', HttpStatus.NOT_FOUND);
    }

    company.signature = {
      userId: user.id,
      signedAt: new Date(),
    };

    await company.save();

    const treezor: TreezorService = new TreezorService({
      baseUrl: process.env.TREEZOR_API_URL,
      token: process.env.TREEZOR_TOKEN,
      secretKey: process.env.TREEZOR_SECRET_KEY,
    });

    const { users } = await treezor.getBeneficiaries({
      userTypeId: 1,
      userStatus: 'VALIDATED',
      parentUserId: company.treezorUserId,
    });

    let sendKyc: boolean = true;
    users.forEach((physicalUser: any) => {
      if (physicalUser.country === 'US' || physicalUser.birthCountry === 'US' || physicalUser.nationality === 'US') {
        sendKyc = false;
      }
    });

    if (!sendKyc) {
      company = await this.companyRepository.findOne({ where: { id: company.id }, relations: ['claimer'] });
      const zendesk = new Zendesk({
        url: process.env.ZENDESK_API_URL,
        email: process.env.ZENDESK_API_EMAIL,
        token: process.env.ZENDESK_API_TOKEN,
      });

      zendesk.tickets.create({
        type: 'incident',
        priority: 'hight',
        requester: { name: company.claimer.fullName, email: company.claimer.email },
        subject: 'KYC FATCA',
        comment: { body: `Le KYC necessite des documents complémentaires pour l'entreprise ${company.name}` },
        custom_fields: [environmentZendesk],
      });
    }

    return true;
  }

  /**
   * Create a beneficiary with his documents
   */
  public async createBeneficiary(user: User, data: any): Promise<any> {
    const company: Company = user.currentCompany;
    if (!company) {
      throw new HttpException('api.error.company.notfound', HttpStatus.NOT_FOUND);
    }

    const { documents } = data;
    let beneficiary = null;
    const treezor: TreezorService = new TreezorService({
      baseUrl: process.env.TREEZOR_API_URL,
      token: process.env.TREEZOR_TOKEN,
      secretKey: process.env.TREEZOR_SECRET_KEY,
      // userId: company.treezorUserId,
    });

    data.userTag = {};
    if (data.isCurrentUser) {
      data.userTag.userId = user.id;
    }

    if (data.isHosted) {
      data.userTag.isHosted = true;
    }

    data.userTag = JSON.stringify(data.userTag);
    delete data.isCurrentUser;
    delete data.isHosted;
    delete data.documents;
    const { taxResidence } = data;
    delete data.taxResidence;
    data.userTypeId = 1;
    data.parentUserId = company.treezorUserId;
    data.parentType = 'leader'; // TODO: deprecated but required
    if (!data.userId) {
      data.email = `payment.${shortid.generate()}@libeo.io`;
    }

    try {
      beneficiary = (data.userId) ? await treezor.updateUser(data) : await treezor.createUser(data);
    } catch (err) {
      throw new HttpException(err.message, HttpStatus.BAD_GATEWAY);
    }

    // Create or Update and return the tax residence
    if (taxResidence) {
      data.taxResidence = await this.createOrUpdateTaxResidence(beneficiary.userId, taxResidence, beneficiary.country);
    }

    if (!documents) {
      return beneficiary;
    }

    const promiseDocuments: any[] = documents.map(document => {
      document.userId = beneficiary.userId;
      return treezor.createDocument(document);
    });

    try {
      beneficiary.documents = await Promise.all(promiseDocuments);
    } catch (err) {
      throw new HttpException(err.message, HttpStatus.BAD_GATEWAY);
    }

    return beneficiary;
  }

  /**
   * Remove a beneficiary
   */
  public async removeBeneficiary(company: Company, userId: number): Promise<any> {
    const treezor = new TreezorService({
      baseUrl: process.env.TREEZOR_API_URL,
      token: process.env.TREEZOR_TOKEN,
      secretKey: process.env.TREEZOR_SECRET_KEY,
      userId: company.treezorUserId,
    });

    try {
      return await treezor.removeUser({ userId, origin: 'USER' });
    } catch (err) {
      throw new HttpException(err.message, HttpStatus.BAD_GATEWAY);
    }
  }

  /**
   * Get a tax residence
   *
   * @param   {number}                  userId   - User's id residence
   * @param   {string}                  country  - Country of the resident
   *
   * @return  {Promise<ITaxResidence>}           - Retuns a tax residence
   */
  public async getTaxResidence(userId: number, country: string): Promise<ITaxResidence> {
    const treezor: TreezorService = new TreezorService({
      baseUrl: process.env.TREEZOR_API_URL,
      token: process.env.TREEZOR_TOKEN,
      secretKey: process.env.TREEZOR_SECRET_KEY,
    });

    try {
      return await treezor.getTaxResidence(userId, country);
    } catch (err) {
      throw new HttpException(err.message, err.statusCode);
    }
  }

  /**
   * Create or update a tax residence
   *
   * @param   {number}        userId      - User's id residence
   * @param   {string}        taxPayerId  - Tax payer's id
   * @param   {string}        country     - Country of the resident
   *
   * @return  {Promise<any>}              - Return a tax residence Treezor
   */
  public async createOrUpdateTaxResidence(userId: number, taxPayerId: string, country: string): Promise<any> {
    let taxResidence: any = null;
    const treezor: TreezorService = new TreezorService({
      baseUrl: process.env.TREEZOR_API_URL,
      token: process.env.TREEZOR_TOKEN,
      secretKey: process.env.TREEZOR_SECRET_KEY,
    });

    try {
      taxResidence = await treezor.getTaxResidence(userId, country);
    } catch (err) {
      throw new HttpException(err.message, err.statusCode);
    }

    if (!taxResidence) {
      try {
        taxResidence = await treezor.createTaxResidence({
          country,
          taxPayerId: (taxPayerId) ? taxPayerId : null,
          userId,
          liabilityWaiver: (!taxPayerId) ? true : false,
        });
      } catch (err) {
        throw new HttpException(err.message, err.statusCode);
      }
    }

    taxPayerId = (taxPayerId) ? taxPayerId : '';
    if (taxResidence && (taxResidence.taxPayerId !== taxPayerId || taxResidence.country !== country)) {
      try {
        taxResidence = await treezor.updateTaxResidence({
          taxResidenceId: taxResidence.id,
          country,
          taxPayerId: (taxPayerId) ? taxPayerId : null,
          userId,
          liabilityWaiver: (!taxPayerId) ? true : false,
        });
      } catch (err) {
        throw new HttpException(err.message, err.statusCode);
      }
    }

    return taxResidence;
  }

  /**
   * Remove a document Treezor
   *
   * @param   {number}              documentId  - Document's ID Treezor
   *
   * @return  {Promise<IDocument>}              - Return a document cancelled
   */
  public async removeDocument(documentId: number): Promise<IDocument> {
    const treezor = new TreezorService({
      baseUrl: process.env.TREEZOR_API_URL,
      token: process.env.TREEZOR_TOKEN,
      secretKey: process.env.TREEZOR_SECRET_KEY,
    });

    try {
      // TODO: Check if my document
      return await treezor.deleteDocument(documentId);
    } catch (err) {
      throw new HttpException(err.message, err.statusCode);
    }
  }

  /**
   * Get all representatives of current company
   */
  public async getRepresentatives(company: Company): Promise<List> {
    if (!company || !company.siren) {
      return {
        total: 0,
        rows: [],
      };
    }

    let info = null;
    const treezor = new TreezorService({
      baseUrl: process.env.TREEZOR_API_URL,
      token: process.env.TREEZOR_TOKEN,
      secretKey: process.env.TREEZOR_SECRET_KEY,
    });

    try {
      const { businessinformations } = await treezor.getBusinessInformations({
        country: 'FR',
        registrationNumber: company.siren,
      });
      if (businessinformations && businessinformations.length > 0) {
        info = businessinformations[0];
      } else {
        return {
          total: 0,
          rows: [],
        };
      }
    } catch (err) {
      throw new HttpException(err.message, err.statusCode);
    }

    const users = info.users;

    return {
      total: users.length,
      rows: users,
    };
  }

  /**
   * Get all beneficiary bank accounts
   */
  public async getBeneficiaries(company: Company, limit?: number, page?: number): Promise<List> {
    if (!company || !company.treezorUserId) {
      return {
        total: 0,
        rows: [],
      };
    }

    const treezor = new TreezorService({
      baseUrl: process.env.TREEZOR_API_URL,
      token: process.env.TREEZOR_TOKEN,
      secretKey: process.env.TREEZOR_SECRET_KEY,
      userId: company.treezorUserId,
    });

    try {
      const { users } = await treezor.getBeneficiaries({
        userTypeId: 1,
        userStatus: 'VALIDATED',
        parentUserId: company.treezorUserId,
      });
      return {
        total: users.length,
        rows: users,
      };
    } catch (err) {
      throw new HttpException(err.message, err.statusCode);
    }
  }

  /**
   * Get all documents of company
   */
  public async getDocuments(userId: number, limit?: number, page?: number): Promise<List> {
    const treezor = new TreezorService({
      baseUrl: process.env.TREEZOR_API_URL,
      token: process.env.TREEZOR_TOKEN,
      secretKey: process.env.TREEZOR_SECRET_KEY,
    });

    try {
      const { documents } = await treezor.getDocuments({
        userId,
        pageCount: limit,
        pageNumber: page,
      });
      return {
        total: documents.length,
        rows: documents,
      };
    } catch (err) {
      throw new HttpException(err.message, err.statusCode);
    }
  }

  /**
   * Update kyc status of company
   */
  public async updateKycStatus(user: User, status: CompanyKycStatus): Promise<Company> {
    const company: Company = user.currentCompany;
    if (!company) {
      throw new HttpException('api.error.company.notfound', HttpStatus.NOT_FOUND);
    }

    company.kycStatus = status;
    await company.save();

    return company;
  }

  /**
   * Save current KYC step
   *
   * @param   {Company}           company  - Current company
   * @param   {string}            step     - Current KYC step
   *
   * @return  {Promise<Company>}           - Return the updated current company
   */
  public async updateKycStep(company: Company, step: string): Promise<Company> {
    if (!company) {
      throw new HttpException('api.error.company.notfound', HttpStatus.NOT_FOUND);
    }

    company.kycStep = step;
    await company.save();

    return company;
  }

  /**
   * Get all already known business information at Treezor
   *
   * @param   {string}            siren  - Siren of company
   *
   * @return  {Promise<Company>}         - Returns the company with additional information
   */
  public async getCompanyComplementaryInfos(siren: string): Promise<IComplementaryInfos> {
    const treezor: TreezorService = new TreezorService({
      baseUrl: process.env.TREEZOR_API_URL,
      token: process.env.TREEZOR_TOKEN,
      secretKey: process.env.TREEZOR_SECRET_KEY,
    });

    try {
      const { businessinformations }: IBusinesses = await treezor.getBusinessInformations({
        country: 'FR',
        registrationNumber: siren,
      });

      const [info]: IBusiness[] = businessinformations;

      return {
        capital: Number(info.legalShareCapital) || null,
        legalAnnualTurnOver: info.legalAnnualTurnOver || null,
        numberEmployees: info.legalNumberOfEmployeeRange || null,
        legalNetIncomeRange: info.legalNetIncomeRange || null,
        phone: (info.phone) ? info.phone.split(' ').join('') : null,
      };
    } catch (err) {
      throw new HttpException(err.message, err.statusCode);
    }
  }
}
