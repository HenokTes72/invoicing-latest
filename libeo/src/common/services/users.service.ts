import * as bcrypt from 'bcryptjs';
import * as mailjet from 'node-mailjet';
import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from '../entities/user.entity';
import { Company } from '../entities/company.entity';
import { CreateUserDto, UpdateUserDto } from '../dto/users.dto';
import { TokenGeneratorService } from './token-generator.service';

@Injectable()
export class UsersService {
  private readonly saltRounds: number = 10;

  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
    @InjectRepository(Company)
    private readonly companyRepository: Repository<Company>,
    private readonly tokenGeneratorService: TokenGeneratorService,
  ) { }

  /**
   * Create a user
   */
  public async createUser(data: CreateUserDto): Promise<User> {
    const user = new User();
    user.firstname = data.firstname;
    user.lastname = data.lastname;
    user.email = data.email;
    user.password = data.password;
    user.enabled = true;

    // Encrypt password
    user.password = await this.getHash(data.password);

    await user.save();

    return user;
  }

  /**
   * Get current company by user
   */
  public async findMyCompanyByUser(user: User): Promise<Company> {
    return this.companyRepository.findOne({ id: user.currentCompany.id });
  }

  /**
   * Get a single user by email
   */
  public async findOneByEmail(email: string): Promise<User> {
    return this.userRepository.findOne({ email: email.toLocaleLowerCase() });
  }

  /**
   * Encrypt password with Bcrypt
   */
  public async getHash(password: string | undefined): Promise<string> {
    return bcrypt.hash(password, this.saltRounds);
  }

  /**
   * Compare passwords with Bcrypt
   */
  public async compareHash(password: string | undefined, hash: string | undefined): Promise<boolean> {
    return bcrypt.compare(password, hash);
  }

  /**
   * Generate a confirmation token and send a mail in user
   */
  public async confirmationToken(user: User, baseUrl: string): Promise<User> {
    user.enabled = false;
    user.confirmationToken = this.tokenGeneratorService.generateToken();
    await user.save();

    // Send code by mail (Mailjet)
    mailjet
      .connect(process.env.MAILJET_APIKEY, process.env.MAILJET_SECRETKEY)
      .post('send', { version: 'v3.1' })
      .request({
        Messages: [{
          From: {
            Email: 'lucas@libeo.io',
            Name: 'Service Client Libeo',
          },
          To: [{
            Email: user.email,
            Name: user.fullName,
          }],
          TemplateID: 705763,
          TemplateLanguage: true,
          Subject: 'Activez votre compte Libeo',
          Variables: {
            FullName: user.fullName,
            Email_validation_link: (baseUrl) ? `${baseUrl}/login/${user.confirmationToken}?email=${user.email}` : `https://libeoweb.livedemo.fr/${user.confirmationToken}?email=${user.email}`,
          },
        }],
      })
      .catch(err => {
        throw new HttpException('api.error.user.mailjet', err.statusCode);
      });

    return user;
  }

  /**
   * Enabled user by confirmation token
   */
  public async activationUser(confirmationToken: string): Promise<boolean> {
    const user = await this.userRepository.findOne({ enabled: false, confirmationToken });
    if (!user) {
      throw new HttpException('api.error.user.invalid_confirmation_token', HttpStatus.BAD_REQUEST);
    }

    user.enabled = true;
    user.confirmationToken = null;
    await user.save();

    return true;
  }

  /**
   * Update current user
   *
   * @param   {User}           user  - Current user
   * @param   {UpdateUserDto}  data  - Date of user
   *
   * @return  {Promise<User>}        - Returns updated current user
   */
  public async updateUser(user: User, data: UpdateUserDto): Promise<User> {
    if (data.password) {
      data.password = await this.getHash(data.password);
    }

    user = Object.assign(user, data);
    await user.save();

    return user;
  }
}
