import * as Zendesk from 'zendesk-node-api';
import { Injectable, HttpException, HttpStatus, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Payment, PaymentStatus } from '../entities/payment.entity';
import { InvoicesService } from './invoices.service';
import { Company, CompanyKycStatus } from '../entities/company.entity';
import { Invoice, InvoiceStatus } from '../entities/invoice.entity';
import { User } from '../entities/user.entity';
import { BalancesService } from './balances.service';
import { TreezorService } from '../../payment/treezor.service';
import { Iban } from '../entities/iban.entity';
import { IBeneficiary } from '../../payment/interfaces/treezor/beneficiary.interface';
import { Cron, NestSchedule } from 'nest-schedule';
import { PaymentRepository } from '../repositories/payment.repository';
import { Repository, In } from 'typeorm';
import { IBalance } from '../../payment/interfaces/treezor/balance.interface';
import { IPayout } from '../../payment/interfaces/treezor/payout.interface';
import { ContactsService } from './contacts.service';
import { Contact } from '../entities/contact.entity';
import { environmentZendesk } from '../../constants';
import { PaymentNotification } from '../entities/payment-notification.entity';

@Injectable()
export class PaymentsService extends NestSchedule {
  constructor(
    @InjectRepository(PaymentRepository)
    private readonly paymentRepository: PaymentRepository,
    @InjectRepository(Invoice)
    private readonly invoiceRepository: Repository<Invoice>,
    @InjectRepository(Company)
    private readonly companyRepository: Repository<Company>,
    @InjectRepository(PaymentNotification)
    private readonly paymentNotificationRepository: Repository<PaymentNotification>,
    private readonly invoicesService: InvoicesService,
    private readonly balancesService: BalancesService,
    private readonly contactsService: ContactsService,
  ) {
    super();
  }

  /**
   * Send an email to the selected contacts
   *
   * @param   {Company}           currentCompany  - Current company
   * @param   {[type]}            invoiceId       - Invoice's id
   * @param   {string[]}          contactIds      - Contact's ids
   *
   * @return  {Promise<boolean>}
   */
  public async payoutContacts(user: User, invoiceId: string, contactIds: string[]): Promise<boolean> {
    const invoice: Invoice = await this.invoicesService.findOneByIdAndCurrentCompany(invoiceId, user.currentCompany);
    if (!invoice) {
      throw new HttpException('api.error.invoice.notfound', HttpStatus.NOT_FOUND);
    }

    const contacts: Contact[] = await this.contactsService.findByCompanyAndIds(invoice.companyEmitter, contactIds, user.currentCompany);

    if (contacts.length === 0) {
      return false;
    }

    const To: any[] = [];
    await Promise.all(contacts.map(async contact => {
      const paymentNotification: PaymentNotification = this.paymentNotificationRepository.create({
        contact,
        invoice,
        createdBy: user,
      });

      await paymentNotification.save();

      // if (contact.emails && contact.emails.length > 0) {
      //   const [ email ]: Email[] = contact.emails;
      //   To.push({ Email: email.email, Name: contact.fullName });
      // }
    }));

    // Send code by mail (Mailjet)
    // mailjet
    //  .connect(process.env.MAILJET_APIKEY, process.env.MAILJET_SECRETKEY)
    //  .post('send', { version: 'v3.1' })
    //  .request({
    //    Messages: [{
    //      From: { Email: 'lucas@libeo.io', Name: 'Service Client Libeo' },
    //      To,
    //      Subject: 'Une facture a été payée',
    //      TextPart: `La facture de ${invoice.companyEmitter.name || invoice.companyEmitter.brandName} d'un montant de ${invoice.total}${invoice.currency} TTC a été payée par ${user.fullName}.`,
    //    }],
    //  })
    //    .catch(err => {
    //      console.log(err.message, To);
    //      throw new HttpException('api.error.user.mailjet', err.statusCode);
    //    });

    return true;
  }

  /**
   * Create a new payment
   *
   * @param   {User}              user       - Current user
   * @param   {string}            invoiceId  - Invoice's ID
   * @param   {Date}              date       - Payment planning date (optional)
   * @param   {string}            code       - Validation code to pay the invoice (optional)
   *
   * @return  {Promise<Invoice>}             - Returns the paid invoice
   */
  public async payout(user: User, invoiceId: string, date?: Date, code?: string): Promise<Invoice> {
    // Check invoice status
    const invoice: Invoice = await this.invoicesService.findOneByIdAndCurrentCompany(invoiceId, user.currentCompany);
    if (invoice.status !== InvoiceStatus.TO_PAY) {
      throw new HttpException('api.error.invoice.invalid_status', HttpStatus.BAD_REQUEST);
    }

    // Check security code
    if ((invoice.code && !invoice.codeValidatedBy) || code) {
      await this.invoicesService.checkCode(invoiceId, code);
    }

    // Check KYC
    // if (!await this.checkKyc(invoice.companyReceiver)) {
    //   throw new HttpException('api.error.payment.kyc', HttpStatus.BAD_REQUEST);
    // }

    // TODO: Check contact

    // Create object payment
    const payment: Payment = this.paymentRepository.create({
      treezorPayerWalletId: invoice.companyReceiver.treezorWalletId,
      status: PaymentStatus.REQUESTED,
      amount: invoice.total,
      currency: invoice.currency,
      invoice,
      paymentAt: (date) ? new Date(date) : new Date(),
      paymentRequestUser: user,
      libeoEstimatedBalance: 0,
    });

    // Synchronization of the invoice status
    invoice.status = InvoiceStatus.PLANNED;

    if (invoice.iban && invoice.iban.treezorBeneficiaryId) {
      payment.treezorBeneficiaryId = invoice.iban.treezorBeneficiaryId;
    }

    // Save payment and invoice
    await Promise.all([
      payment.save(),
      invoice.save(),
    ]);

    // Update balance
    this.updateLibeoBalance(invoice.companyReceiver, payment);

    // Create a new benefeciary bank account in Treezor
    if (invoice.iban && !invoice.iban.treezorBeneficiaryId) {
      this.createBeneficiary(invoice, payment);
    }

    return invoice;
  }

  /**
   * Create a new beneficiary bank account at Treezor
   *
   * @param   {Invoice}                invoice  - Invoice with data
   * @param   {Payment}                payment  - Current payment (optional)
   *
   * @return  {Promise<IBeneficiary>}           - Returns the bank account of the created beneficiary
   */
  public async createBeneficiary(invoice: Invoice, payment?: Payment): Promise<IBeneficiary> {
    const treezor: TreezorService = new TreezorService({
      baseUrl: process.env.TREEZOR_API_URL,
      token: process.env.TREEZOR_TOKEN,
      secretKey: process.env.TREEZOR_SECRET_KEY,
      userId: invoice.companyEmitter.treezorUserId,
    });

    try {
      const beneficiary: IBeneficiary = await treezor.createBeneficiary({
        body: {
          tag: invoice.iban.id,
          userId: invoice.companyReceiver.treezorUserId,
          nickName: invoice.companyReceiver.name + ' -> ' + invoice.companyEmitter.name,
          name: invoice.companyEmitter.name,
          iban: invoice.iban.iban,
          bic: invoice.iban.bic,
          usableForSct: true,
        },
      });

      const iban: Iban = invoice.iban;
      iban.treezorBeneficiaryId = beneficiary.id;
      await iban.save();

      if (payment) {
        payment.treezorBeneficiaryId = beneficiary.id;
        await payment.save();
      }

      return beneficiary;
    } catch (err) {
      throw new HttpException(err.message, err.statusCode);
    }
  }

  /**
   * Check the company's KYCs (moral user at Treezor)
   *
   * @param   {Company}           company  - The company to be controlled
   *
   * @return  {Promise<boolean>}           - Returns false if the KYCs were not submitted to Treezor, otherwise true
   */
  public async checkKyc(company: Company): Promise<boolean> {
    const treezorStatuses: CompanyKycStatus[] = [CompanyKycStatus.PENDING, CompanyKycStatus.REFUSED, CompanyKycStatus.VALIDATED];
    if (treezorStatuses.indexOf(company.kycStatus) === -1) {
      return false;
    }

    return true;
  }

  /**
   * Get a single payment by invoice
   *
   * @param   {Invoice}           invoice  - Invoice attached to the payment
   *
   * @return  {Promise<Payment>}           - Returns the payment of the invoice
   */
  public async findOneByInvoice(invoice: Invoice): Promise<Payment> {
    const payment: Payment = await this.paymentRepository.findOne({ invoice });
    if (!payment) {
      throw new HttpException('api.error.payment.notfound', HttpStatus.NOT_FOUND);
    }

    return payment;
  }

  /**
   * Update the field libeoEstimatedBalance of a payment
   *
   * @param   {Company}          company         - Target company
   * @param   {Payment}          currentPayment  - Current payment (optional)
   *
   * @return  {Promise<void>}
   */
  public async updateLibeoBalance(company: Company, currentPayment?: Payment): Promise<void> {
    const [ balance, payments ]: [IBalance, Payment[]] = await Promise.all([
      this.balancesService.getBalance(company),
      this.paymentRepository.getPlannedPayments(company, (currentPayment) ? currentPayment.paymentAt : null),
    ]);

    let calculationLibeoBalance: number = Number(balance.authorizedBalance);

    if (currentPayment) {
      calculationLibeoBalance = await this.balancesService.calculationLibeoBalance(balance, currentPayment.invoice.dueDate);
      currentPayment.libeoEstimatedBalance = calculationLibeoBalance;
      await currentPayment.save();

      if (calculationLibeoBalance < 0) {
        return;
      }
    }

    try {
      await Promise.all(payments.map((payment: Payment) => {
        payment.libeoEstimatedBalance = calculationLibeoBalance - payment.amount;

        if (payment.libeoEstimatedBalance > 0) {
          calculationLibeoBalance = payment.libeoEstimatedBalance;
        }

        return payment.save();
      }));
    } catch (err) {
      throw new HttpException(err.message, HttpStatus.BAD_REQUEST);
    }
  }

  /**
   * Check if the beneficiary bank account has been created at Treezor
   *
   * @param   {number}            beneficiaryId  - Beneficiary's ID Treezor
   *
   * @return  {Promise<boolean>}                 - Returns true if the beneficiary bank account has been found, otherwise false
   */
  public async checkBeneficiary(beneficiaryId: number): Promise<boolean> {
    const treezor: TreezorService = new TreezorService({
      baseUrl: process.env.TREEZOR_API_URL,
      token: process.env.TREEZOR_TOKEN,
      secretKey: process.env.TREEZOR_SECRET_KEY,
    });

    try {
      const beneficiary: any = await treezor.getBeneficiary(beneficiaryId);
      if (beneficiary) {
        return true;
      }
    } catch (err) {
      throw new HttpException(err.message, err.statusCode);
    }

    return false;
  }

  /**
   * Deferred payments sent at 5am and 8:30am
   *
   * @return {Promise<void>}
   */
  // @Cron('0 5 * * *')
  // @Cron('30 8 * * *')
  @Cron('* * * * *')
  public async deferredPayments(): Promise<void> {
    const logger = new Logger();
    const payments: Payment[] = await this.paymentRepository.getDeferredPayments([
      PaymentStatus.REQUESTED,
      PaymentStatus.TREEZOR_SYNC_KO_NOT_ENOUGH_BALANCE,
      PaymentStatus.TREEZOR_SYNC_KO_MISC,
      PaymentStatus.TREEZOR_WH_KO_NOT_ENOUGH_BALANCE,
      PaymentStatus.TREEZOR_WH_KO_MISC,
    ]);

    logger.log(`${payments.length} deferred payments`);

    if (payments.length === 0) {
      return;
    }

    const treezor: TreezorService = new TreezorService({
      baseUrl: process.env.TREEZOR_API_URL,
      token: process.env.TREEZOR_TOKEN,
      secretKey: process.env.TREEZOR_SECRET_KEY,
    });

    await Promise.all(payments.map(async (payment: Payment) => {
      payment.status = PaymentStatus.SENT_TO_TREEZOR;
      try {
        await payment.save();
      } catch (err) {
        logger.error(err.message);
      }

      try {
        // Send payment
        const payout: IPayout = await treezor.createPayout({
          beneficiaryId: payment.treezorBeneficiaryId,
          amount: payment.invoice.total,
          currency: payment.invoice.currency,
          walletId: payment.invoice.companyReceiver.treezorWalletId,
          payoutTag: payment.id,
          label: payment.invoice.companyReceiver.name || payment.invoice.companyReceiver.brandName || '',
          supportingFileLink: (payment.amount > 2000) ? payment.invoice.filepath : '',
        });

        // Update payment
        payment.treezorPayoutId = payout.payoutId;
        payment.treezorRequestAt = new Date();
        payment.status = PaymentStatus.TREEZOR_PENDING;
        await payment.save();
        logger.log(`Payment ${payment.id} passed`);
      } catch (err) {
        logger.error(err.message);
        // Update payment
        payment.status = PaymentStatus.TREEZOR_SYNC_KO_MISC;
        payment.treezorRequestAt = new Date();
        await payment.save();

        const companyReceiver: Company = await this.companyRepository.findOne({ where: { id: payment.invoice.companyReceiver.id }, relations: ['claimer'] });
        const claimer: User = companyReceiver.claimer;

        const zendesk: any = new Zendesk({
          url: process.env.ZENDESK_API_URL,
          email: process.env.ZENDESK_API_EMAIL,
          token: process.env.ZENDESK_API_TOKEN,
        });

        zendesk.tickets.create({
          type: 'incident',
          priority: 'urgent',
          requester: { name: claimer.fullName || null, email: claimer.email || null },
          subject: 'Paiement en erreur',
          comment: { body: `Le paiement ${payment.id} est retourné en erreur par treezor et demande une investiguation.\nLe paiement sera retenté automatiquement.\nMessage d'erreur : ${err.message}` },
          custom_fields: [environmentZendesk],
        });
      }
    }));
  }

  /**
   * Update invoice status
   *
   * @param   {string}            id      - Invoice's ID
   * @param   {InvoiceStatus}     status  - Current status of the invoice
   * @param   {User}              user    - Current user
   *
   * @return  {Promise<Invoice>}          - Returns the updated invoice
   */
  public async updateInvoiceStatus(id: string, status: InvoiceStatus, user: User): Promise<Invoice> {
    const invoice: Invoice = await this.invoiceRepository.findOne({ id });
    if (!invoice) {
      throw new HttpException('api.error.invoice.notfound', HttpStatus.NOT_FOUND);
    }

    if (invoice.status === InvoiceStatus.PAID) {
      throw new HttpException('api.error.invoice.already_paid', HttpStatus.BAD_REQUEST);
    }

    // TODO: Remove if DI on Subscriber
    // Cancellation of the invoice
    if (status === InvoiceStatus.TO_PAY && invoice.status === InvoiceStatus.PLANNED) {
      const payments: Payment[] = await this.paymentRepository.find({
        invoice,
        status: In([
          PaymentStatus.REQUESTED,
          PaymentStatus.SENT_TO_TREEZOR,
          PaymentStatus.TREEZOR_PENDING,
          PaymentStatus.TREEZOR_SYNC_KO_NOT_ENOUGH_BALANCE,
          PaymentStatus.TREEZOR_SYNC_KO_MISC,
          PaymentStatus.TREEZOR_WH_KO_MISC,
          PaymentStatus.TREEZOR_WH_KO_NOT_ENOUGH_BALANCE,
        ]),
      });

      const promises: any = payments.map(async (payment: Payment) => {
        payment.status = PaymentStatus.CANCELLED;
        payment.cancellationRequestAt = new Date();
        payment.cancellationRequestUser = user;
        await payment.save();

        if (payment.treezorPayoutId) {
          const treezor: TreezorService = new TreezorService({
            baseUrl: process.env.TREEZOR_API_URL,
            token: process.env.TREEZOR_TOKEN,
            secretKey: process.env.TREEZOR_SECRET_KEY,
          });

          try {
            await treezor.deletePayout(payment.treezorPayoutId);
          } catch (err) {
            throw new HttpException(err.message, err.statusCode);
          }
        }
      });

      await Promise.all(promises);

      // Update Libeo balance
      this.updateLibeoBalance(invoice.companyReceiver);
    }

    if (status === InvoiceStatus.SCANNED && invoice.status === InvoiceStatus.TO_PAY) {
      invoice.code = null;
      invoice.codeValidatedAt = null;
      invoice.codeValidatedBy = null;
    }

    invoice.status = status;
    await invoice.save();

    return invoice;
  }
}
