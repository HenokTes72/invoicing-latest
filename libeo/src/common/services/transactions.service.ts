import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { List } from '../interfaces/common.interface';
import { TreezorService } from '../../payment/treezor.service';
import { Company } from '../entities/company.entity';

@Injectable()
export class TransactionsService {
  /**
   * Get all transactions by company
   */
  public async findByCompany(company: Company, limit?: number, page?: number): Promise<List> {
    if (!company || !company.treezorWalletId) {
      return {
        total: 0,
        rows: [],
      };
    }

    let transactions = [];
    const treezor = new TreezorService({
      baseUrl: process.env.TREEZOR_API_URL,
      token: process.env.TREEZOR_TOKEN,
      secretKey: process.env.TREEZOR_SECRET_KEY,
    });

    try {
      const result = await treezor.getTransactions({ walletId: company.treezorWalletId, pageNumber: page, pageCount: limit });
      transactions = result.transactions;
    } catch (err) {
      throw new HttpException(err.message, HttpStatus.BAD_GATEWAY);
    }

    return {
      total: transactions.length,
      rows: transactions,
    };
  }
}
