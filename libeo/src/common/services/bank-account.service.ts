import { Injectable, HttpException, HttpStatus, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BankAccount } from '../entities/bank-account.entity';
import { Repository } from 'typeorm';
import { Company } from '../entities/company.entity';
import { List } from '../interfaces/common.interface';
import { CreateOrUpdateBankAccountDto } from '../dto/bank-account.dto';
import { Iban } from '../entities/iban.entity';
import { IbansService } from './ibans.service';
import { IValidateIban } from '../interfaces/iban.interface';
import { User } from '../entities/user.entity';

@Injectable()
export class BankAccountService {
  constructor(
    @InjectRepository(BankAccount)
    private readonly bankAccountRepository: Repository<BankAccount>,
    private readonly ibansService: IbansService,
  ) {}

  /**
   * Create or update a bank account
   *
   * @param   {User}                          user  - Current user
   * @param   {CreateOrUpdateBankAccountDto}  data  - Data of bank account
   * @param   {string}                        id    - Bank account's id
   *
   * @return  {Promise<BankAccount>}                - Returns a bank account object created/updated
   */
  public async createOrUpdateBankAccount(user: User, data: CreateOrUpdateBankAccountDto, id?: string): Promise<BankAccount> {
    if (!user.currentCompany) {
      throw new HttpException('api.error.company.not_found', HttpStatus.NOT_FOUND);
    }

    let bankAccount: BankAccount = null;

    if (id) {
      bankAccount = await this.bankAccountRepository.findOne({ where: { id }, relations: ['company', 'iban'] });
      if (!bankAccount) {
        throw new HttpException('api.error.bank_account.not_found', HttpStatus.NOT_FOUND);
      }

      bankAccount.label = data.label;
      return bankAccount.save();
    }

    bankAccount = this.bankAccountRepository.create({ label: data.label, company: user.currentCompany });

    // Check IBAN
    if (data.iban) {
      const alreadyIban: Iban = await this.ibansService.findOneByIbanAndCompany(data.iban, user.currentCompany);
      bankAccount.iban = alreadyIban;
      if (!alreadyIban) {
        try {
          const res: IValidateIban = await this.ibansService.getApiValidateIban(data.iban);
          res.iban = data.iban;
          bankAccount.iban = await this.ibansService.createIbanBankAccount(res, user);
        } catch (err) {
          const logger = new Logger();
          logger.error(err.message);
          throw new HttpException('api.error.iban.check', err.statusCode);
        }
      }
    }

    return this.bankAccountRepository.save(bankAccount);
  }

  /**
   * Get all bank account from company
   *
   * @param   {Company}        company  - Current company user
   *
   * @return  {Promise<List>}           - Returns a list of bank accounts
   */
  public async getBankAccounts(company: Company): Promise<List> {
    const [ bankAccounts, total ]: [ BankAccount[], number ] = await this.bankAccountRepository.findAndCount({ company });

    return {
      total,
      rows: bankAccounts,
    };
  }
}
