import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { History, HistoryEntity } from '../entities/history.entity';
import { CreateHistoryDto } from '../dto/histories.dto';
import { List } from '../interfaces/common.interface';

@Injectable()
export class HistoriesService {
  constructor(
    @InjectRepository(History)
    private readonly historyRepository: Repository<History>,
  ) {}

  /**
   * Log an event in DB
   */
  public async createHistory(data: CreateHistoryDto): Promise<History> {
    const history: History = this.historyRepository.create(data);
    return this.historyRepository.save(history);
  }

  /**
   * Get history from a invoice
   */
  public async findByInvoiceId(id: string, orderBy?: string, limit?: number, offset?: number): Promise<List> {
    const result = await Promise.all([
      await this.historyRepository.count({ entity: HistoryEntity.INVOICE, entityId: id }),
      await this.historyRepository.find({
        where: {
          entity: HistoryEntity.INVOICE,
          entityId: id,
        },
        order: {
          createdAt: 'DESC',
        },
        take: limit,
        skip: offset,
      }),
    ]);

    return {
      total: result[0],
      rows: result[1],
    };
  }
}
