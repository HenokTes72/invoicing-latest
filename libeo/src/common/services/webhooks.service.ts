import * as crypto from 'crypto';
import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Webhook } from '../entities/webhook.entity';
import { IWebhook } from '../interfaces/webhook.interface';

@Injectable()
export class WebhooksService {
  constructor(
    @InjectRepository(Webhook)
    private readonly webhookRepository: Repository<Webhook>,
  ) {}

  /**
   * Convert the keys of the object from SnakeCase to CamelCase
   *
   * @param   {any}  obj  - Object to be modified
   *
   * @return  {any}       - Return a object modified
   */
  private snakeToCamel(obj: any): any {
    const data = {};
    Object.keys(obj).forEach(k => {
      const key = k.replace(/([-_][a-z])/ig, ($1) => {
        return $1.toUpperCase()
          .replace('-', '')
          .replace('_', '');
      });

      data[key] = obj[k];
    });

    return data;
  }

  /**
   * Compare signature with our signature generated
   *
   * @param   {IWebhook}  data  - Data of webhook
   *
   * @return  {void}
   */
  private compareSignature(data: IWebhook): void {
    if (data.object_payload) {
      const signature: string = crypto
        .createHmac('sha256', process.env.TREEZOR_SECRET_KEY)
        .update(JSON.stringify(data.object_payload))
        .digest()
        .toString('base64');

      if (signature !== data.object_payload_signature) {
        throw new HttpException('Incorrect signature webhook', HttpStatus.BAD_REQUEST);
      }
    }
  }

  /**
   * Create a webhook
   *
   * @param   {IWebhook}          data  - Data of webhook
   *
   * @return  {Promise<Webhook>}        - Returns the created Webhook
   */
  public async createWebhook(data: IWebhook): Promise<Webhook> {
    data = this.snakeToCamel(data);
    const webhook: Webhook = this.webhookRepository.create(data);
    return this.webhookRepository.save(webhook);
  }

  /**
   * Update a webhook
   *
   * @param   {IWebhook}          where  - Filter request db
   * @param   {IWebhook}          data   - Data of webhook
   *
   * @return  {Promise<Webhook>}         - Returns the updated Webhook
   */
  public async updateWebhook(where: any, data: IWebhook): Promise<Webhook> {
    if (process.env.DOMAIN) {
      this.compareSignature(data);
    }

    data = this.snakeToCamel(data);

    let webhook: Webhook = await this.webhookRepository.findOne(where);
    if (!webhook) {
      webhook = this.webhookRepository.create(data);
    } else {
      webhook = Object.assign(webhook, data);
    }

    await this.webhookRepository.save(webhook);

    return webhook;
  }
}
