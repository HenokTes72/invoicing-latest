import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { TreezorService } from '../../payment/treezor.service';
import { Company } from '../entities/company.entity';
import { InvoiceStatus } from '../entities/invoice.entity';
import { InvoicesService } from './invoices.service';
import { PaymentRepository } from '../repositories/payment.repository';
import { InjectRepository } from '@nestjs/typeorm';
import { getStatusLibeoBalance } from '../entities/payment.entity';
import { IBalance } from '../../payment/interfaces/treezor/balance.interface';

@Injectable()
export class BalancesService {
  constructor(
    private readonly invoicesService: InvoicesService,
    @InjectRepository(PaymentRepository)
    private readonly paymentRepository: PaymentRepository,
  ) {}

  /**
   * Get the balance of Treezor
   *
   * @param   {Company}            company  - Current company object
   *
   * @return  {Promise<IBalance>}
   */
  public async getBalance(company: Company): Promise<IBalance> {
    if (!company || !company.treezorWalletId) {
      return null;
    }

    const treezor = new TreezorService({
      baseUrl: process.env.TREEZOR_API_URL,
      token: process.env.TREEZOR_TOKEN,
      secretKey: process.env.TREEZOR_SECRET_KEY,
    });

    try {
      const { balances } = await treezor.getBalances({ walletId: company.treezorWalletId });
      const [ balance ] = balances;

      return balance;
    } catch (err) {
      throw new HttpException(err.message, HttpStatus.BAD_GATEWAY);
    }
  }

  /**
   * Calculate an estimate of the balance based on future invoices
   *
   * @param   {IBalance}         balance  - Balance object of Treezor
   * @param   {Date}             dueAt    - Due date of invoice
   *
   * @return  {Promise<number>}
   */
  public async calculationLibeoBalance(balance: IBalance, dueAt: Date): Promise<number> {
    const sumInvoices: number = await this.paymentRepository.sumInvoicesByStatusAndDueAt(getStatusLibeoBalance, dueAt);

    return balance.authorizedBalance - sumInvoices;
  }

  /**
   * Check if the balance is sufficient
   *
   * @param   {IBalance}          balance    - Balance object of Treezor
   * @param   {Company}           company    - Current company object
   * @param   {string}            invoiceId  - Invoice's ID
   *
   * @return  {Promise<boolean>}
   */
  public async checkBalance(balance: IBalance, company: Company, invoiceId: string): Promise<boolean> {
    const invoice = await this.invoicesService.findOneByIdAndCurrentCompany(invoiceId, company);
    if (invoice.status !== InvoiceStatus.TO_PAY) {
      throw new HttpException('api.error.invoice.invalid_status', HttpStatus.BAD_REQUEST);
    }

    if (invoice.total < await this.calculationLibeoBalance(balance, invoice.dueDate)) {
      return true;
    }

    return false;
  }
}
