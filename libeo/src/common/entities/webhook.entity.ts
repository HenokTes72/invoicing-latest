import { Entity, BaseEntity, Column, CreateDateColumn, UpdateDateColumn, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Webhook extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ nullable: true })
  accessTag: string;

  @Column({ type: 'simple-json', nullable: true })
  requestPayload: any;

  @Column({ type: 'simple-json', nullable: true })
  responsePayload: any;

  @Column({ nullable: true })
  webhook: string;

  @Column({ nullable: true })
  webhookId: number;

  @Column({ nullable: true })
  object: string;

  @Column({ nullable: true })
  objectId: number;

  @Column({ type: 'simple-json', nullable: true })
  objectPayload: any;

  @Column({ nullable: true })
  objectPayloadSignature: string;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;
}
