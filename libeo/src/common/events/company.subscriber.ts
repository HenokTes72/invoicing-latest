import * as contextService from 'request-context';
import { EventSubscriber, EntitySubscriberInterface, InsertEvent, UpdateEvent, getRepository } from 'typeorm';
import { Company } from '../entities/company.entity';
import { TreezorService } from '../../payment/treezor.service';
import { HttpException } from '@nestjs/common';
import { HistoriesService } from '../services/histories.service';
import { HistoryEntity } from '../entities/history.entity';
import { HistoryEvent } from '../dto/histories.dto';
import { UserParentType } from '../../payment/interfaces/treezor/user.interface';
import * as shortid from 'shortid';

@EventSubscriber()
export class CompanySubscriber implements EntitySubscriberInterface<Company> {
  /**
   * Wait a few milliseconds
   *
   * @param   {number}        ms  - The milliseconds to wait
   *
   * @return  {Promise<any>}
   */
  private async delay(ms: number): Promise<any> {
    return new Promise(res => setTimeout(res, ms));
  }

  /**
   * Create a wallet for the company at Treezor
   *
   * @param   {TreezorService}    treezor  - Instance of TreezorService
   * @param   {Company}           company  - Current company
   *
   * @return  {Promise<Company>}           - Returns the modified current company
   */
  private async createWallet(treezor: TreezorService, company: Company): Promise<Company> {
    try {
      const wallet = await treezor.createWallet({
        walletTypeId: 10,
        userId: company.treezorUserId,
        eventName: company.siren,
        tariffId: parseInt(process.env.TREEZOR_TARIFFID, 10),
        currency: 'EUR',
      });

      company.treezorWalletId = wallet.walletId || null;
      company.treezorIban = wallet.iban || null;
      company.treezorBic = wallet.bic || null;

      return company;
    } catch (err) {
      throw new HttpException(err.message, err.statusCode);
    }
  }

  /**
   * Create a moral user (company) at Treezor
   *
   * @param   {TreezorService}    treezor  - Instance of TreezorService
   * @param   {Company}           company  - Current company
   *
   * @return  {Promise<Company>}           - Returns the modified current company
   */
  private async createMoralUser(treezor: TreezorService, company: Company): Promise<Company> {
    try {
      const [address1] = company.addresses;
      const user = await treezor.createUser({
        email: `payment.${shortid.generate()}@libeo.io`,
        userTypeId: 2,
        legalRegistrationNumber: company.siren,
        legalTvaNumber: company.vatNumber,
        legalName: company.name || company.brandName || '',
        legalForm: parseInt(company.legalForm, 10),
        legalRegistrationDate: company.incorporationAt,
        legalSector: company.naf,
        legalNumberOfEmployeeRange: company.numberEmployees || '0',
        legalAnnualTurnOver: company.legalAnnualTurnOver,
        legalNetIncomeRange: company.legalNetIncomeRange,
        phone: (company.phone) ? company.phone : '0000000000',
        address1: (address1) ? address1.address1 : null,
        city: (address1) ? address1.city : null,
        postcode: (address1) ? JSON.stringify(address1.zipcode) : null,
        country: 'FR',
      });
      company.treezorEmail = user.email || null;
      company.treezorUserId = user.userId || null;

      return company;
    } catch (err) {
      throw new HttpException(err.message, err.statusCode);
    }
  }

  /**
   * Create physical users with representatives
   *
   * @param   {TreezorService}  treezor  - Instance of TreezorService
   * @param   {Company}         company  - Current company
   *
   * @return  {Promise<void>}
   */
  private async createPhysicalUsers(treezor: TreezorService, company: Company): Promise<void> {
    let users = [];
    try {
      const { businessinformations } = await treezor.getBusinessInformations({
        country: 'FR',
        registrationNumber: company.siren,
      });
      const [info] = businessinformations;
      if (info && info.users) {
        users = info.users;
      }
    } catch (err) {
      throw new HttpException(err.message, err.statusCode);
    }

    const promises = users.map(async user => {
      if (user.userTypeId !== 1) {
        return null;
      }

      try {
        return await treezor.createUser({
          firstname: user.firstname || null,
          lastname: user.lastname || null,
          birthday: user.birthday || null,
          occupation: user.parentType || null,
          specifiedUSPerson: 0,
          parentUserId: company.treezorUserId,
          parentType: UserParentType.LEADER, // TODO: deprecated but required
          email: `payment.${shortid.generate()}@libeo.io`,
          userTypeId: 1,
        });
      } catch (err) {
        throw new HttpException(err.message, err.statusCode);
      }
    });

    try {
      await Promise.all(promises);
    } catch (err) {
      throw new HttpException(err.message, err.statusCode);
    }
  }

  /**
   * Indicates that this subscriber only listen to Company events.
   */
  public listenTo() {
    return Company;
  }

  /**
   * Called after company inserting.
   */
  public async afterInsert(event: InsertEvent<Company>) {
    if (!event.entity.claimer) {
      return;
    }

    const treezor: TreezorService = new TreezorService({
      baseUrl: process.env.TREEZOR_API_URL,
      token: process.env.TREEZOR_TOKEN,
      secretKey: process.env.TREEZOR_SECRET_KEY,
    });

    let company: Company = event.entity;
    company = await this.createMoralUser(treezor, company);
    await this.delay(300);
    company = await this.createWallet(treezor, company);
    await this.createPhysicalUsers(treezor, company);
    await event.manager.save(company);
  }

  /**
   * Called after company updating.
   */
  public async afterUpdate(event: UpdateEvent<Company>) {
    if (event.entity.kycStatus !== event.databaseEntity.kycStatus) {
      const historiesService: HistoriesService = new HistoriesService(getRepository('History'));
      historiesService.createHistory({
        user: contextService.get('request:user'),
        params: { kycStatus: event.entity.kycStatus },
        entity: HistoryEntity.COMPANY,
        entityId: event.databaseEntity.id,
        event: HistoryEvent.UPDATE_KYC_STATUS,
      });
    }

    // Shell company
    if (!event.databaseEntity.siren) {
      const treezor: TreezorService = new TreezorService({
        baseUrl: process.env.TREEZOR_API_URL,
        token: process.env.TREEZOR_TOKEN,
        secretKey: process.env.TREEZOR_SECRET_KEY,
      });

      let company: Company = event.entity;
      company = await this.createMoralUser(treezor, company);
      await this.delay(300);
      company = await this.createWallet(treezor, company);
      await this.createPhysicalUsers(treezor, company);
      await event.manager.save(company);
    }
  }
}
