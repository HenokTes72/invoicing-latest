import { EntityRepository, Repository } from 'typeorm';
import { Payment, PaymentStatus, getStatusLibeoBalance } from '../entities/payment.entity';
import { Company, CompanyKycStatus } from '../entities/company.entity';

@EntityRepository(Payment)
export class PaymentRepository extends Repository<Payment> {
  /**
   * Get sum invoices
   *
   * @param   {Array<PaymentStatus>}  status  - Array of status payment
   * @param   {Date}                  dueAt   - Due date of invoice
   *
   * @return  {Promise<number>}               - Return sum of invoices
   */
  public async sumInvoicesByStatusAndDueAt(status: PaymentStatus[], dueAt: Date): Promise<number> {
    const res = await this.createQueryBuilder('p')
      .select('SUM(invoice.total)', 'sum')
      .leftJoin('p.invoice', 'invoice')
      .where('invoice.dueDate <= :dueAt', { dueAt })
      .andWhere('p.status IN(:...status)', { status })
      .andWhere('p.libeoEstimatedBalance >= 0')
      .getRawOne();
    // TODO: Add current company
    // Warning: Insertion à la même date quand on aura l'heure à minuit
    // where (invoice.dueDate < :dueAt or (invoice.dueDate = :dueAt and invoice.createdAt < createdAt)) and ...

    return (res && res.sum) ? res.sum : 0;
  }

  /**
   * Get the deferred payments
   *
   * @param   {PaymentStatus[]}  status  - Array of status payment
   *
   * @return  {Promise<Payment[]>}       - Return the payments
   */
  public async getDeferredPayments(status: PaymentStatus[]): Promise<Payment[]> {
    return this.createQueryBuilder('p')
      .leftJoinAndSelect('p.invoice', 'i')
      .leftJoinAndSelect('i.companyReceiver', 'c')
      .where('p.paymentAt <= :date', { date: new Date() })
      .andWhere('p.treezorBeneficiaryId is not null')
      .andWhere('p.status IN(:...status)', { status })
      .andWhere('p.libeoEstimatedBalance > 0')
      .andWhere('c.kycStatus = :companyKycStatus', { companyKycStatus: CompanyKycStatus.VALIDATED })
      .andWhere('c.isFreezed IS NOT TRUE')
      .getMany();
  }

  /**
   * Get the planned payments
   *
   * @param   {Company}  companyReceiver  - Company object
   * @param   {Date}     paymentAt        - Date of payment
   *
   * @return  {Promise<Payment[]>}        - Return the payments
   */
  public async getPlannedPayments(companyReceiver: Company, paymentAt?: Date): Promise<Payment[]> {
    const q = this.createQueryBuilder('p');

    q.leftJoinAndSelect('p.invoice', 'i')
      .leftJoin('i.companyReceiver', 'c')
      .where('p.status IN (:...status)', { status: getStatusLibeoBalance });

    if (paymentAt) {
      q.andWhere('p.paymentAt > :date', { date: paymentAt });
    }

    return q
      .andWhere('c.id = :companyReceiverId', { companyReceiverId: companyReceiver.id })
      .orderBy('p.paymentAt', 'ASC')
      // .orderBy('p.createdAt', 'ASC')
      .getMany();
  }
}
