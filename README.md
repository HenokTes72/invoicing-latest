# Installation

Dependencies:

- node (npm/yarn)
- docker
- docker-compose
- docker registry (registry.gitlab.com)
- git repository (gitlab.com)


## Environnement

```bash
cp .env.dist .env
```

## Docker

To build image:

Requirements:
* Docker
* `~/.yarnrc`, `~/.yarn/` and `~/.cache/yarn/` have to exist. 

1. Build application:
    ```bash
    docker run --rm -it --read-only \
      --entrypoint /bin/sh \
      --user 1000:1000 \
      -v $(pwd):/workspace \
      -v ~/.cache/yarn/:/home/node/.cache/yarn/ \
      -v /tmp/:/tmp \
      -v ~/.yarn/:/home/node/.yarn/ \
      -v ~/.yarnrc:/home/node/.yarnrc \
      -w /workspace \
      node:11-alpine -c "yarn workspaces run install --frozen-lockfile"
    docker run --rm -it --read-only \
      --entrypoint /bin/sh \
      --user 1000:1000 \
      -v $(pwd):/workspace \
      -v ~/.cache/yarn/:/home/node/.cache/yarn/ \
      -v /tmp/:/tmp \
      -v ~/.yarn/:/home/node/.yarn/ \
      -w /workspace \
      node:11-alpine -c "yarn workspaces run build"
    ```
2. Build Docker image
    ```bash
    docker build -t registry.gitlab.com/libeo/libeo:latest -f config/docker/node/Dockerfile .
    ```

### Setup network

In local env, you need to create a docker network called `traefik_net`. Run:

``` bash
docker network create traefik_net
```

### Setup registry

You need to [login to the docker registry](https://docs.docker.com/engine/reference/commandline/login/) of Libeo `registry.gitlab.com/libeo/docker-images/`. Run:

``` bash
docker login registry.gitlab.com -u GITLAB_USERNAME -p GITLAB_PASSWORD
```

## Run

Run:

``` bash
yarn up
```

For clean docker resources:

``` bash
yarn down
```
