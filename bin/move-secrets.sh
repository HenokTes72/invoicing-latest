#!/bin/sh
if [[ ${CI_COMMIT_REF_NAME} == "master" ]]; then
  BRANCH=master
else
  BRANCH=develop
fi

rsync -arvc secrets/${BRANCH}/.env ./
rsync -arvc secrets/${BRANCH}/.env ./libeo
rsync -arvc secrets/${BRANCH}/config.js ./libeoweb
