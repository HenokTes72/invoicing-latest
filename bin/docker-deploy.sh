#!/bin/bash
DEPLOY_USER="api"
REACT_APP_API_URL="https://api.${CI_PROJECT_NAME}.io"

if [ "${CI_COMMIT_REF_NAME}" = "master" ]
then
  DEPLOY_DOMAIN="${DEPLOY_USER}.${CI_PROJECT_NAME}.io"
else
  DEPLOY_DOMAIN="${DEPLOY_USER}.sandbox.${CI_PROJECT_NAME}.io"
  REACT_APP_API_URL="https://api.sandbox.${CI_PROJECT_NAME}.io"
fi

# environment variables
DISTANT="/var/www/vhosts/${DEPLOY_DOMAIN}/next-v2"
DEPLOY="${DEPLOY_USER}@${DEPLOY_DOMAIN}"
DOCKER_PROJECT_NAME="${CI_PROJECT_NAME}${CI_COMMIT_REF_NAME}"

echo "DEPLOY REGISTRY IMAGE:\nregistry.gitlab.com/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}/${CI_COMMIT_REF_NAME}:${CI_COMMIT_SHA}"
echo " "

# prepare package
mkdir -p build/tmp/
cp docker-compose.yml build/tmp/docker-compose.yml

mkdir -p build/tmp/config/docker/varnish
cp $PWD/config/docker/varnish/default.vcl build/tmp/config/docker/varnish/default.vcl

mkdir -p build/tmp/config/docker/httpd
cp $PWD/config/docker/httpd/httpd.conf build/tmp/config/docker/httpd/httpd.conf

mkdir -p build/tmp/libeoweb
cp -r libeoweb/build/. build/tmp/libeoweb

# copy secret into tmp folder
cp "secrets/${CI_COMMIT_REF_NAME}/.env" build/tmp/
cp "secrets/${CI_COMMIT_REF_NAME}/config.js" build/tmp/libeoweb/

cat build/tmp/.env

# echo variable to .env file
echo "DOMAIN=${DEPLOY_DOMAIN}" >> build/tmp/.env
echo "REACT_APP_API_URL=${REACT_APP_API_URL}" >> build/tmp/.env
echo "CI_COMMIT_REF_NAME=${CI_COMMIT_REF_NAME}" >> build/tmp/.env
echo "CI_COMMIT_SHA=${CI_COMMIT_SHA}" >> build/tmp/.env
echo "CI_PROJECT_NAME=${CI_PROJECT_NAME}" >> build/tmp/.env

# then load .env variable into cluster
source build/tmp/.env

# curl https://ipinfo.io/ip

# deploy on environment
echo "ssh -4 -o StrictHostKeychecking=no -o UserKnownHostsFile=/dev/null -i secrets/libeo-deploy ${DEPLOY} \"[ ! -e ${DISTANT} ] && mkdir -p ${DISTANT} || true\""
echo " "

ssh -4 -o StrictHostKeychecking=no -o UserKnownHostsFile=/dev/null -i secrets/libeo-deploy ${DEPLOY} "[ ! -e ${DISTANT} ] && mkdir -p ${DISTANT} || true"

echo "rsync -arvc -e 'ssh -4 -o StrictHostKeychecking=no -o UserKnownHostsFile=/dev/null -i secrets/libeo-deploy' --delete build/tmp/ \"${DEPLOY}:${DISTANT}/\""
echo " "
rsync -arvc -e 'ssh -4 -o StrictHostKeychecking=no -o UserKnownHostsFile=/dev/null -i secrets/libeo-deploy' --delete build/tmp/ "${DEPLOY}:${DISTANT}/"

echo "ssh -4 -o StrictHostKeychecking=no -o UserKnownHostsFile=/dev/null -i secrets/libeo-deploy ${DEPLOY} \"cd ${DISTANT} && docker login -u ${CI_REGISTRY_USER} -p ${CI_REGISTRY_PASSWORD} ${CI_REGISTRY} && docker-compose pull && docker-compose --project-name ${DOCKER_PROJECT_NAME} up -d --force-recreate --no-build\""
echo " "
ssh -4 -o StrictHostKeychecking=no -o UserKnownHostsFile=/dev/null -i secrets/libeo-deploy ${DEPLOY} "cd ${DISTANT} && docker login -u ${CI_REGISTRY_USER} -p ${CI_REGISTRY_PASSWORD} ${CI_REGISTRY} && docker-compose pull && docker-compose --project-name ${DOCKER_PROJECT_NAME} up -d --force-recreate --no-build"
rm -Rf build/tmp
